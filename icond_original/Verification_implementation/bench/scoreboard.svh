class scoreboard extends uvm_scoreboard;
    `uvm_component_utils(scoreboard)
	
    // Variables

    reffmod reffmod_h;
    comparator comparator_h;

    // Initialization functions

    function new(string name, uvm_component parent);
        super.new(name, parent);
    endfunction: new
            
    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        reffmod_h = reffmod::type_id::create("reffmod_h" , this);
        comparator_h = comparator::type_id::create("comparator_h" , this);
    endfunction: build_phase
                            
    function void connect_phase(uvm_phase phase);
      reffmod_h.tx_exported.connect(comparator_h.reffmod_export);
    endfunction

endclass
