class reffmod extends uvm_component;
	`uvm_component_utils(reffmod)

	// Parameters and enum
    localparam CURRENT_MODE = 4;
    typedef enum {IDLE = 0, FAULT = 1, DONE = 2, PRECHARGE = 3, CHARGE = 4, FLOAT = 5} charge_states;

    //PID and cc_cv_mode variables
    localparam ERR_MAX = 1023;
    localparam ERR_MIN = -1023;

    // MPPT and main loop parameters
    localparam MPPT_INTERVAL = 9259; //100000000/(40*270)
    localparam MPPT_AVERAGE = 4; //adjust how many samples we want
    localparam TRACK_DELAY = 2; //debouncing between the two modes
    localparam SECOND_COUNT = 976;
    localparam MPPT_STEP = 19; //0.5v

    // Battery state machine parameters
    localparam IMIN_UPDATE = 5;
    localparam I_BAT_DETECT = 16;
    localparam VBAT_DETECTION = 1640;

    localparam ILIM = 1024;
    localparam TOPPING_VOLTAGE = 586;
    localparam IFLAT_COUNT = 600;
    localparam FLOAT_TIME = 14400;
    localparam FLOATING_VOLTAGE = 628;
    localparam FLOAT_RELAX_TIME = 60;
    localparam IFLOAT = 102;
    localparam CUTOFF_VOLTAGE = 488;
    localparam CHARGING_VOLTAGE = 674;
    
    uvm_analysis_port #(out_transaction) tx_exported;
    uvm_blocking_get_port #(in_transaction) tx_collected;

	// Variables

	out_transaction out_tx, previous_out;
	in_transaction in_tx, previous_in;
    init init_h;

    //PID and cc_cv_mode variables
    int unsigned pp, warmup;
    byte unsigned cc_cv;
    byte cmode;
    int pi, increment;

    // MPPT and main loop variables
    longint unsigned power, l_power, f_iin, f_vin, fl_iin, fl_vin, vinref, ineq;
    int unsigned second;
    int track, mppt_calc, dmax;

    // Battery state machine variables
    byte unsigned battery_state, imin_db;
    int unsigned imin, iflat_db, state_counter, vref, iref;

	function new(string name, uvm_component parent);
		super.new(name, parent);
        track = TRACK_DELAY;
        mppt_calc = MPPT_INTERVAL;
        dmax = 0;
	endfunction : new

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		tx_exported = new("tx_exported", this);
		tx_collected = new("tx_collected", this);
        out_tx = out_transaction::type_id::create("out_tx");
		// MPPT and main loop variables
		uvm_config_db#(int unsigned)::get(null, "", "second", second);
		//PID and cc_cv_mode variables
		uvm_config_db#(byte unsigned)::get(null, "", "cc_cv", cc_cv);
        uvm_config_db#(byte)::get(null, "", "cmode", cmode);
        uvm_config_db#(int unsigned)::get(null, "", "warmup", warmup);
        uvm_config_db#(int)::get(null, "", "increment",increment );
        // Battery state machine variables
        uvm_config_db#(int unsigned)::get(null, "", "iref", iref);
        uvm_config_db#(int unsigned)::get(null, "", "vref", vref);
        uvm_config_db#(byte unsigned)::get(null, "", "battery_state", battery_state);
        uvm_config_db#(byte unsigned)::get(null, "", "imin_db", imin_db);
        uvm_config_db#(int unsigned)::get(null, "", "imin", imin);
        uvm_config_db#(int unsigned)::get(null, "", "iflat_db", iflat_db);
        uvm_config_db#(int unsigned)::get(null, "", "state_counter", state_counter);
	endfunction : build_phase

	virtual function int calculate_power(longint unsigned current, longint unsigned tension);
		return current*tension;
	endfunction : calculate_power

	virtual task cc_cv_mode;
        if (in_tx.vout > vref) begin 
            if(cc_cv)
                cc_cv--;
            else begin
                if(cmode)
                    pi = 0;
                cmode = 0;
            end
        end

        if(in_tx.iout > iref) begin 
            
            if (!cmode)
                pi = 0;
            cmode = 1;
            cc_cv = CURRENT_MODE;
        end
    endtask

    virtual task pid(longint unsigned setpoint, longint unsigned feedback);
        int er;
        int ipid;

        er = setpoint - feedback;

        if(er > ERR_MAX)
            er = ERR_MAX;
        if (er < ERR_MIN)
            er = ERR_MIN;
        if(!warmup) begin
            pp = er;

            pi += er;
            
            if (pi > ERR_MAX)
                pi = ERR_MAX;
            if (pi < ERR_MIN)
                pi = ERR_MIN;

            ipid = pp;
            ipid += (pi / 256);

            if (ipid > ERR_MAX)
                ipid = ERR_MAX;
            if (ipid < ERR_MIN)
                ipid = ERR_MIN;

            increment += ipid;
        end
        else begin
            warmup--;
            if (er > 0)
                increment++;
            else
                increment--;

            pi = 0;
        end
        init_h.set_NCO;
        
    endtask : pid

    virtual task battery_state_machine;
        if(battery_state == PRECHARGE)
        begin
            out_tx.led_s = 2'b10;
            if(in_tx.vout < CUTOFF_VOLTAGE)//battery
            begin
                if(state_counter)
                    state_counter--;
                else
                begin
                    battery_state = FAULT;
                    out_tx.led_s = 2'b10;
                end
            end else
            begin
                battery_state = CHARGE;
                init_h.SET_CURRENT(ILIM);//battery
            end
        end else
        if(battery_state == CHARGE)
        begin
            out_tx.led_s = 2'b10;
            if(!cmode)
            begin
                if(in_tx.iout < imin)
                begin
                    if(imin_db)
                        imin_db--;
                    else
                    begin
                        imin = in_tx.iout;
                        imin_db = IMIN_UPDATE;
                        iflat_db = IFLAT_COUNT;//battery
                    end
                end else
                begin
                    imin_db = IMIN_UPDATE;
                    if(iflat_db) 
                        iflat_db--;
                end
            end else
            begin
                imin_db = IMIN_UPDATE;
                iflat_db = IFLAT_COUNT;//battery
                imin = ILIM;//battery
            end
            if(imin < IFLOAT || !iflat_db)//battery
            begin
                battery_state = FLOAT;
                state_counter = FLOAT_TIME;//battery

                init_h.SET_VOLTAGE(FLOATING_VOLTAGE);//battery
            end
        end  else
        if(battery_state == FLOAT)
        begin
            out_tx.led_s = 2'b01;
            if(state_counter)
                state_counter--;
            else
            begin
                battery_state = DONE;
            end 
            if(state_counter < FLOAT_RELAX_TIME && in_tx.iout < I_BAT_DETECT)//baterry
                battery_state = IDLE;
        end  else
        if(battery_state == IDLE)
        begin
            out_tx.led_s = 2'b00;
            init_h.SET_VOLTAGE(0);
            init_h.SET_CURRENT(0);
            init_h.stop_converter;
        end  else
        if(battery_state == FAULT)
        begin
            out_tx.led_s = 2'b11;
            init_h.SET_VOLTAGE(0);
            init_h.SET_CURRENT(0);
            init_h.stop_converter;  
        end  else
        if(battery_state == DONE)
        begin
            out_tx.led_s = 2'b01;
            if(in_tx.vout < TOPPING_VOLTAGE && in_tx.vout > VBAT_DETECTION)//battery
            begin
                battery_state = CHARGE;

                init_h.SET_CURRENT(ILIM);//battery
                init_h.SET_VOLTAGE(CHARGING_VOLTAGE);//battery

                imin = ILIM;
                imin_db = IMIN_UPDATE;
                iflat_db = IFLAT_COUNT;//battery

                init_h.start_converter;
            end  else
            begin
                init_h.SET_VOLTAGE(0);
                init_h.SET_CURRENT(0);
                init_h.stop_converter;   
                if(in_tx.vout < VBAT_DETECTION)
                    battery_state = IDLE;
            end 
        end 
    endtask : battery_state_machine

    virtual task mppt;
        longint unsigned delta_i, delta_v, delta_p;
        power = f_iin * f_vin;
        delta_p = power - l_power;

        delta_i = f_iin - fl_iin;
        delta_v = f_vin - fl_vin;

        if (delta_v) begin 
            ineq = delta_p / delta_v;
            if(ineq > 0)
                vinref += MPPT_STEP;
            else if(ineq < 0)
                vinref -= MPPT_STEP;
        end
        else begin
            if (delta_i > 0)
                vinref += MPPT_STEP;
            else if(delta_i)
                vinref -= MPPT_STEP;
        end

        fl_iin = f_iin;
        fl_vin = f_vin;
        l_power = power;

        f_iin = 0;
        f_vin = 0;
    endtask : mppt

	virtual task run_phase(uvm_phase phase);
        init_h.init_hardware();
		forever begin
			tx_collected.get(in_tx);

			if(in_tx.reset == 0)
			begin
				init_h.init_hardware();
				init_h.init_state_machine();
				track = TRACK_DELAY;
        		mppt_calc = MPPT_INTERVAL;
        		dmax = 0;
			end

            if(track && mppt_calc)
                mppt_calc--;
            if(second)
                second--;

            out_tx.read = 1;
            //270 clks
            if(in_tx.done)
            begin
                out_tx.read = 0;
                if(!track)
                begin
                    if(battery_state != FAULT)
                    begin
                        cc_cv_mode;
                        if(!cmode)
                            pid(in_tx.vout, vref);
                        else
                            pid(in_tx.iout, iref);

                        if(increment >= dmax)
                            track = TRACK_DELAY;
                    end 
                end  else
                begin
                    if(mppt_calc < MPPT_AVERAGE)
                    begin
                        f_vin += in_tx.vin;
                        f_iin += in_tx.iin;
                    end 

                    if(!mppt_calc)
                    begin
                        init_h.init_hardware();
                        mppt_calc = MPPT_INTERVAL;

                        mppt;
                    end 

                    pid(vinref, in_tx.vin);

                    if(in_tx.vout > vref || in_tx.iout)
                    begin
                        track--;
                        dmax = increment;
                        if(!track)
                            init_h.init_state_machine();
                    end  else
                        track = TRACK_DELAY;
                end 
                if(!second);
                begin
                    second = SECOND_COUNT;
                    if(!track)
                        battery_state_machine();
                end 
            end 

			tx_exported.write(out_tx);
		end
	
	endtask : run_phase

endclass : reffmod
