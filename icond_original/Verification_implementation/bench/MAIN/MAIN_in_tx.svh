class in_transaction extends uvm_sequence_item;
	`uvm_object_utils(in_transaction)

	// Parameters

	localparam DATA_WIDH = 8;

	// Variables

	rand longint unsigned vin;
	rand longint unsigned iin;
	rand longint unsigned vout;
	rand longint unsigned iout;
	rand bit done;
	rand bit reset;

	constraint c_vin {vin >= 0 ; vin <= 255;}
	constraint c_iin {iin >= 0 ; iin <= 255;}
	constraint c_vout {vout >= 0 ; vout <= 255;}
	constraint c_iout {iout >= 0 ; iout <= 255;}

	// Basic functions

	function new(string name = "MAIN_in_tx");
		super.new(name);
	endfunction : new

	function string convert2string;
		return $sformatf("vin = %0d, iin = %0d, vout = %0d, iout = %0d, done = %b, reset = %b",
			vin, iin, vout, iout, done, reset);
	endfunction : convert2string

endclass
