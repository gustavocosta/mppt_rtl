class init extends uvm_component;
    `uvm_component_utils(init)
    
    // Parameters and typedefs

    localparam NCO_MIN = 1;
    localparam NCO_MAX = 29500;
    localparam WARMUP_TIME = 64;
    localparam CURRENT_MODE = 4;
    localparam BUTTON_COUNT = 61;
    localparam SECOND_COUNT = 976;
    localparam SECOND_COUNT_START = 999;
    localparam IMIN_UPDATE = 5;
    typedef enum {IDLE = 0, FAULT = 1, DONE = 2, PRECHARGE = 3, CHARGE = 4, FLOAT = 5} charge_states;
    
    // Battery parameters

    localparam PRECHARGE_TIME = 600;
    localparam ILIM_PRECHARGE = 1024;
    localparam CHARGING_VOLTAGE = 674;
    localparam ILIM = 1024;
    
    // Battery charge variables

    
    int unsigned  state_counter, iflat_db, imin;
    byte unsigned  imin_db;
    charge_states battery_state;
    
    // Hardware Variables 
    
    int unsigned  warmup, vref, iref, second;
    byte unsigned cc_cv, but_cnt, led_cnt; 
    int increment;
    byte cmode;
    bit[1:0] led_s;
    bit T0IF;
    
    // UVM functions

    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
    endfunction
    
    // Init database

    function void init_database;
        // SET

        uvm_config_db#(int)::set(null, "uvm_test_top.*", "increment", 0);
        
        uvm_config_db#(byte)::set(null, "uvm_test_top.*", "cmode", 0);
        uvm_config_db#(int unsigned )::set(null, "uvm_test_top.*", "warmup", 0);
       
              
        uvm_config_db#(byte unsigned)::set(null, "uvm_test_top.*", "cc_cv", 0);
        uvm_config_db#(byte unsigned)::set(null, "uvm_test_top.*", "but_cnt", 0);
       

        uvm_config_db#(int unsigned)::set(null, "uvm_test_top.*", "second", 0);

        uvm_config_db#(byte unsigned)::set(null, "uvm_test_top.*", "battery_state", 0);
        uvm_config_db#(int unsigned)::set(null, "uvm_test_top.*", "iref", 0);
        uvm_config_db#(int unsigned)::set(null, "uvm_test_top.*", "vref", 0);

        uvm_config_db#(bit[1:0])::set(null, "uvm_test_top.*", "led_s", 0);

        uvm_config_db#(byte unsigned)::set(null, "uvm_test_top.*", "imin_db", 0);
        uvm_config_db#(int unsigned)::set(null, "uvm_test_top.*", "imin", 0);
        uvm_config_db#(int unsigned)::set(null, "uvm_test_top.*", "iflat_db", 0);
        uvm_config_db#(int unsigned)::set(null, "uvm_test_top.*", "state_counter", 0);
        
        // GET

        uvm_config_db#(int)::get(null, "", "increment",increment );
        
        uvm_config_db#(byte)::get(null, "", "cmode", cmode);
        uvm_config_db#(int unsigned)::get(null, "", "warmup", warmup);
       
              
        uvm_config_db#(byte unsigned)::get(null, "", "cc_cv", cc_cv);
        uvm_config_db#(byte unsigned)::get(null, "", "but_cnt", but_cnt);
       

        uvm_config_db#(int unsigned)::get(null, "", "second", second);

        uvm_config_db#(byte unsigned)::get(null, "", "battery_state", battery_state);
        uvm_config_db#(int unsigned)::get(null, "", "iref", iref);
        uvm_config_db#(int unsigned)::get(null, "", "vref", vref);
        
        uvm_config_db#(bit[1:0])::set(null, "", "led_s", led_s);

        uvm_config_db#(byte unsigned)::get(null, "", "imin_db", imin_db);
        uvm_config_db#(int unsigned)::get(null, "", "imin", imin);
        uvm_config_db#(int unsigned)::get(null, "", "iflat_db", iflat_db);
        uvm_config_db#(int unsigned)::get(null, "", "state_counter", state_counter);
    endfunction : init_database
    
    // Hardware initialization
    
    function void init_hardware;
        
        increment = NCO_MIN;

        cmode = 0;
        warmup = WARMUP_TIME;

        cc_cv = CURRENT_MODE;
        but_cnt = BUTTON_COUNT;
        second = SECOND_COUNT_START;

        battery_state = IDLE;

        iref = 0;
        vref = 0;

        stop_converter();

    endfunction : init_hardware


    // State machine initialization

    function void init_state_machine;
        battery_state = PRECHARGE;
        state_counter = PRECHARGE_TIME; //battery

        SET_CURRENT(ILIM_PRECHARGE); //battery A ACHAR
        SET_VOLTAGE(CHARGING_VOLTAGE); //battery A ACHAR

        imin = ILIM;            //battery A ACHAR
        imin_db = IMIN_UPDATE; //a achar
        //iflat_db = IFLAT_COUNT; //battery A ACHAR

    endfunction


    // Supporting functions

    function void start_converter;
        warmup = WARMUP_TIME;
    endfunction : start_converter

    function void stop_converter;

        uvm_config_db#(int)::set(null, "uvm_test_top.*", "increment", 0);
        set_NCO();
    endfunction : stop_converter

    function void set_NCO;
        if (increment < NCO_MIN)
            increment = NCO_MIN;
        if (increment > NCO_MAX)
            increment = NCO_MAX;

    endfunction : set_NCO
    
    function void SET_VOLTAGE(int unsigned x);
        vref = x;       
    endfunction : SET_VOLTAGE

    function void SET_CURRENT(int unsigned x);
        iref = x;
    endfunction : SET_CURRENT

endclass : init
