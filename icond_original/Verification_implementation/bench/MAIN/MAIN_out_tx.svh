class out_transaction extends uvm_sequence_item;
	`uvm_object_utils(out_transaction)

	// Variables

	int increment;
	bit [1:0] led_s;
	bit read;

	// Basic functions

	function new(string name = "MAIN_out_tx");
		super.new(name);
	endfunction : new

    function bit compare(out_transaction to_be_compared);
        if (increment == to_be_compared.increment &&
            led_s == to_be_compared.led_s &&
            read == to_be_compared.read)
            return 1;
        else
            return 0;
    endfunction

	function string convert2string;
		return $sformatf("increment = %0d, led_s = %b, read = %b",increment, led_s, read);
	endfunction : convert2string

endclass
