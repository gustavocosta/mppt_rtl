class seqlib extends uvm_sequence #(in_transaction);
	`uvm_object_utils(seqlib);

	function new(string name = "");
		super.new(name);
	endfunction : new

	task body;
		in_transaction tx;

		forever begin
			tx = in_transaction::type_id::create("tx");
			start_item(tx);
			assert(tx.randomize());
			finish_item(tx);
		end
	endtask : body

endclass
