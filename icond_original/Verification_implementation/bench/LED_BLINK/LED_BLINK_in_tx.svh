class in_transaction extends uvm_sequence_item;
	`uvm_object_utils(in_transaction)

	rand bit [1:0] led_s;
	bit reset;

	function new(string name = "in_transaction");
		super.new(name);
	endfunction : new

	function string convert2string;
		return $sformatf("led_s = %b", led_s);
	endfunction : convert2string

endclass : in_transaction
