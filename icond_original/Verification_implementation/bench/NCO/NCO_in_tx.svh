class in_transaction extends uvm_sequence_item;
	`uvm_object_utils(in_transaction)

	// Variables

	rand int increment;
	bit reset;

	constraint c_increment {increment >= 1 ; increment <= 29500;}

	// Basic functions
	
	function new(string name = "in_transaction");
		super.new(name);
	endfunction : new

	function string convert2string;

		return $sformatf("increment = %0d, reset = %b", increment, reset);

	endfunction : convert2string


endclass : in_transaction
