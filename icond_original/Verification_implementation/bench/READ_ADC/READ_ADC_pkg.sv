package pkg;

    import uvm_pkg::*;
    
    `include "uvm_macros.svh"
    `include "READ_ADC_in_tx.svh"
    `include "READ_ADC_out_tx.svh"
    `include "READ_ADC_reffmod.svh"
    `include "../comparator.svh"
    `include "../scoreboard.svh"
    `include "../in_monitor.svh"
    `include "READ_ADC_out_monitor.svh"
    `include "READ_ADC_in_driver.svh"
    `include "READ_ADC_seqlib.svh"
    `include "../in_agent.svh"
    `include "READ_ADC_subscriber.svh"
    `include "../out_agent.svh"
    `include "../env.svh"
    `include "../test.svh"
    
    
endpackage
