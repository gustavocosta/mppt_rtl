class out_monitor extends uvm_monitor;
    `uvm_component_utils(out_monitor)
     
    // out_monitor variables
    
    virtual dut_if vif;
    out_transaction tx;
    uvm_analysis_port #(out_transaction) out_aport;

    // Initialization functions
    
    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase (uvm_phase phase);
        super.build_phase(phase);
        if (!uvm_config_db#(virtual dut_if)::get(this,"","vif", vif))
            `uvm_fatal("NOVIF", $sformatf("The dut_if virtual interface was not pre-configured"));
        out_aport = new("out_aport", this);
        tx = out_transaction::type_id::create("tx");
    endfunction :  build_phase

    task monitoring_item(uvm_phase phase);
        tx.vin = vif.vin;
        tx.iin = vif.iin;
        tx.vout = vif.vout;
        tx.iout = vif.iout;
        tx.A = vif.A;
        tx.convst = vif.convst;
        tx.rd = vif.rd;
        tx.done = vif.done;
    endtask
    
    task run_phase (uvm_phase phase);
        forever begin
            @(posedge vif.clk);
            #2;
            monitoring_item(phase);
            out_aport.write(tx);
        end
    endtask : run_phase
    
endclass : out_monitor
