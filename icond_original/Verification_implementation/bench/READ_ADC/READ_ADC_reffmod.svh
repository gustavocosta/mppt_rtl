class reffmod extends uvm_component;
	`uvm_component_utils(reffmod)

	typedef enum {S0, S1, S2, S3, S4, S5, S6, S7,
	 S8, S9, S10, S11, S12, S13, S14, S15} states;

	// Parameters
    
    localparam BYTE_MAX_VALUE = 256;
    localparam CONST_CONV = 0.8;
    localparam VREF = 2.5;

	//Ports
    
    uvm_analysis_port #(out_transaction) tx_exported;
    uvm_blocking_get_port #(in_transaction) tx_collected;

	// Variables

	out_transaction out_tx;
	in_transaction in_tx;
	states states_h;
    real vl,il;
    bit [2:0] count;
    bit [3:0] count_states;
    int weight;


	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	virtual function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		tx_exported = new("tx_exported", this);
		tx_collected = new("tx_collected", this);
		out_tx = out_transaction::type_id::create("out_tx");
        count = 3'b000;
        count_states = 4'b0000;
        weight = 0;
        states_h = states'(count_states);
	endfunction : build_phase

    virtual function void calc_vl;
        if (out_tx.A == 3'b000)
            out_tx.vin = in_tx.db;
        else if(out_tx.A == 3'b010)
            out_tx.vout = in_tx.db;
    endfunction

    virtual function void calc_il;
        if(out_tx.A == 3'b001)
            out_tx.iin = in_tx.db;
        else if(out_tx.A == 3'b011)
            out_tx.iout = in_tx.db;
    endfunction
    
	virtual function void next_state();
		case (states_h)
			S4 : begin
				if(~in_tx.EOC)
					states_h = S5;
			end
			S8 : begin
				count++;
				if (count == 3'b100) begin
					states_h = S9;
				    count = 3'b000;
				end
				else
					states_h = S3;
			end
			S9 : begin
				if (in_tx.read)
                    states_h = S3;
			end
            default : begin
                count_states = states_h;
                count_states++;
                states_h = states'(count_states);
            end
		endcase
	
	endfunction : next_state

	virtual task run_phase(uvm_phase phase);
	
        forever begin
			tx_collected.get(in_tx);

			if(in_tx.reset == 0)
			begin
				states_h = S0;
				count = 3'b000;
        		weight = 0;
        	end

            if(!weight) begin

				case (states_h)

					S0: begin
						out_tx.A = 3'b0;
						out_tx.rd = 1;
						out_tx.convst = 1;
	                    weight = 0;
	                    
					end
					S1: begin
						out_tx.rd = 0;
						out_tx.convst = 1;
	                    weight = 2;
					end
					S2: begin
						out_tx.rd = 1;
						out_tx.convst = 1;
	                    weight = 15;
					end
					S3: begin
						out_tx.rd = 1;
						out_tx.convst = 0;
	                    weight = 1;
					end
					S4: begin
						out_tx.rd = 1;
						out_tx.convst = 1;
	                    weight = 0;
					end
					S5: begin
						if(out_tx.A == 3'b011)
                            out_tx.A = 3'b000;
                        else
                            out_tx.A = out_tx.A + 3'b1;
						out_tx.rd = 1;
						out_tx.convst = 1;
	                    weight = 0;
					end
					S6: begin
						out_tx.rd = 0;
						out_tx.convst = 1;
	                    weight = 1;
					end
					S7: begin
						out_tx.rd = 0;
	                    if (out_tx.A == 3'b000 || out_tx.A == 3'b010) begin
	                        calc_vl();
	                    end
	                    else 
	                        calc_il();
						out_tx.convst = 1;
	                    weight = 0;
					end
					S8: begin
						out_tx.rd = 1;
						out_tx.convst = 1;
	                    weight = 15;
					end
	                
					S9: begin
						out_tx.rd = 1;
						out_tx.convst = 1;
	                    weight = 0;
					end
					default : `uvm_info("STATE_ERROR", $sformatf("THERE IS NOT THIS STATE %0d", states_h), UVM_HIGH)
				endcase
				next_state();
            end
            else
                weight--;
			tx_exported.write(out_tx);
		end

	
	endtask : run_phase

endclass : reffmod
