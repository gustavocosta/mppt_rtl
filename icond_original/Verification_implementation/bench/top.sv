module top;
    
    // Parameters
    
    // Variables

	logic clk;
	logic reset;

	// Interface

    dut_if dut_if_h(clk, reset);

	// DUV

    dut dut_h(dut_if_h);

	initial begin
		clk = 1'b1;
		forever #5 clk = !clk;
	end

	initial begin
		uvm_config_db #(virtual dut_if)::set(null, "uvm_test_top.*", "vif", dut_if_h);
		run_test();
	end

endmodule
