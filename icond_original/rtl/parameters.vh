//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:50:15 03/16/2017 
// Design Name: 
// Module Name:    parameters 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`ifndef _parameters_vh_
`define _parameters_vh_

	// LED RELATED
	localparam LED_OFF =	 2'b00;	// Led is Off
	localparam LED_ON = 	 2'b01;	// Led is On
	localparam BLINK_05HZ =  2'b10;	// Blink for each 2 seconds 
	localparam BLINK_2HZ =	 2'b11;	// Blink for each 0.5 seconds
	
	// NCO PARAMETERS
	localparam NCO_MIN 				= 1;
	localparam NCO_START 			= 1;
	localparam NCO_MAX 				= 29500;
	localparam WARMUP_TIME			= 64;
	localparam CURRENT_MODE			= 4;
	localparam ERR_MAX			   = 1023;
	localparam ERR_MIN				= -1023;
	
	// BATTERY STATES
	localparam IDLE				= 3'b000;
	localparam FAULT				= 3'b001;
	localparam DONE				= 3'b010;
	localparam PRECHARGE			= 3'b011;
	localparam CHARGE				= 3'b100;
	localparam FLOAT				= 3'b101;
	
	// BATTERY SPECIFIC PARAMETERS
	localparam CHARGING_VOLTAGE		= 674;		// 14.5 V
	localparam FLOATING_VOLTAGE		= 628;		// 13.5 V
	localparam TOPPING_VOLTAGE			= 586;		// 12.6 V
	localparam CUTOFF_VOLTAGE			= 488;		// 10.5 V
	localparam ILIM_PRECHARGE			= 41;        //0,39 //1023;		// 6 A
	localparam ILIM						= 41;		// 6 A
	localparam IFLOAT						= 41;     //102;		// 0.6 A
	localparam PRECHARGE_TIME			= 600;		// 600 sec
	localparam FLOAT_TIME				= 14400;		// 4 h
	localparam FLOAT_RELAX_TIME		= 60;			// 1 m
	localparam IFLAT_COUNT				= 600;
	localparam IMIN_UPDATE				= 5;
	localparam I_BAT_DETECT 			= 16;
	localparam VBAT_DETECTION 			= 1640;
	//localparam CHARGE_TIME 			= 43200;
	//localparam CAPACITY 				= 4000;
	
	// MPPT E MAIN
	localparam MPPT_INTERVAL 		= 9259; // 100000000/(40*270)
	localparam MPPT_AVERAGE 		= 4; 	// adjust how many samples we want
	localparam TRACK_DELAY 			= 2; 	// debouncing between the two modes
	localparam SECOND_COUNT 		= 4000000; //976 //baybe 23053
	localparam SECOND_COUNT_START = 999; //999
	localparam MPPT_STEP 			= 19;	// 0.5v
	
	// BIND PARAMETERS
	// localparam ISENSE				= iout;
	// localparam VSENSE				= vout;
	// localparam ITOP					= IFLOAT
	// localparam CONSTANT_VOLTAGE		= (!cmode) = (cmode == 0)

`endif
