`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	15:47:14 03/06/2017
// Design Name:		Gustavo Costa
// Module Name:		nco
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Revision 0.10 - Making two NCO signals to control both transistors
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module nco #(parameter ACCUM_SIZE = 23)  		// Bits quantity in accumulator
(
	input wire 			clk,					// Input clock
	input wire 			reset,				// Input reset
	input wire [14:0]	increment,			// How much to add in the acumulator
	output reg [1:0]	nco_signal			// The pulse
);

	reg [ACCUM_SIZE - 1:0] accum; 		// Store accumulator value
	reg [7:0] pulse_clk_cnt, 				// Pulse clock counter. Count number of clocks since pulse started.
			pulse_clk_max;						// Pulse clock max. When counter reach this, pulse ends.

	reg state; /* 
				* 0 = Waiting for acummulator to overflow
				* 1 = Waiting for the pulse to end
				*/
				
	initial begin
		init();
	end
			
	task init;
		begin
			accum = 0;
			pulse_clk_max = 8'd200;
			pulse_clk_cnt = pulse_clk_max;
			state = 1'b01;
			nco_signal = 2'b01;
		end
	endtask
	
	always @(posedge clk or negedge reset) begin
		if (~reset) begin
			init();
		end
		else begin
			case( state )
				1'b0:	begin													// Waiting for the overflow
							if ( accum + increment < accum ) 		// Overflow occurred
							begin
								accum = 0;	
								nco_signal[1] = 1'b1;					// Activate hgate pulse 
								nco_signal[0] = 1'b0;					//	Deactivate lgate pulse
								pulse_clk_cnt = pulse_clk_max;  		// Set counter
								state = 1;									// Wait for the pulse to end
							end 	
							else 												// Still accumulatng
								accum = accum + increment;
						end
					
				1'b1:	begin													// Waiting for the pulse to end
							accum = accum + increment;			
							if ( pulse_clk_cnt > 0 ) 					// Pulse still not ended...
								pulse_clk_cnt = pulse_clk_cnt - 1;	// Decrement counter
							else begin   									// Pulse ended.
								nco_signal[1] = 1'b0;					// Deactivate hgate pulse, 
								nco_signal[0] = 1'b1;					//	Activate lgate pulse
								state = 1'b0;								// Wait for the increment to overflow.
							end	
						end
			endcase
		end
	end
endmodule