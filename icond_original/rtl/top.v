`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	20:03:59 07/08/2017
// Design Name:		Gustavo Costa
// Module Name:		top
// Project Name:		Implementação de um controlador MPPT utilizando FPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Revision 0.10 - Added an button debounce function and making two NCO signals to control both transistors
// Revision 0.20 - Added single_pulse function
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
    input clk,
    input button,
    input [7:0] db,
    input eoc,
    output [2:0] a,
    output convst,
    output cs,
    output rd,
    output led,
    output [1:0] nco_signal,
	 output button_out,
	 output button_debounce_out,
	 output [2:0] battery_state,
	 output led2
    );
	

	wire [1:0] led_s;
	wire [7:0] vin, iin, vout, iout;
	wire [14:0] increment;
	wire done, read, button_debounce, pulse;

	assign button_debounce_out = pulse;
	assign button_out = button;
	assign led2 = led;
	
	debouncer debouncer_module(clk, button, button_debounce);
	sigle_pulse pulse_module(clk, button_debounce, pulse);
	
	read_adc read_adc_module(clk, pulse, db, eoc, read, a, convst,	cs, rd, done, vin, iin,	vout, iout);
	led_blink led_module (clk, led_s, led); 
	nco nco_module(clk, pulse, increment, nco_signal);
	main main_module(clk, pulse, done, vin, iin, vout, iout, read, led_s, increment, battery_state);

endmodule

module debouncer(
    input clk, //this is the clock provided
    input PB,  //this is the input to be debounced
    output reg PB_state  //this is the debounced switch
);
/*This module debounces the pushbutton PB.
 *It can be added to your project files and called as is:
 *DO NOT EDIT THIS MODULE
 */

// Synchronize the switch input to the clock
reg PB_sync_0;
always @(posedge clk) PB_sync_0 <= PB;
reg PB_sync_1;
always @(posedge clk) PB_sync_1 <= PB_sync_0;

// Debounce the switch
reg [28:0] PB_cnt;
always @(posedge clk)
if(PB_state==PB_sync_1)
    PB_cnt <= 0;
else
begin
    PB_cnt <= PB_cnt + 1'b1;  
    if(PB_cnt == 28'hf42400) PB_state <= ~PB_state;  
end
endmodule


// Generate a single pulse when the button is pressed
module sigle_pulse
(
	input clock, ready,
	output reg pulse
);
	reg previous;
	
	initial begin
		previous = 1;
		pulse = 1;
	end
	 
	always @(negedge clock) begin
		if(pulse == 0)
		begin
			pulse = 1;
		end
		else
		if((ready == 0) && (previous == 1))
		begin
			pulse = 0;
			previous = 0;
		end
		else
		if((ready == 1) && (previous == 0))
		begin
			previous = 1;
		end
	end
	
endmodule
