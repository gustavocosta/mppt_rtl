`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    08:02:21 08/05/2017 
// Design Name: 
// Module Name:    led_test_sw 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module led_test_sw(
    input SW0,
    input SW1,
    input SW2,
    input SW3,
    input CLK,
    output [7:0] LED
    );
	
	/* Led output and its states
		0 = OFF
		1 = ON
		2 = BLINK AT 0.5 HZ
		3 = BLINK AT 2 HZ
	*/
	wire led;
	reg [1:0] next_led_s, led_s;
	
	
	// Instantiate the led_blink.v module
	led_blink led_module (CLK, led_s, led); 
	
	// Assign all the the eight FPGA embedded LEDs to the module output led
	assign LED = {8{led}};
	
	// Set initial state to 0 (led off)
	initial begin
		led_s = 0;
		next_led_s = 0;
	end
	
	// Switches are pullup. When one of them is pressed, it goes logic level 0.
	always @ (negedge SW0 or negedge SW1 or negedge SW2 or negedge SW3) begin
		if ( ~SW0 )
			next_led_s = 0;
		else if (~SW1)
			next_led_s = 1;
		else if (~SW2)
			next_led_s = 2;
		else if (~SW3)
			next_led_s = 3;
	end
	
	// At each clock posedge, update the led state.
	always @ (posedge CLK) begin
		led_s <= next_led_s;
	end
endmodule
