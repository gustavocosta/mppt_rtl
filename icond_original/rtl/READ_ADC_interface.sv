interface dut_if(input clk, reset);

    // In signals

    
    logic [7:0] db;
    logic EOC, read;

    // Out signals

    logic [7:0] vin, iin, vout, iout;
    logic [2:0] A;
    logic convst, rd, done;
endinterface 
