`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	13:56:28 03/06/2017
// Design Name:		Gustavo Costa
// Module Name:		led_blink
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "parameters.vh"
module led_blink(
    input wire clk,			// Clock input
    input wire [1:0] led_s,	// Led state.  Described by definitions at start of code.
    output reg led			// Led output. Active high.
);


	// Definitions for led states
//	localparam LED_OFF =	 2'b00;	// Led is Off
//	localparam LED_ON = 	 2'b01;	// Led is On
//	localparam BLINK_05HZ =  2'b10;	// Blink for each 2 seconds 
//	localparam BLINK_2HZ =	 2'b11;	// Blink for each 0.5 seconds

	// Led Counter. When zero, makes the LED output toggle.
	reg [28:0] led_cnt;
	// Max count. Depends on the desired frequency.
	reg [28:0] max_cnt;
	
	// Task for updating count
	task update_count;
	begin
		if ( led_cnt > 0 ) 
			led_cnt = led_cnt - 1;
		else begin
			led_cnt = max_cnt;
			led = ~led;
		end
	end
	endtask

	// Initially, make output and count equal to zero
	initial begin
		led = 0;
		led_cnt = 29'd0;
	end
	
	// Main Loop
	always @(posedge clk) begin
		case ( led_s ) // Check the led state input
			LED_OFF:	led = 0;	
			LED_ON:	led = 1;
			BLINK_05HZ:	begin	// Blink each 2 seconds
				max_cnt = 29'd210_000_000;
				update_count();
			end	
			BLINK_2HZ: begin	// Blink each 0.5 seconds
				max_cnt = 29'd49_000_000;
				update_count(); 
			end
		endcase
	end
	
endmodule
