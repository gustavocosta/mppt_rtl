`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	20:20:09 03/06/2017
// Design Name:		Gustavo Costa
// Module Name:		read_adc
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module read_adc(
	input wire clk,			// Clock is posedge
	input wire reset,			// Reset is negedge
	input wire [7:0] db,		// Data Bus. Receives data from conversor.
	input wire eoc,			// Active low. End Of Conversion. Indicates that data is avaliable at DataBus. 
	input wire read,			// Active high. Signal to start conversion 
	output reg [2:0] a,		// Selector from conversor. {000, 001, 010, 011} = {vin, iin, vout, iout}
	output reg convst,		// Active in negedge (low). Conversion Start.
	output reg cs,				// Active low. Chip Select. Will be tied logically to ground.
	output reg rd,				// Active low. Used to read "a" variable and activate data read buffers in ADC.
	output reg done,			// Active high. Indicates to main that all 4 reads are ready to be read.
	output wire [7:0] vin,	// Tension Input from circuit.
	output wire [7:0] iin,	// Current Input from circuit.
	output wire [7:0] vout,	// Tension Output from circuit.
	output wire [7:0] iout	// Current Output from circuit.
);

	reg [7:0] data_out [0:3];
	reg [3:0] state, next_state;// Describe state info.
	reg [4:0] wait_clk;			// Used to wait a specific number of clocks before going to next state
	reg [4:0] next_wait_clk;	// Define next value for wait_clk

	assign vin = data_out[0];
	assign iin = data_out[1];
	assign vout = data_out[2];
	assign iout = data_out[3];

	always @(state) begin
		case( state )
			// ================> Output of form: 		   {  a        convs   cs   rd  done, wait_clk }
			4'd0:  { a, convst, cs, rd, done, next_wait_clk} <= { 3'b000,  1'b1, 1'b0, 1'b1, 1'b0, 5'd2	};	// Needs 2  clock cycle. Initial state. 
			4'd1:  { a, convst, cs, rd, done, next_wait_clk} <= { 3'b000,  1'b1, 1'b0, 1'b0, 1'b0, 5'd15	};	// Needs 3  clock cycles. Read ADC input and store at buffers.
			4'd2:  { a, convst, cs, rd, done, next_wait_clk} <= { 3'b000,  1'b1, 1'b0, 1'b1, 1'b0, 5'd1	};	// Needs 16 clock cycles. Finish reading from ADC input
			
			4'd3:  { a, convst, cs, rd, done, next_wait_clk} <= {a,        1'b0, 1'b0, 1'b1, 1'b0, 5'd0	};	// Needs 2  clock cycles. Initial state for loop
			4'd4:  { a, convst, cs, rd, done, next_wait_clk} <= {a,		  1'b1, 1'b0, 1'b1, 1'b0, 5'd0	};	// Activate convst. Then, waits for EOC.
			4'd5:  { a, convst, cs, rd, done, next_wait_clk} <= {a + 1'b1, 1'b1, 1'b0, 1'b1, 1'b0, 5'd1	};	// Needs 1  clock cycle. Increment a. 
			4'd6:  { a, convst, cs, rd, done, next_wait_clk} <= {a,        1'b1, 1'b0, 1'b0, 1'b0, 5'd0	};	// Needs 2 clock cycles. Activate read.
			4'd7:  { a, convst, cs, rd, done, next_wait_clk} <= {a,        1'b1, 1'b0, 1'b0, 1'b0, 5'd15	};	// Needs 1 clock cycles. Activate read.
			4'd8:  { a, convst, cs, rd, done, next_wait_clk} <= {a,        1'b1, 1'b0, 1'b1, 1'b0, 5'd1	};	// Needs 16 clock cycles. Finish reading from ADC input. Go to state 4'd3 for 3 more times.

			4'd9:  { a, convst, cs, rd, done, next_wait_clk} <= { 3'b000,  1'b1, 1'b0, 1'b1, 1'b1, 5'd2	};	// Make done output equal to 1. Wait for read input signal and make state equal to 4'd0
		endcase
	end

	// Initial state
	initial begin
			state = 4'd0;
			// "done" is initialized at "always @(state)"
			done = 0;
			data_out[0] = 8'd0;
			data_out[1] = 8'd0;
			data_out[2] = 8'd0;
			data_out[3] = 8'd0;
			wait_clk = 5'd2;
			next_wait_clk = 5'd0;
	end

	always @ (state or eoc or read)
	begin
		case (state)
			4'd4: begin								// Wait for EOC
				if (~eoc)
					next_state = 4'd5;
			end
			4'd8: begin
				if ( a < 3'b100 )						// Check if A == 4 ( data from 0 to 3 were already converted and read)
					next_state = 4'd3;
				else
					next_state = 4'd9;				// Otherwise, go to next state
			end
			4'd9: begin								// Waiting read signal. 
				if (read)
					next_state = 4'd3;
			end
			default: next_state = state + 4'd1;
		endcase
	end

	// Sequential logic
	always @ (posedge clk or negedge reset) begin
		if (~reset) begin
			state = 4'd0;
			wait_clk = 4'd2;
			data_out[0] = 8'd0;
			data_out[1] = 8'd0;
			data_out[2] = 8'd0;
			data_out[3] = 8'd0;
			// done and 'wait_clk' are initialized at always @(state) 
		end
		else begin
			if(wait_clk == 5'd0) begin
				state = next_state;
				wait_clk = next_wait_clk;
				if ( state == 4'd6 ) begin
					if ( a == 3'b0 ) begin
						data_out[3] = db;
					end
					else begin
						data_out[a - 1] = db;
					end
				end
			end
			else begin
				wait_clk = wait_clk - 1;
			end
		end
	end
endmodule