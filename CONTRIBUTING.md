Guia de contribuição
------
+ Criando nova branch através da interface:
    - Clique em "Issues > 'Nome da Issue'";
    - Clique em "New branch";
    - Lembre sempre de adicionar tasks no formato "* [ ] T__ - 'Nome da task'"(onde no lugar de '__' deve ser posto o número da task), clicando em "Edit" na Issue;
    - E de alterar a label da issue em "Issues > Board" para a label referente ao status dela.

+ Clone sua branch de trabalho atual através do comando "git clone -b 'nome do branch' --single-branch https://gitlab.com/pem-ufal/mppt.git".

+ Na hora de adicionar mudanças ao seu commit recomenda-se o uso do comando "git add -p", opções:
    - y - Enviar pedaço para a "proposta";
    - n - Ignorar pedaço;
    - q - Ignora este e todos os pedaços seguintes;
    - a - Adiciona este e todos os pedaços do arquivo;
    - d - Ignora este pedaço e todos os outros do arquivo;
    - s - Dividir o pedaço em pedaços menores.

+ Para dar "commit" em sua branch de trabalho recomenda-se que seja usado o comando "git commit -s" ou "git commit -s -m 'mensagem de commit'".

+ Usem sempre a tag "WIP: 'nome do branch' - 'T__, T__'", na primeira linha do seu commit para commits de tasks que ainda não estão finalizadas, ao finalizar uma task retirar a palavra "WIP: " da frente na mensagem do commit, e no caso de estar fazendo um commit com a issue finalizada deixe na primeira linha somente o nome do branch.
