`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:46:05 08/04/2017
// Design Name:   read_adc
// Module Name:   /home/brunolima/Projetos/MPPT/lastBranch/rtl/read_adc_tb.v
// Project Name:  mppt
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: read_adc
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module read_adc_tb;

	// Inputs
	reg clk;
	reg [7:0] db;
	reg eoc;
	reg read;

	// Outputs
	wire [2:0] a;
	wire convst;
	wire cs;
	wire rd;
	wire done;
	wire [20:0] vin;
	wire [20:0] iin;
	reg [10:0] clk_index;
	
	// Instantiate the Unit Under Test (UUT)
	read_adc uut (
		.clk(clk), 
		.db(db), 
		.eoc(eoc), 
		.read(read), 
		.a(a), 
		.convst(convst), 
		.cs(cs), 
		.rd(rd), 
		.done(done), 
		.vin(vin), 
		.iin(iin)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		db = 0;
		eoc = 1;
		read = 0;
		clk_index = 0;

		// Wait 100 ns for global reset to finish
		db = 8'b000110000;
		read = 1;

		
		#500
		eoc = 0;
		#90
		eoc = 1;
		read = 0;
	end
	
	always begin
		#5;
		clk = ~clk;
		if(clk)clk_index = clk_index + 1;
	end
      
endmodule

