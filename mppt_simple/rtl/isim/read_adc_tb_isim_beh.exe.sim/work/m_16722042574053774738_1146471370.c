/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/gustavo/Documentos/MPPT/mppt_rtl/mppt_simple/rtl/read_adc.v";
static unsigned int ng1[] = {0U, 0U};
static unsigned int ng2[] = {1283U, 0U};
static unsigned int ng3[] = {1U, 0U};
static unsigned int ng4[] = {1025U, 0U};
static unsigned int ng5[] = {2U, 0U};
static unsigned int ng6[] = {1039U, 0U};
static unsigned int ng7[] = {3U, 0U};
static unsigned int ng8[] = {4U, 0U};
static unsigned int ng9[] = {298U, 0U};
static unsigned int ng10[] = {5U, 0U};
static unsigned int ng11[] = {1281U, 0U};
static unsigned int ng12[] = {6U, 0U};
static unsigned int ng13[] = {3331U, 0U};
static unsigned int ng14[] = {7U, 0U};
static unsigned int ng15[] = {3073U, 0U};
static unsigned int ng16[] = {8U, 0U};
static unsigned int ng17[] = {3087U, 0U};
static unsigned int ng18[] = {9U, 0U};
static unsigned int ng19[] = {10U, 0U};
static unsigned int ng20[] = {2346U, 0U};
static unsigned int ng21[] = {11U, 0U};
static unsigned int ng22[] = {3329U, 0U};
static unsigned int ng23[] = {1322U, 0U};
static int ng24[] = {0, 0};
static unsigned int ng25[] = {293U, 0U};
static unsigned int ng26[] = {1074U, 0U};



static void Always_37_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    int t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;

LAB0:    t1 = (t0 + 4760U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(37, ng0);
    t2 = (t0 + 6072);
    *((int *)t2) = 1;
    t3 = (t0 + 4792);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(37, ng0);

LAB5:    xsi_set_current_line(38, ng0);
    t4 = (t0 + 3368);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);

LAB6:    t7 = ((char*)((ng1)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t7, 4);
    if (t8 == 1)
        goto LAB7;

LAB8:    t2 = ((char*)((ng3)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB9;

LAB10:    t2 = ((char*)((ng5)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB11;

LAB12:    t2 = ((char*)((ng7)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB13;

LAB14:    t2 = ((char*)((ng8)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB15;

LAB16:    t2 = ((char*)((ng10)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB17;

LAB18:    t2 = ((char*)((ng12)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB19;

LAB20:    t2 = ((char*)((ng14)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB21;

LAB22:    t2 = ((char*)((ng16)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB23;

LAB24:    t2 = ((char*)((ng18)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB25;

LAB26:    t2 = ((char*)((ng19)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB27;

LAB28:    t2 = ((char*)((ng21)));
    t8 = xsi_vlog_unsigned_case_compare(t6, 4, t2, 4);
    if (t8 == 1)
        goto LAB29;

LAB30:
LAB32:
LAB31:    xsi_set_current_line(54, ng0);
    t2 = ((char*)((ng23)));
    t3 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t3, t2, 0, 0, 8, 0LL);
    t4 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t4, t2, 8, 0, 1, 0LL);
    t5 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t5, t2, 9, 0, 1, 0LL);
    t7 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t7, t2, 10, 0, 1, 0LL);
    t9 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t9, t2, 11, 0, 3, 0LL);

LAB33:    goto LAB2;

LAB7:    xsi_set_current_line(40, ng0);
    t9 = ((char*)((ng2)));
    t10 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t10, t9, 0, 0, 8, 0LL);
    t11 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t11, t9, 8, 0, 1, 0LL);
    t12 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t12, t9, 9, 0, 1, 0LL);
    t13 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t13, t9, 10, 0, 1, 0LL);
    t14 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t14, t9, 11, 0, 3, 0LL);
    goto LAB33;

LAB9:    xsi_set_current_line(41, ng0);
    t3 = ((char*)((ng4)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB11:    xsi_set_current_line(42, ng0);
    t3 = ((char*)((ng6)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB13:    xsi_set_current_line(43, ng0);
    t3 = ((char*)((ng2)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB15:    xsi_set_current_line(44, ng0);
    t3 = ((char*)((ng9)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB17:    xsi_set_current_line(45, ng0);
    t3 = ((char*)((ng11)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB19:    xsi_set_current_line(47, ng0);
    t3 = ((char*)((ng13)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB21:    xsi_set_current_line(48, ng0);
    t3 = ((char*)((ng15)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB23:    xsi_set_current_line(49, ng0);
    t3 = ((char*)((ng17)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB25:    xsi_set_current_line(50, ng0);
    t3 = ((char*)((ng13)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB27:    xsi_set_current_line(51, ng0);
    t3 = ((char*)((ng20)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

LAB29:    xsi_set_current_line(52, ng0);
    t3 = ((char*)((ng22)));
    t4 = (t0 + 3848);
    xsi_vlogvar_wait_assign_value(t4, t3, 0, 0, 8, 0LL);
    t5 = (t0 + 2408);
    xsi_vlogvar_wait_assign_value(t5, t3, 8, 0, 1, 0LL);
    t7 = (t0 + 2248);
    xsi_vlogvar_wait_assign_value(t7, t3, 9, 0, 1, 0LL);
    t9 = (t0 + 2088);
    xsi_vlogvar_wait_assign_value(t9, t3, 10, 0, 1, 0LL);
    t10 = (t0 + 1928);
    xsi_vlogvar_wait_assign_value(t10, t3, 11, 0, 3, 0LL);
    goto LAB33;

}

static void Initial_59_1(char *t0)
{
    char *t1;
    char *t2;

LAB0:    xsi_set_current_line(59, ng0);

LAB2:    xsi_set_current_line(60, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 3368);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 4);
    xsi_set_current_line(61, ng0);
    t1 = ((char*)((ng24)));
    t2 = (t0 + 2568);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 1);
    xsi_set_current_line(62, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 3048);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 8);
    xsi_set_current_line(63, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 3208);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 8);
    xsi_set_current_line(64, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 2728);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 22);
    xsi_set_current_line(65, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 2888);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 22);
    xsi_set_current_line(66, ng0);
    t1 = ((char*)((ng5)));
    t2 = (t0 + 3688);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 8);
    xsi_set_current_line(67, ng0);
    t1 = ((char*)((ng8)));
    t2 = (t0 + 3848);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 8);
    xsi_set_current_line(68, ng0);
    t1 = ((char*)((ng1)));
    t2 = (t0 + 2248);
    xsi_vlogvar_assign_value(t2, t1, 0, 0, 1);

LAB1:    return;
}

static void Always_71_2(char *t0)
{
    char t8[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    char *t16;
    char *t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;

LAB0:    t1 = (t0 + 5256U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(71, ng0);
    t2 = (t0 + 6088);
    *((int *)t2) = 1;
    t3 = (t0 + 5288);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(72, ng0);

LAB5:    xsi_set_current_line(73, ng0);
    t4 = (t0 + 3368);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = ((char*)((ng3)));
    memset(t8, 0, 8);
    xsi_vlog_unsigned_add(t8, 4, t6, 4, t7, 4);
    t9 = (t0 + 3528);
    xsi_vlogvar_assign_value(t9, t8, 0, 0, 4);
    xsi_set_current_line(74, ng0);
    t2 = (t0 + 3528);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng21)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    if (*((unsigned int *)t6) != 0)
        goto LAB7;

LAB6:    t7 = (t5 + 4);
    if (*((unsigned int *)t7) != 0)
        goto LAB7;

LAB10:    if (*((unsigned int *)t4) > *((unsigned int *)t5))
        goto LAB8;

LAB9:    t10 = (t8 + 4);
    t11 = *((unsigned int *)t10);
    t12 = (~(t11));
    t13 = *((unsigned int *)t8);
    t14 = (t13 & t12);
    t15 = (t14 != 0);
    if (t15 > 0)
        goto LAB11;

LAB12:
LAB13:    xsi_set_current_line(78, ng0);
    t2 = (t0 + 3368);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng5)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t11 = *((unsigned int *)t4);
    t12 = *((unsigned int *)t5);
    t13 = (t11 ^ t12);
    t14 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t18 = (t14 ^ t15);
    t19 = (t13 | t18);
    t20 = *((unsigned int *)t6);
    t21 = *((unsigned int *)t7);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB18;

LAB15:    if (t22 != 0)
        goto LAB17;

LAB16:    *((unsigned int *)t8) = 1;

LAB18:    t10 = (t8 + 4);
    t25 = *((unsigned int *)t10);
    t26 = (~(t25));
    t27 = *((unsigned int *)t8);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB19;

LAB20:
LAB21:    xsi_set_current_line(84, ng0);
    t2 = (t0 + 3368);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng16)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t11 = *((unsigned int *)t4);
    t12 = *((unsigned int *)t5);
    t13 = (t11 ^ t12);
    t14 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t18 = (t14 ^ t15);
    t19 = (t13 | t18);
    t20 = *((unsigned int *)t6);
    t21 = *((unsigned int *)t7);
    t22 = (t20 | t21);
    t23 = (~(t22));
    t24 = (t19 & t23);
    if (t24 != 0)
        goto LAB26;

LAB23:    if (t22 != 0)
        goto LAB25;

LAB24:    *((unsigned int *)t8) = 1;

LAB26:    t10 = (t8 + 4);
    t25 = *((unsigned int *)t10);
    t26 = (~(t25));
    t27 = *((unsigned int *)t8);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB27;

LAB28:
LAB29:    goto LAB2;

LAB7:    t9 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB9;

LAB8:    *((unsigned int *)t8) = 1;
    goto LAB9;

LAB11:    xsi_set_current_line(74, ng0);

LAB14:    xsi_set_current_line(75, ng0);
    t16 = ((char*)((ng1)));
    t17 = (t0 + 3528);
    xsi_vlogvar_assign_value(t17, t16, 0, 0, 4);
    goto LAB13;

LAB17:    t9 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB18;

LAB19:    xsi_set_current_line(78, ng0);

LAB22:    xsi_set_current_line(79, ng0);
    t16 = (t0 + 1208U);
    t17 = *((char **)t16);
    t16 = (t0 + 3208);
    xsi_vlogvar_assign_value(t16, t17, 0, 0, 8);
    xsi_set_current_line(81, ng0);
    t2 = (t0 + 3208);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng25)));
    memset(t8, 0, 8);
    xsi_vlog_unsigned_multiply(t8, 22, t4, 8, t5, 22);
    t6 = (t0 + 2888);
    xsi_vlogvar_assign_value(t6, t8, 0, 0, 22);
    goto LAB21;

LAB25:    t9 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB26;

LAB27:    xsi_set_current_line(84, ng0);

LAB30:    xsi_set_current_line(85, ng0);
    t16 = (t0 + 1208U);
    t17 = *((char **)t16);
    t16 = (t0 + 3048);
    xsi_vlogvar_assign_value(t16, t17, 0, 0, 8);
    xsi_set_current_line(87, ng0);
    t2 = (t0 + 3048);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng26)));
    memset(t8, 0, 8);
    xsi_vlog_unsigned_multiply(t8, 22, t4, 8, t5, 22);
    t6 = (t0 + 2728);
    xsi_vlogvar_assign_value(t6, t8, 0, 0, 22);
    goto LAB29;

}

static void Always_91_3(char *t0)
{
    char t6[8];
    char t30[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t7;
    char *t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    char *t21;
    char *t22;
    unsigned int t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    char *t28;
    char *t29;
    char *t31;
    char *t32;
    unsigned int t33;
    unsigned int t34;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    unsigned int t39;
    unsigned int t40;
    unsigned int t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    char *t45;
    char *t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    char *t52;
    char *t53;

LAB0:    t1 = (t0 + 5504U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(91, ng0);
    t2 = (t0 + 6104);
    *((int *)t2) = 1;
    t3 = (t0 + 5536);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(91, ng0);

LAB5:    xsi_set_current_line(92, ng0);
    t4 = (t0 + 1528U);
    t5 = *((char **)t4);
    t4 = ((char*)((ng1)));
    memset(t6, 0, 8);
    t7 = (t5 + 4);
    t8 = (t4 + 4);
    t9 = *((unsigned int *)t5);
    t10 = *((unsigned int *)t4);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB9;

LAB6:    if (t18 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t6) = 1;

LAB9:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB10;

LAB11:
LAB12:    xsi_set_current_line(96, ng0);
    t2 = (t0 + 3368);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng5)));
    memset(t6, 0, 8);
    t7 = (t4 + 4);
    t8 = (t5 + 4);
    t9 = *((unsigned int *)t4);
    t10 = *((unsigned int *)t5);
    t11 = (t9 ^ t10);
    t12 = *((unsigned int *)t7);
    t13 = *((unsigned int *)t8);
    t14 = (t12 ^ t13);
    t15 = (t11 | t14);
    t16 = *((unsigned int *)t7);
    t17 = *((unsigned int *)t8);
    t18 = (t16 | t17);
    t19 = (~(t18));
    t20 = (t15 & t19);
    if (t20 != 0)
        goto LAB17;

LAB14:    if (t18 != 0)
        goto LAB16;

LAB15:    *((unsigned int *)t6) = 1;

LAB17:    t22 = (t6 + 4);
    t23 = *((unsigned int *)t22);
    t24 = (~(t23));
    t25 = *((unsigned int *)t6);
    t26 = (t25 & t24);
    t27 = (t26 != 0);
    if (t27 > 0)
        goto LAB18;

LAB19:
LAB20:    goto LAB2;

LAB8:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(92, ng0);

LAB13:    xsi_set_current_line(93, ng0);
    t28 = ((char*)((ng1)));
    t29 = (t0 + 2568);
    xsi_vlogvar_assign_value(t29, t28, 0, 0, 1);
    goto LAB12;

LAB16:    t21 = (t6 + 4);
    *((unsigned int *)t6) = 1;
    *((unsigned int *)t21) = 1;
    goto LAB17;

LAB18:    xsi_set_current_line(96, ng0);

LAB21:    xsi_set_current_line(97, ng0);
    t28 = (t0 + 1528U);
    t29 = *((char **)t28);
    t28 = ((char*)((ng3)));
    memset(t30, 0, 8);
    t31 = (t29 + 4);
    t32 = (t28 + 4);
    t33 = *((unsigned int *)t29);
    t34 = *((unsigned int *)t28);
    t35 = (t33 ^ t34);
    t36 = *((unsigned int *)t31);
    t37 = *((unsigned int *)t32);
    t38 = (t36 ^ t37);
    t39 = (t35 | t38);
    t40 = *((unsigned int *)t31);
    t41 = *((unsigned int *)t32);
    t42 = (t40 | t41);
    t43 = (~(t42));
    t44 = (t39 & t43);
    if (t44 != 0)
        goto LAB25;

LAB22:    if (t42 != 0)
        goto LAB24;

LAB23:    *((unsigned int *)t30) = 1;

LAB25:    t46 = (t30 + 4);
    t47 = *((unsigned int *)t46);
    t48 = (~(t47));
    t49 = *((unsigned int *)t30);
    t50 = (t49 & t48);
    t51 = (t50 != 0);
    if (t51 > 0)
        goto LAB26;

LAB27:
LAB28:    goto LAB20;

LAB24:    t45 = (t30 + 4);
    *((unsigned int *)t30) = 1;
    *((unsigned int *)t45) = 1;
    goto LAB25;

LAB26:    xsi_set_current_line(97, ng0);

LAB29:    xsi_set_current_line(98, ng0);
    t52 = ((char*)((ng3)));
    t53 = (t0 + 2568);
    xsi_vlogvar_assign_value(t53, t52, 0, 0, 1);
    goto LAB28;

}

static void Always_104_4(char *t0)
{
    char t8[8];
    char t34[8];
    char t39[8];
    char t55[8];
    char t63[8];
    char t99[8];
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t9;
    char *t10;
    unsigned int t11;
    unsigned int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    char *t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    unsigned int t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    unsigned int t35;
    unsigned int t36;
    unsigned int t37;
    unsigned int t38;
    char *t40;
    char *t41;
    unsigned int t42;
    unsigned int t43;
    unsigned int t44;
    unsigned int t45;
    unsigned int t46;
    unsigned int t47;
    unsigned int t48;
    unsigned int t49;
    unsigned int t50;
    unsigned int t51;
    unsigned int t52;
    unsigned int t53;
    char *t54;
    char *t56;
    unsigned int t57;
    unsigned int t58;
    unsigned int t59;
    unsigned int t60;
    unsigned int t61;
    char *t62;
    unsigned int t64;
    unsigned int t65;
    unsigned int t66;
    char *t67;
    char *t68;
    char *t69;
    unsigned int t70;
    unsigned int t71;
    unsigned int t72;
    unsigned int t73;
    unsigned int t74;
    unsigned int t75;
    unsigned int t76;
    char *t77;
    char *t78;
    unsigned int t79;
    unsigned int t80;
    unsigned int t81;
    int t82;
    unsigned int t83;
    unsigned int t84;
    unsigned int t85;
    int t86;
    unsigned int t87;
    unsigned int t88;
    unsigned int t89;
    unsigned int t90;
    char *t91;
    unsigned int t92;
    unsigned int t93;
    unsigned int t94;
    unsigned int t95;
    unsigned int t96;
    char *t97;
    char *t98;
    char *t100;
    char *t101;
    unsigned int t102;
    unsigned int t103;
    unsigned int t104;
    unsigned int t105;
    unsigned int t106;
    unsigned int t107;
    unsigned int t108;
    unsigned int t109;
    unsigned int t110;
    unsigned int t111;
    unsigned int t112;
    unsigned int t113;
    char *t114;
    char *t115;
    unsigned int t116;
    unsigned int t117;
    unsigned int t118;
    unsigned int t119;
    unsigned int t120;
    char *t121;
    char *t122;
    char *t123;
    char *t124;

LAB0:    t1 = (t0 + 5752U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(104, ng0);
    t2 = (t0 + 6120);
    *((int *)t2) = 1;
    t3 = (t0 + 5784);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(104, ng0);

LAB5:    xsi_set_current_line(105, ng0);
    t4 = (t0 + 3688);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = ((char*)((ng1)));
    memset(t8, 0, 8);
    t9 = (t6 + 4);
    t10 = (t7 + 4);
    t11 = *((unsigned int *)t6);
    t12 = *((unsigned int *)t7);
    t13 = (t11 ^ t12);
    t14 = *((unsigned int *)t9);
    t15 = *((unsigned int *)t10);
    t16 = (t14 ^ t15);
    t17 = (t13 | t16);
    t18 = *((unsigned int *)t9);
    t19 = *((unsigned int *)t10);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB9;

LAB6:    if (t20 != 0)
        goto LAB8;

LAB7:    *((unsigned int *)t8) = 1;

LAB9:    t24 = (t8 + 4);
    t25 = *((unsigned int *)t24);
    t26 = (~(t25));
    t27 = *((unsigned int *)t8);
    t28 = (t27 & t26);
    t29 = (t28 != 0);
    if (t29 > 0)
        goto LAB10;

LAB11:    xsi_set_current_line(109, ng0);

LAB14:    xsi_set_current_line(110, ng0);
    t2 = (t0 + 3688);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng3)));
    memset(t8, 0, 8);
    xsi_vlog_unsigned_minus(t8, 8, t4, 8, t5, 8);
    t6 = (t0 + 3688);
    xsi_vlogvar_assign_value(t6, t8, 0, 0, 8);
    xsi_set_current_line(112, ng0);
    t2 = (t0 + 3368);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = ((char*)((ng10)));
    memset(t8, 0, 8);
    t6 = (t4 + 4);
    t7 = (t5 + 4);
    t11 = *((unsigned int *)t4);
    t12 = *((unsigned int *)t5);
    t13 = (t11 ^ t12);
    t14 = *((unsigned int *)t6);
    t15 = *((unsigned int *)t7);
    t16 = (t14 ^ t15);
    t17 = (t13 | t16);
    t18 = *((unsigned int *)t6);
    t19 = *((unsigned int *)t7);
    t20 = (t18 | t19);
    t21 = (~(t20));
    t22 = (t17 & t21);
    if (t22 != 0)
        goto LAB18;

LAB15:    if (t20 != 0)
        goto LAB17;

LAB16:    *((unsigned int *)t8) = 1;

LAB18:    memset(t34, 0, 8);
    t10 = (t8 + 4);
    t25 = *((unsigned int *)t10);
    t26 = (~(t25));
    t27 = *((unsigned int *)t8);
    t28 = (t27 & t26);
    t29 = (t28 & 1U);
    if (t29 != 0)
        goto LAB19;

LAB20:    if (*((unsigned int *)t10) != 0)
        goto LAB21;

LAB22:    t24 = (t34 + 4);
    t35 = *((unsigned int *)t34);
    t36 = (!(t35));
    t37 = *((unsigned int *)t24);
    t38 = (t36 || t37);
    if (t38 > 0)
        goto LAB23;

LAB24:    memcpy(t63, t34, 8);

LAB25:    t91 = (t63 + 4);
    t92 = *((unsigned int *)t91);
    t93 = (~(t92));
    t94 = *((unsigned int *)t63);
    t95 = (t94 & t93);
    t96 = (t95 != 0);
    if (t96 > 0)
        goto LAB37;

LAB38:
LAB39:
LAB12:    goto LAB2;

LAB8:    t23 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t23) = 1;
    goto LAB9;

LAB10:    xsi_set_current_line(105, ng0);

LAB13:    xsi_set_current_line(106, ng0);
    t30 = (t0 + 3848);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t0 + 3688);
    xsi_vlogvar_assign_value(t33, t32, 0, 0, 8);
    xsi_set_current_line(107, ng0);
    t2 = (t0 + 3528);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 3368);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 4);
    goto LAB12;

LAB17:    t9 = (t8 + 4);
    *((unsigned int *)t8) = 1;
    *((unsigned int *)t9) = 1;
    goto LAB18;

LAB19:    *((unsigned int *)t34) = 1;
    goto LAB22;

LAB21:    t23 = (t34 + 4);
    *((unsigned int *)t34) = 1;
    *((unsigned int *)t23) = 1;
    goto LAB22;

LAB23:    t30 = (t0 + 3368);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = ((char*)((ng21)));
    memset(t39, 0, 8);
    t40 = (t32 + 4);
    t41 = (t33 + 4);
    t42 = *((unsigned int *)t32);
    t43 = *((unsigned int *)t33);
    t44 = (t42 ^ t43);
    t45 = *((unsigned int *)t40);
    t46 = *((unsigned int *)t41);
    t47 = (t45 ^ t46);
    t48 = (t44 | t47);
    t49 = *((unsigned int *)t40);
    t50 = *((unsigned int *)t41);
    t51 = (t49 | t50);
    t52 = (~(t51));
    t53 = (t48 & t52);
    if (t53 != 0)
        goto LAB29;

LAB26:    if (t51 != 0)
        goto LAB28;

LAB27:    *((unsigned int *)t39) = 1;

LAB29:    memset(t55, 0, 8);
    t56 = (t39 + 4);
    t57 = *((unsigned int *)t56);
    t58 = (~(t57));
    t59 = *((unsigned int *)t39);
    t60 = (t59 & t58);
    t61 = (t60 & 1U);
    if (t61 != 0)
        goto LAB30;

LAB31:    if (*((unsigned int *)t56) != 0)
        goto LAB32;

LAB33:    t64 = *((unsigned int *)t34);
    t65 = *((unsigned int *)t55);
    t66 = (t64 | t65);
    *((unsigned int *)t63) = t66;
    t67 = (t34 + 4);
    t68 = (t55 + 4);
    t69 = (t63 + 4);
    t70 = *((unsigned int *)t67);
    t71 = *((unsigned int *)t68);
    t72 = (t70 | t71);
    *((unsigned int *)t69) = t72;
    t73 = *((unsigned int *)t69);
    t74 = (t73 != 0);
    if (t74 == 1)
        goto LAB34;

LAB35:
LAB36:    goto LAB25;

LAB28:    t54 = (t39 + 4);
    *((unsigned int *)t39) = 1;
    *((unsigned int *)t54) = 1;
    goto LAB29;

LAB30:    *((unsigned int *)t55) = 1;
    goto LAB33;

LAB32:    t62 = (t55 + 4);
    *((unsigned int *)t55) = 1;
    *((unsigned int *)t62) = 1;
    goto LAB33;

LAB34:    t75 = *((unsigned int *)t63);
    t76 = *((unsigned int *)t69);
    *((unsigned int *)t63) = (t75 | t76);
    t77 = (t34 + 4);
    t78 = (t55 + 4);
    t79 = *((unsigned int *)t77);
    t80 = (~(t79));
    t81 = *((unsigned int *)t34);
    t82 = (t81 & t80);
    t83 = *((unsigned int *)t78);
    t84 = (~(t83));
    t85 = *((unsigned int *)t55);
    t86 = (t85 & t84);
    t87 = (~(t82));
    t88 = (~(t86));
    t89 = *((unsigned int *)t69);
    *((unsigned int *)t69) = (t89 & t87);
    t90 = *((unsigned int *)t69);
    *((unsigned int *)t69) = (t90 & t88);
    goto LAB36;

LAB37:    xsi_set_current_line(112, ng0);

LAB40:    xsi_set_current_line(113, ng0);
    t97 = (t0 + 1368U);
    t98 = *((char **)t97);
    t97 = ((char*)((ng24)));
    memset(t99, 0, 8);
    t100 = (t98 + 4);
    t101 = (t97 + 4);
    t102 = *((unsigned int *)t98);
    t103 = *((unsigned int *)t97);
    t104 = (t102 ^ t103);
    t105 = *((unsigned int *)t100);
    t106 = *((unsigned int *)t101);
    t107 = (t105 ^ t106);
    t108 = (t104 | t107);
    t109 = *((unsigned int *)t100);
    t110 = *((unsigned int *)t101);
    t111 = (t109 | t110);
    t112 = (~(t111));
    t113 = (t108 & t112);
    if (t113 != 0)
        goto LAB44;

LAB41:    if (t111 != 0)
        goto LAB43;

LAB42:    *((unsigned int *)t99) = 1;

LAB44:    t115 = (t99 + 4);
    t116 = *((unsigned int *)t115);
    t117 = (~(t116));
    t118 = *((unsigned int *)t99);
    t119 = (t118 & t117);
    t120 = (t119 != 0);
    if (t120 > 0)
        goto LAB45;

LAB46:
LAB47:    goto LAB39;

LAB43:    t114 = (t99 + 4);
    *((unsigned int *)t99) = 1;
    *((unsigned int *)t114) = 1;
    goto LAB44;

LAB45:    xsi_set_current_line(113, ng0);

LAB48:    xsi_set_current_line(114, ng0);
    t121 = (t0 + 3848);
    t122 = (t121 + 56U);
    t123 = *((char **)t122);
    t124 = (t0 + 3688);
    xsi_vlogvar_assign_value(t124, t123, 0, 0, 8);
    xsi_set_current_line(115, ng0);
    t2 = (t0 + 3528);
    t3 = (t2 + 56U);
    t4 = *((char **)t3);
    t5 = (t0 + 3368);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 4);
    goto LAB47;

}


extern void work_m_16722042574053774738_1146471370_init()
{
	static char *pe[] = {(void *)Always_37_0,(void *)Initial_59_1,(void *)Always_71_2,(void *)Always_91_3,(void *)Always_104_4};
	xsi_register_didat("work_m_16722042574053774738_1146471370", "isim/read_adc_tb_isim_beh.exe.sim/work/m_16722042574053774738_1146471370.didat");
	xsi_register_executes(pe);
}
