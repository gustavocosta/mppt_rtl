`timescale 1ns / 1ps

module spi_slave #(parameter DATA_SIZE = 8)
(
		// Entradas
		input                     							CLK_IN,
		input                     							CLK_SPI_IN,
		input                     							SS_IN,
		input            							         RST_IN,
		input 			[0:DATA_SIZE-1] 	DATA_IN,
		input                     							MOSI,

		// Saídas
		output reg                 						MISO,
		output reg [0:DATA_SIZE-1] 		DATA_OUT,
		output reg rising_ss
);

// Variaveis internas
 reg [1:0] buffer_clk;
 reg [1:0] buffer_ss;
 reg [4:0] counter;
 reg [0:DATA_SIZE-1] data_MISO;
 reg [0:DATA_SIZE-1] data_MOSI;
 reg rising_clk;
 reg drop_clk;
 reg drop_ss;

// Contador
	always @ ( posedge CLK_IN or negedge RST_IN )
	begin
		if( ~RST_IN || SS_IN )		begin
				counter <= {DATA_SIZE{1'b0}};
		end
		else if ( drop_clk )		begin
				counter <= counter + 1'b1;
		end
		else		begin
				counter <= counter;
		end
	end

// Buffer dos últimos dois clks e chipseletor
	always @ ( posedge CLK_IN or negedge RST_IN )
	begin
			if( ~RST_IN )	 begin
				buffer_clk <= 2'b00;
				buffer_ss <= 2'b00;
			end
			else 	begin
				buffer_clk <= (buffer_clk<<1) | (CLK_SPI_IN) ; 
				buffer_ss <= (buffer_ss<<1) | (SS_IN) ; 
			end
	end

//	Detectando subida e descida do ss
	always @ ( posedge CLK_IN or negedge RST_IN )
	begin
		if( ~RST_IN )	begin
			rising_clk <= 0;
			drop_clk <= 0;		
			rising_ss <= 0;
			drop_ss <= 0;	
		end
		else begin
			rising_clk <= (buffer_clk == 2'b01);		// Subida do clk
			drop_clk <= (buffer_clk == 2'b10);			// Decida do clk
			rising_ss <= (buffer_ss == 2'b01);			// Subida do ss
			drop_ss <= (buffer_ss == 2'b10);			// Decida do ss
		end
	end

// Pegar os dados e saida de dados
	always @ ( posedge CLK_IN or negedge RST_IN )
	begin
			if( ~RST_IN )	begin
					DATA_OUT <= {DATA_SIZE{1'b0}};
			end
			else	begin
				if( rising_ss )	begin
					DATA_OUT <= data_MOSI;
				end
				else begin
					DATA_OUT <= DATA_OUT;
				end
				if( drop_ss ) begin
					data_MISO <= DATA_IN;
				end
			end
	end

// Dados durante o processo
	always @ ( posedge CLK_IN or negedge RST_IN )
	begin
			if( ~RST_IN )	begin
				MISO <= 1'b0;
				data_MOSI <= {DATA_SIZE{1'b0}};
			end
			else	begin
				if( rising_clk ) begin
					data_MOSI[counter] <= MOSI;
					MISO <= data_MISO[counter];
				end
			end
	end


endmodule