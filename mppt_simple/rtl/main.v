`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	15:02:51 03/13/2017
// Design Name:		Gustavo Costa
// Module Name:		main
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Revision 1.00 - Changed incond from microchp for incond original
// Revision 1.10 - PID controller adjusted to work on current mode
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main(
	input	clk,
	input	reset,
	input	adc_done,
   input signed [21:0] vin,
   input signed [21:0] iin,
   output reg	read,
   output reg [1:0] led_s,
   output reg [7:0] duty_cicle
   );
	
	// LED RELATED
	localparam LED_OFF =	 2'b00;	// Led is Off
	localparam LED_ON = 	 2'b01;	// Led is On
	localparam BLINK_05HZ =  2'b10;	// Blink for each 2 seconds 
	localparam BLINK_2HZ =	 2'b11;	// Blink for each 0.5 seconds
	
	// MPPT E MAIN
	localparam MPPT_INTERVAL 		= 9259; // 100000000/(40*270)
	localparam MPPT_AVERAGE 		= 4; 	// adjust how many samples we want
	localparam TRACK_DELAY 			= 2; 	// debouncing between the two modes
	localparam SECOND_COUNT 		= 120000; //976 //baybe 23053
	localparam SECOND_COUNT_START = 999; //999
	localparam MPPT_STEP 			= 19;	// 0.5v

	// -- INITIALIZING VARIABLES
	// Main
	reg on_off; //1 - on; 0 - off
	reg [15:0] mppt_calc;
	// MPPT
	reg signed [21:0]	delta_i, delta_v;
	reg signed [21:0]	f_iin, f_vin, fl_iin, fl_vin;
	reg signed [21:0]	di_dv, i_v;
	
	reg [1:0] main_state;	/*	0 = initial_state
										1 = waiting done to be active
										2 = doing calculations
										
									*/
	// -- MACROS
	task SET_LED_BLINK();
		input[1:0] n;
		begin
			led_s = n;
		end
	endtask
	
	// TASKS USED IN MAIN
	// Initialize variables used in all tasks
	task init;
	begin
		mppt_calc = MPPT_INTERVAL;
		f_iin = 21'b0;
		f_vin = 21'b0;
		fl_iin = 21'b0;
		fl_vin = 21'b0;
	
		duty_cicle = 50;
		
		main_state = 2'b11;
		read = 0;
	end
	endtask
	
	
	//Change MPP voltage reference
	task inc_duty();
		begin
			duty_cicle = duty_cicle + 1'b1;
			if (duty_cicle > 95)
			begin
				duty_cicle = 95;
			end
		end
	endtask
	
	task dec_duty();
		begin
			duty_cicle = duty_cicle - 1'b1;
			if (duty_cicle < 5)
			begin
				duty_cicle = 5;
			end
		end
	endtask
	
	// MPPT
	task mppt_INCCOND;
	begin
		delta_i = f_iin - fl_iin;
		delta_v = f_vin - fl_vin;

		if(delta_v > 0)
		begin
			if(delta_v > 0) begin 
				di_dv = (delta_i*10'd1000)/ delta_v;
			end
			else begin
				di_dv = 0;
			end
			
			if(f_vin > 0) begin
				i_v = -((f_iin*10'd1000)/ f_vin);
			end
			else begin
				i_v = 0;
			end
			
			if(di_dv != i_v)
			begin
				if(di_dv > i_v) inc_duty();
				else dec_duty();
			end
			
		end else
		begin
			if(delta_i != 0)
			begin
				if(delta_i > 0) inc_duty();
				else dec_duty();
			end
		end

		fl_iin = f_iin;
		fl_vin = f_vin;

		f_iin = 0;
		f_vin = 0;
	end
	endtask


	//reg[31:0] count; //debugin?

	initial begin
		//count = 0;
		init();
		main_state = 2'b11;
		SET_LED_BLINK(LED_OFF);
		duty_cicle = 50;
		on_off = 0;
	end
	
	// Main loop
	always @(posedge clk)
	begin
		if ( ~reset )
		begin
			// initialization
			if(!reset)
			begin
				init();
				if( on_off == 0 ) begin
					on_off = 1;
					main_state = 2'b00;
					SET_LED_BLINK(BLINK_05HZ);
				end
				else begin
					on_off = 0;
					SET_LED_BLINK(LED_OFF);
					main_state = 2'b11;
					duty_cicle = 50;
				end
			end
		end		
		else begin
			case( main_state )
			0 : begin
					SET_LED_BLINK(BLINK_05HZ);
					if (mppt_calc > 0 ) begin
						mppt_calc = mppt_calc - 16'd1;
					end
					read = 1'b1; //read_adc
					main_state = 1;
				end // End of main_state == 0
			1 : if ( adc_done == 1'b1 ) begin
					main_state = 2; // Update state when done goes active high. Data is ready at input.
					read = 1'b0;
			end
			2 : begin
					//Calculating MPPT
					if ( mppt_calc < MPPT_AVERAGE )	// Defines the number of samples
					begin
						f_vin = f_vin + vin;	// Average of input voltages
						f_iin = f_iin + iin;	// Average of input currents
					end
					
					if ( mppt_calc == 0 ) 
					begin
						f_vin = f_vin/(3'd4);
						f_iin = f_iin/(3'd4);
						mppt_calc = MPPT_INTERVAL;
						mppt_INCCOND();			// Executes MPPT
					end
						
					main_state = 0;
				end	// End of main_state == 1
				
				3 : begin
					if( on_off == 0 )
					begin
						SET_LED_BLINK(LED_OFF);
						duty_cicle = 50;
					end
				end
			endcase	// END_CASE
		end		// END_OF_NOT_RESET
	end

endmodule
