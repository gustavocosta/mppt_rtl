`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	15:47:14 03/06/2017
// Design Name:		Gustavo Costa
// Module Name:		nco
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Revision 0.10 - Making two NCO signals to control both transistors
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module nco(
	input wire 			clk,					// Input clock
	input wire [7:0]	duty_cicle,			// How much to add in the acumulator
	output reg pwm_signal					// The pulse
);

	reg [29:0] accum;							// Store accumulator value
	reg [1:0] state; /* 
				* 0 = PWM hi = 1
				* 1 = PWM hi = 0
				*/
				
	initial begin
		accum = 0;
		state = 2'b00;
		pwm_signal = 1'b0;
	end
	
	always @(posedge clk) begin
		accum = accum + 1'b1;
		case( state )
			2'b00: begin
				pwm_signal = 1'b1;
				if ( accum >= (3'd4*duty_cicle) )
				begin
					accum = 0;
					state = 2'b01;
				end
			end
			2'b01: begin
				pwm_signal = 1'b0;
				if (accum >= (3'd4*(100-duty_cicle)))
				begin
					accum = 30'd0;
					state = 2'b11;
				end
			end
			default: begin
				pwm_signal = 1'b0;
				if (accum >= 4)
				begin
					accum = 30'd0;
					state = 2'b00;
				end
			end
		endcase
	end
endmodule
