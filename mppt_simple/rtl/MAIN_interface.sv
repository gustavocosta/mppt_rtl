interface dut_if (input clk, reset);

    // In signals 

    logic [7:0] vin, iin, vout, iout;
    logic done;

    // Out signals

    bit [1:0] led_s;
    logic increment, read;
endinterface
