`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	20:20:09 03/06/2017
// Design Name:		Gustavo Costa
// Module Name:		read_adc
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module read_adc(
	input wire clk,			// Clock is posedge
	input wire [7:0] db,		// Data Bus. Receives data from conversor.
	input wire eoc,			// Active low. End Of Conversion. Indicates that data is avaliable at DataBus. 
	input wire read,			// Active high. Signal to start conversion 
	output reg [2:0] a,		// Selector from conversor. {000, 001, 010, 011} = {vin, iin, vout, iout}
	output reg convst,		// Active in negedge (low). Conversion Start.
	output reg cs,				// Active low. Chip Select. Will be tied logically to ground.
	output reg rd,				// Active low. Used to read "a" variable and activate data read buffers in ADC.
	output reg done,			// Active high. Indicates to main that all 4 reads are ready to be read.
	output reg signed [21:0] vin,	// Tension Input from circuit.
	output reg signed [21:0] iin		// Current Input from circuit.
);

	reg [7:0] data_out1, data_out2;
	reg [3:0] state, next_state;// Describe state info.
	reg [7:0] wait_clk;			// Used to wait a specific number of clocks before going to next state
	reg [7:0] next_wait_clk;	// Define next value for wait_clk

	always @(state) begin
		case( state )
			// ================> Output of form: 		    {   a	  convs,cs,   rd,   wait_clk }
			4'd0:  { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b1, 1'b0, 1'b1, 8'd3	};	// Needs 2  clock cycle. Initial state. 
			4'd1:  { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b1, 1'b0, 1'b0, 8'd1	};	// Needs 5  clock cycles. Read ADC input and store at buffers.
			4'd2:  { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b1, 1'b0, 1'b0, 8'd15	};	// Needs 16 clock cycles. Finish reading from ADC input
			4'd3:  { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b1, 1'b0, 1'b1, 8'd3	};
			4'd4:  { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b0, 1'b0, 1'b1, 8'd42	};	// Needs 3 clock cycles. Initial state for loop
			4'd5:  { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b1, 1'b0, 1'b1, 8'd1	};	// Activate convst. Then, waits for EOC.
			
			4'd6:  { a, convst, cs, rd, next_wait_clk} <= { 3'b001, 1'b1, 1'b0, 1'b1, 8'd3	};	// Needs 1  clock cycle. Increment a. 
			4'd7:  { a, convst, cs, rd, next_wait_clk} <= { 3'b001, 1'b1, 1'b0, 1'b0, 8'd1	};	// Needs 2 clock cycles. Activate read.
			4'd8:  { a, convst, cs, rd, next_wait_clk} <= { 3'b001, 1'b1, 1'b0, 1'b0, 8'd15	};	// Needs 1 clock cycles. Activate read.
			4'd9:  { a, convst, cs, rd, next_wait_clk} <= {	3'b001, 1'b1, 1'b0, 1'b1, 8'd3	};	// Needs 16 clock cycles. Finish reading from ADC input. Go to state 4'd3 for 3 more times.
			4'd10: { a, convst, cs, rd, next_wait_clk} <= { 3'b001, 1'b0, 1'b0, 1'b1, 8'd42	};	// Needs 3 clock cycles. Initial state for loop
			4'd11: { a, convst, cs, rd, next_wait_clk} <= { 3'b001, 1'b1, 1'b0, 1'b1, 8'd1	};	// Activate convst. Then, waits for EOC.
			
			default: { a, convst, cs, rd, next_wait_clk} <= { 3'b000, 1'b1, 1'b0, 1'b1, 8'd42	};
		endcase
	end

	// Initial state
	initial begin
			state = 4'd0;
			done = 0;
			data_out1 = 8'd0;
			data_out2 = 8'd0;
			vin = 21'd0;
			iin = 21'd0;
			wait_clk = 5'd2;
			next_wait_clk = 5'd4;
			cs = 1'b0;
	end

	always @ (state)
	begin
		next_state = state + 4'd1;
		if(next_state > 4'd11) begin
			next_state = 4'd0;
		end
		
		if(state == 4'd2) begin
			data_out2 = db;
			
			iin = data_out2 * 11'd293;
		end
		
		if(state == 4'd8) begin
			data_out1 = db;
			
			vin = data_out1 * 11'd1074;
		end
	end
	
	always @ (clk or state or read) begin
		if(read == 1'b0) begin
			done = 1'b0;
		end
		
		if(state == 4'd2) begin
			if(read == 1'b1) begin
				done = 1'b1;
			end
		end
	end

	// Sequential logic
	always @ (posedge clk) begin
		if(wait_clk == 5'd0) begin
			wait_clk = next_wait_clk;
			state = next_state;
		end
		else begin
			wait_clk = wait_clk - 8'd1;
			
			if((state == 4'd5) || (state == 4'd11)) begin
				if(eoc == 0) begin
					wait_clk = next_wait_clk;
					state = next_state;
				end
			end
		end
	end
endmodule