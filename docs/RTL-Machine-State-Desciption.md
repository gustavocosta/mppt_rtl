## Máquina de estados (rascunho)
<br/>
Decidimos que o CS será aterrado permanentemente, uma vez que estaremos utilizando apenas um chip AD.
<br/>
<br/>
- 0. A = 0 **( leitura 1: corrente)**,  10ns
- 1. RD = 0, 30 ns
- 2. RD = 1, 160 ns
<br/>
<br/>
- 3. **( conversão da leitura 1: corrente )**. CONVST = 0, 20ns
- 4. CONVST = 1, EOC = 0, 400 ns
- 5. A = ~A **( leitura 2: tensão )**
- 6. RD = 0,  20 ns
- 7. **( recebe a leitura 1: corrente no decoder )** . DEC_EN = 1
- 8. DEC_EN = 0; RD = 1;
<br/>
<br/>
- 9. Calc_p = 1. Calcular a potencia;
- 10. Calc_diff = 1; Calcula a diferença.
- 11. Output_en = 1; Joga na saída.
- 12. Espera o fim dos ciclos do PWM.
<br/>
<br/>
Volte ao estado 4, incrementando os índices.
<br/>
<br/>
Por: Gustavo Costa, Bruno Lima e Lucas Peixoto. <br/>
*9 de Fevereiro de 2017*. <br/>
*Revisado em 15 de Fevereiro de 2017, por Bruno Lima*
