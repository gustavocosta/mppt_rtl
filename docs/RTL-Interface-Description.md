Descrição das portas dos módulos RTL
8 de Fevereiro de 2017

### Módulo Top

 In: CLK, EOC, EN_DATA[7:0]

 Out: A[2:0], CONVST, CS, RD, DPWM

Descrição sobre as entradas:
* **CLK**: Clock utilizado para sincronizar todos os módulos.
* **EOC**: *Ativo em low. Leva aprox. 400 ns*. EndOfConversion. Indica que os dados da conversão estão prontos para serem lidos.
* **EN_DATA**: Onde os dados convertidos estarão disponíveis. Possui 8 bits.

Descrição sobre as saídas:
* **A[2:0]**: Seletor do multiplexador do ADC. Selecionará tensão/corrente nesse caso.  Possui 3 bits.
* **CONVST**: *Ativo em low.*  Bit de início da conversão do ADC. *O processo da conversão AD se inicia na borda de descida desse sinal*.
* **CS**: *Ativo em low*. Chip selector do ADC. Usado para habilitar a porta paralela do AD7829. Necessário se o ADC está compartilhando um BUS com outro device.
* **RD**: *Ativo em low*. Bit de leitura do ADC. Leva os buffers de saída  para o estado de alta impedânciam e joga os dados de leitura no bus de dados. *Ambos CS e RD precisam estar em nível lógico baixo para habilitar o bus de dados*.
* **DPWM**: *É a saída do PWM*. É um único bit. Seu tempo em nível alto dependerá do duty-cicle gerado no bloco *MPPT*.

Referências utilizadas: 
* Datasheet do [AD7829-1](http://www.analog.com/media/en/technical-documentation/data-sheets/AD7829-1.pdf)
* [Plano de Verificação](/uploads/eacbf9ea49b0ee91baf03326890276f4/Verification-plan__3_.odt)

### Módulo MPPT

 In: current[7:0], tension[7:0], Calc_P, Calc_diff, Out_en, CLK, RESET

 Out: Duty_c[7:0]

Descrição sobre as entradas:
* **current[7:0]**: Leitura da corrente, convertida em 8 bits.
* **tension[7:0]**: Leitura da tensão, convertida em 8 bits.
* **Calc_P**:  Flag para calcular a potência.
* **Calc_diff**: Flag para calcular a diferencial da potência em relação à tensão.
* **Out_en**: Flag para habilitar a leitura do Duty_c[7:0]  na saída.
* **RESET**: Zera os buffers retorna ao estado inicial.

Descrição sobre as saídas:
* **Duty_c[7:0]**: Servirá de entrada para o PWM. 8 bits. Varia entre 0 e 255.

