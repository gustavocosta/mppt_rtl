Passo-a-passo para o teste do HelloWorld no MimasFPGA.
6 de Fevereiro de 2017.

A instalação do ISE 14.7 foi feita seguindo o tutorial deste [link](https://embeddedmicro.com/tutorials/mojo-software-and-updates/installing-ise), que ensina o passo-a-passo necessário para a instalação no Ubuntu.  Lembrando que é necessário fazer o cadastro no site da [Xilinx](https://www.xilinx.com/registration/create-account.html) para fazer o download da IDE e para adiquirir a licensa grátis.

Comigo aconteceu do setup.bin não executar caso a pasta raiz da instalação possuísse um espaço em branco no nome. Caso isso aconteça, basta renomar a pasta.

Após instalada a ferramenta, foi possível testar o ["HelloWorld"](https://bitbucket.org/rodrigopex/hellomimas/overview) disponibilizado pelo Prof. Peixoto. O HelloWorld já vem com as User Constraints (portas de I/O do FPGA usadas pelo projeto) definidas. Um [modelo](http://productdata.numato.com/assets/downloads/fpga/mimas/Mimas.ucf) do User Constrains e outros arquivos de referência podem ser encontrados na sessão Downloads do site do [Mimas Spartan 6](http://numato.com/mimas-spartan-6-fpga-development-board/). 

Para gravar o HelloWorld na plaquinha do Mimas Spartan pelo Ubuntu, é necessário usar o script [mimasconfig.py](http://productdata.numato.com/assets/downloads/fpga/mimas/MimasConfigPython.zip). Ele possui o python3-serial como dependência, sendo necessário rodar o comando `sudo apt-get install python3-serial` antes de executá-lo. O script utiliza o .bin gerado pelo ISE, enviando-o pela porta USB. Um exemplo do passo-a-passo até a geração do .bin pode ser encontrado [aqui](https://docs.numato.com/kb/learning-fpga-verilog-beginners-guide-part-4-synthesis/).
