opt subtitle "HI-TECH Software Omniscient Code Generator (PRO mode) build 10920"

opt pagewidth 120

	opt pm

	processor	16F1503
clrc	macro
	bcf	3,0
	endm
clrz	macro
	bcf	3,2
	endm
setc	macro
	bsf	3,0
	endm
setz	macro
	bsf	3,2
	endm
skipc	macro
	btfss	3,0
	endm
skipz	macro
	btfss	3,2
	endm
skipnc	macro
	btfsc	3,0
	endm
skipnz	macro
	btfsc	3,2
	endm
indf	equ	0
indf0	equ	0
indf1	equ	1
pc	equ	2
pcl	equ	2
status	equ	3
fsr0l	equ	4
fsr0h	equ	5
fsr1l	equ	6
fsr1h	equ	7
bsr	equ	8
wreg	equ	9
intcon	equ	11
c	equ	1
z	equ	0
pclath	equ	10
# 49 "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 49 "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\main.c"
	dw 0x39A4 ;#
# 50 "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\main.c"
	psect config,class=CONFIG,delta=2 ;#
# 50 "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\main.c"
	dw 0x1FFF ;#
	FNCALL	_main,_Init_Registers
	FNCALL	_main,_Initialize_Hardware
	FNCALL	_main,_Delay_ms
	FNCALL	_main,_read_ADC
	FNCALL	_main,_cc_cv_mode
	FNCALL	_main,_pid
	FNCALL	_main,_Init_State_Machine
	FNCALL	_main,_led_blink
	FNCALL	_main,_Battery_State_Machine
	FNCALL	_pid,___awdiv
	FNCALL	_pid,_set_NCO
	FNCALL	_Initialize_Hardware,_set_NCO
	FNCALL	_Battery_State_Machine,_set_NCO
	FNROOT	_main
	global	_iflat_db
	global	_imin
	global	_iout
	global	_iref
	global	_pi
	global	_pp
	global	_state_counter
	global	_vout
	global	_vref
	global	_imin_db
	global	_led_cnt
	global	_led_state
	global	_temp
	global	_battery_state
	global	_cmode
	global	_ad_res
psect	nvBANK0,class=BANK0,space=1
global __pnvBANK0
__pnvBANK0:
_ad_res:
       ds      2

	global	_increment
_increment:
       ds      2

	global	_second
_second:
       ds      2

	global	_warmup
_warmup:
       ds      2

	global	_but_cnt
_but_cnt:
       ds      1

	global	_cc_cv
_cc_cv:
       ds      1

	global	_lbut
_lbut:
       ds      1

	global	_PORTA
_PORTA	set	12
	global	_PORTC
_PORTC	set	14
	global	_T2CON
_T2CON	set	28
	global	_RA3
_RA3	set	99
	global	_RC5
_RC5	set	117
	global	_T0IF
_T0IF	set	90
	global	_ADCON0
_ADCON0	set	157
	global	_ADCON1
_ADCON1	set	158
	global	_ADRESH
_ADRESH	set	156
	global	_ADRESL
_ADRESL	set	155
	global	_OPTION_REG
_OPTION_REG	set	149
	global	_OSCCON
_OSCCON	set	153
	global	_TRISA
_TRISA	set	140
	global	_TRISC
_TRISC	set	142
	global	_GO_nDONE
_GO_nDONE	set	1257
	global	_TRISA4
_TRISA4	set	1124
	global	_APFCON
_APFCON	set	285
	global	_FVRCON
_FVRCON	set	279
	global	_ANSELA
_ANSELA	set	396
	global	_ANSELC
_ANSELC	set	398
	global	_WPUA
_WPUA	set	524
	global	_NCO1CLK
_NCO1CLK	set	1183
	global	_NCO1CON
_NCO1CON	set	1182
	global	_NCO1INCH
_NCO1INCH	set	1180
	global	_NCO1INCL
_NCO1INCL	set	1179
	file	"zeta-charger.as"
	line	#
psect cinit,class=CODE,delta=2
global start_initialization
start_initialization:

psect	bssCOMMON,class=COMMON,space=1
global __pbssCOMMON
__pbssCOMMON:
_battery_state:
       ds      1

_cmode:
       ds      1

psect	bssBANK0,class=BANK0,space=1
global __pbssBANK0
__pbssBANK0:
_iflat_db:
       ds      2

_imin:
       ds      2

_iout:
       ds      2

_iref:
       ds      2

_pi:
       ds      2

_pp:
       ds      2

_state_counter:
       ds      2

_vout:
       ds      2

_vref:
       ds      2

_imin_db:
       ds      1

_led_cnt:
       ds      1

_led_state:
       ds      1

_temp:
       ds      1

psect clrtext,class=CODE,delta=2
global clear_ram
;	Called with FSR0 containing the base address, and
;	WREG with the size to clear
clear_ram:
	clrwdt			;clear the watchdog before getting into this loop
clrloop:
	clrf	indf0		;clear RAM location pointed to by FSR
	addfsr	0,1
	decfsz wreg		;Have we reached the end of clearing yet?
	goto clrloop	;have we reached the end yet?
	retlw	0		;all done for this memory range, return
; Clear objects allocated to COMMON
psect cinit,class=CODE,delta=2
	global __pbssCOMMON
	clrf	((__pbssCOMMON)+0)&07Fh
	clrf	((__pbssCOMMON)+1)&07Fh
; Clear objects allocated to BANK0
psect cinit,class=CODE,delta=2
	global __pbssBANK0
	movlw	low(__pbssBANK0)
	movwf	fsr0l
	movlw	high(__pbssBANK0)
	movwf	fsr0h
	movlw	016h
	fcall	clear_ram
psect cinit,class=CODE,delta=2
global end_of_initialization

;End of C runtime variable initialization code

end_of_initialization:
movlb 0
ljmp _main	;jump to C main() function
psect	cstackCOMMON,class=COMMON,space=1
global __pcstackCOMMON
__pcstackCOMMON:
	global	?_set_NCO
?_set_NCO:	; 0 bytes @ 0x0
	global	??_set_NCO
??_set_NCO:	; 0 bytes @ 0x0
	global	?_Init_State_Machine
?_Init_State_Machine:	; 0 bytes @ 0x0
	global	??_Init_State_Machine
??_Init_State_Machine:	; 0 bytes @ 0x0
	global	?_Battery_State_Machine
?_Battery_State_Machine:	; 0 bytes @ 0x0
	global	??_Battery_State_Machine
??_Battery_State_Machine:	; 0 bytes @ 0x0
	global	?_Initialize_Hardware
?_Initialize_Hardware:	; 0 bytes @ 0x0
	global	??_Initialize_Hardware
??_Initialize_Hardware:	; 0 bytes @ 0x0
	global	?_Init_Registers
?_Init_Registers:	; 0 bytes @ 0x0
	global	??_Init_Registers
??_Init_Registers:	; 0 bytes @ 0x0
	global	?_Delay_ms
?_Delay_ms:	; 0 bytes @ 0x0
	global	?_read_ADC
?_read_ADC:	; 0 bytes @ 0x0
	global	??_read_ADC
??_read_ADC:	; 0 bytes @ 0x0
	global	?_cc_cv_mode
?_cc_cv_mode:	; 0 bytes @ 0x0
	global	??_cc_cv_mode
??_cc_cv_mode:	; 0 bytes @ 0x0
	global	?_led_blink
?_led_blink:	; 0 bytes @ 0x0
	global	??_led_blink
??_led_blink:	; 0 bytes @ 0x0
	global	?_main
?_main:	; 0 bytes @ 0x0
	global	?___awdiv
?___awdiv:	; 2 bytes @ 0x0
	global	Delay_ms@msec
Delay_ms@msec:	; 2 bytes @ 0x0
	global	___awdiv@divisor
___awdiv@divisor:	; 2 bytes @ 0x0
	ds	2
	global	??_Delay_ms
??_Delay_ms:	; 0 bytes @ 0x2
	global	___awdiv@dividend
___awdiv@dividend:	; 2 bytes @ 0x2
	ds	2
	global	??___awdiv
??___awdiv:	; 0 bytes @ 0x4
	global	___awdiv@counter
___awdiv@counter:	; 1 bytes @ 0x4
	ds	1
	global	___awdiv@sign
___awdiv@sign:	; 1 bytes @ 0x5
	ds	1
	global	___awdiv@quotient
___awdiv@quotient:	; 2 bytes @ 0x6
	ds	2
	global	?_pid
?_pid:	; 0 bytes @ 0x8
	global	pid@feedback
pid@feedback:	; 2 bytes @ 0x8
	ds	2
	global	pid@setpoint
pid@setpoint:	; 2 bytes @ 0xA
	ds	2
	global	??_pid
??_pid:	; 0 bytes @ 0xC
	global	??_main
??_main:	; 0 bytes @ 0xC
psect	cstackBANK0,class=BANK0,space=1
global __pcstackBANK0
__pcstackBANK0:
	global	pid@ipid
pid@ipid:	; 2 bytes @ 0x0
	ds	2
	global	pid@er
pid@er:	; 2 bytes @ 0x2
	ds	2
;;Data sizes: Strings 0, constant 0, data 0, bss 24, persistent 11 stack 0
;;Auto spaces:   Size  Autos    Used
;; COMMON          14     12      14
;; BANK0           80      4      37
;; BANK1           32      0       0

;;
;; Pointer list with targets:

;; ?___awdiv	int  size(1) Largest target is 0
;;


;;
;; Critical Paths under _main in COMMON
;;
;;   _main->_pid
;;   _pid->___awdiv
;;
;; Critical Paths under _main in BANK0
;;
;;   _main->_pid
;;
;; Critical Paths under _main in BANK1
;;
;;   None.

;;
;;Main: autosize = 0, tempsize = 0, incstack = 0, save=0
;;

;;
;;Call Graph Tables:
;;
;; ---------------------------------------------------------------------------------
;; (Depth) Function   	        Calls       Base Space   Used Autos Params    Refs
;; ---------------------------------------------------------------------------------
;; (0) _main                                                 0     0      0     573
;;                     _Init_Registers
;;                _Initialize_Hardware
;;                           _Delay_ms
;;                           _read_ADC
;;                         _cc_cv_mode
;;                                _pid
;;                 _Init_State_Machine
;;                          _led_blink
;;              _Battery_State_Machine
;; ---------------------------------------------------------------------------------
;; (1) _read_ADC                                             2     2      0       0
;;                                              0 COMMON     2     2      0
;; ---------------------------------------------------------------------------------
;; (1) _pid                                                  8     4      4     550
;;                                              8 COMMON     4     0      4
;;                                              0 BANK0      4     4      0
;;                            ___awdiv
;;                            _set_NCO
;; ---------------------------------------------------------------------------------
;; (1) _Initialize_Hardware                                  0     0      0       0
;;                            _set_NCO
;; ---------------------------------------------------------------------------------
;; (1) _Battery_State_Machine                                0     0      0       0
;;                            _set_NCO
;; ---------------------------------------------------------------------------------
;; (2) ___awdiv                                              8     4      4     300
;;                                              0 COMMON     8     4      4
;; ---------------------------------------------------------------------------------
;; (1) _led_blink                                            0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _cc_cv_mode                                           0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _Delay_ms                                             2     0      2      23
;;                                              0 COMMON     2     0      2
;; ---------------------------------------------------------------------------------
;; (1) _Init_Registers                                       0     0      0       0
;; ---------------------------------------------------------------------------------
;; (2) _set_NCO                                              0     0      0       0
;; ---------------------------------------------------------------------------------
;; (1) _Init_State_Machine                                   0     0      0       0
;; ---------------------------------------------------------------------------------
;; Estimated maximum stack depth 2
;; ---------------------------------------------------------------------------------

;; Call Graph Graphs:

;; _main (ROOT)
;;   _Init_Registers
;;   _Initialize_Hardware
;;     _set_NCO
;;   _Delay_ms
;;   _read_ADC
;;   _cc_cv_mode
;;   _pid
;;     ___awdiv
;;     _set_NCO
;;   _Init_State_Machine
;;   _led_blink
;;   _Battery_State_Machine
;;     _set_NCO
;;

;; Address spaces:

;;Name               Size   Autos  Total    Cost      Usage
;;BIGRAM              70      0       0       0        0.0%
;;NULL                 0      0       0       0        0.0%
;;CODE                 0      0       0       0        0.0%
;;BITCOMMON            E      0       0       1        0.0%
;;BITSFR0              0      0       0       1        0.0%
;;SFR0                 0      0       0       1        0.0%
;;COMMON               E      C       E       2      100.0%
;;BITSFR1              0      0       0       2        0.0%
;;SFR1                 0      0       0       2        0.0%
;;BITSFR2              0      0       0       3        0.0%
;;SFR2                 0      0       0       3        0.0%
;;STACK                0      0       2       3        0.0%
;;BITSFR3              0      0       0       4        0.0%
;;SFR3                 0      0       0       4        0.0%
;;ABS                  0      0      33       4        0.0%
;;BITBANK0            50      0       0       5        0.0%
;;BITSFR4              0      0       0       5        0.0%
;;SFR4                 0      0       0       5        0.0%
;;BANK0               50      4      25       6       46.3%
;;BITSFR5              0      0       0       6        0.0%
;;SFR5                 0      0       0       6        0.0%
;;BITBANK1            20      0       0       7        0.0%
;;BITSFR6              0      0       0       7        0.0%
;;SFR6                 0      0       0       7        0.0%
;;BANK1               20      0       0       8        0.0%
;;BITSFR7              0      0       0       8        0.0%
;;SFR7                 0      0       0       8        0.0%
;;BITSFR8              0      0       0       9        0.0%
;;SFR8                 0      0       0       9        0.0%
;;DATA                 0      0      35       9        0.0%
;;BITSFR9              0      0       0      10        0.0%
;;SFR9                 0      0       0      10        0.0%
;;BITSFR10             0      0       0      11        0.0%
;;SFR10                0      0       0      11        0.0%
;;BITSFR11             0      0       0      12        0.0%
;;SFR11                0      0       0      12        0.0%
;;BITSFR12             0      0       0      13        0.0%
;;SFR12                0      0       0      13        0.0%
;;BITSFR13             0      0       0      14        0.0%
;;SFR13                0      0       0      14        0.0%
;;BITSFR14             0      0       0      15        0.0%
;;SFR14                0      0       0      15        0.0%
;;BITSFR15             0      0       0      16        0.0%
;;SFR15                0      0       0      16        0.0%
;;BITSFR16             0      0       0      17        0.0%
;;SFR16                0      0       0      17        0.0%
;;BITSFR17             0      0       0      18        0.0%
;;SFR17                0      0       0      18        0.0%
;;BITSFR18             0      0       0      19        0.0%
;;SFR18                0      0       0      19        0.0%
;;BITSFR19             0      0       0      20        0.0%
;;SFR19                0      0       0      20        0.0%
;;BITSFR20             0      0       0      21        0.0%
;;SFR20                0      0       0      21        0.0%
;;BITSFR21             0      0       0      22        0.0%
;;SFR21                0      0       0      22        0.0%
;;BITSFR22             0      0       0      23        0.0%
;;SFR22                0      0       0      23        0.0%
;;BITSFR23             0      0       0      24        0.0%
;;SFR23                0      0       0      24        0.0%
;;BITSFR24             0      0       0      25        0.0%
;;SFR24                0      0       0      25        0.0%
;;BITSFR25             0      0       0      26        0.0%
;;SFR25                0      0       0      26        0.0%
;;BITSFR26             0      0       0      27        0.0%
;;SFR26                0      0       0      27        0.0%
;;BITSFR27             0      0       0      28        0.0%
;;SFR27                0      0       0      28        0.0%
;;BITSFR28             0      0       0      29        0.0%
;;SFR28                0      0       0      29        0.0%
;;BITSFR29             0      0       0      30        0.0%
;;SFR29                0      0       0      30        0.0%
;;BITSFR30             0      0       0      31        0.0%
;;SFR30                0      0       0      31        0.0%
;;BITSFR31             0      0       0      32        0.0%
;;SFR31                0      0       0      32        0.0%

	global	_main
psect	maintext,global,class=CODE,delta=2
global __pmaintext
__pmaintext:

;; *************** function _main *****************
;; Defined at:
;;		line 53 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\main.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 1E/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels required when called:    2
;; This function calls:
;;		_Init_Registers
;;		_Initialize_Hardware
;;		_Delay_ms
;;		_read_ADC
;;		_cc_cv_mode
;;		_pid
;;		_Init_State_Machine
;;		_led_blink
;;		_Battery_State_Machine
;; This function is called by:
;;		Startup code after reset
;; This function uses a non-reentrant model
;;
psect	maintext
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\main.c"
	line	53
	global	__size_of_main
	__size_of_main	equ	__end_of_main-_main
	
_main:	
	opt	stack 14
; Regs used in _main: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	55
	
l5974:	
;main.c: 55: Init_Registers();
	fcall	_Init_Registers
	line	57
	
l5976:	
;main.c: 57: Initialize_Hardware();
	fcall	_Initialize_Hardware
	line	59
	
l5978:	
;main.c: 59: Delay_ms(500);
	movlw	low(01F4h)
	movwf	(?_Delay_ms)
	movlw	high(01F4h)
	movwf	((?_Delay_ms))+1
	fcall	_Delay_ms
	line	63
	
l5980:	
;main.c: 62: {
;main.c: 63: if(T0IF)
	btfss	(90/8),(90)&7
	goto	u1121
	goto	u1120
u1121:
	goto	l6004
u1120:
	line	66
	
l5982:	
;main.c: 64: {
;main.c: 66: T0IF = 0;
	bcf	(90/8),(90)&7
	line	67
	
l5984:	
;main.c: 67: if(but_cnt) but_cnt--;
	movlb 0	; select bank0
	movf	(_but_cnt),w
	skipz
	goto	u1130
	goto	l5988
u1130:
	
l5986:	
	decf	(_but_cnt),f
	line	68
	
l5988:	
;main.c: 68: if(second) second--;
	movf	(_second+1),w
	iorwf	(_second),w
	skipnz
	goto	u1141
	goto	u1140
u1141:
	goto	l5992
u1140:
	
l5990:	
	movlw	low(01h)
	subwf	(_second),f
	movlw	high(01h)
	subwfb	(_second+1),f
	line	70
	
l5992:	
;main.c: 70: read_ADC();
	fcall	_read_ADC
	line	72
	
l5994:	
;main.c: 72: if(battery_state > DONE)
	movlw	(03h)
	subwf	(_battery_state),w
	skipc
	goto	u1151
	goto	u1150
u1151:
	goto	l6004
u1150:
	line	75
	
l5996:	
;main.c: 74: {
;main.c: 75: cc_cv_mode();
	fcall	_cc_cv_mode
	line	77
	
l5998:	
;main.c: 77: if(!cmode) pid(vout, vref); else
	movf	(_cmode),f
	skipz
	goto	u1161
	goto	u1160
u1161:
	goto	l6002
u1160:
	
l6000:	
	movf	(_vout+1),w
	movwf	(?_pid+1)
	movf	(_vout),w
	movwf	(?_pid)
	movf	(_vref+1),w
	movwf	1+(?_pid)+02h
	movf	(_vref),w
	movwf	0+(?_pid)+02h
	fcall	_pid
	goto	l6004
	line	78
	
l6002:	
;main.c: 78: pid(iout, iref);
	movf	(_iout+1),w
	movwf	(?_pid+1)
	movf	(_iout),w
	movwf	(?_pid)
	movf	(_iref+1),w
	movwf	1+(?_pid)+02h
	movf	(_iref),w
	movwf	0+(?_pid)+02h
	fcall	_pid
	line	81
	
l6004:	
;main.c: 79: }
;main.c: 80: }
;main.c: 81: if(!but_cnt)
	movlb 0	; select bank0
	movf	(_but_cnt),f
	skipz
	goto	u1171
	goto	u1170
u1171:
	goto	l6022
u1170:
	line	83
	
l6006:	
;main.c: 82: {
;main.c: 83: but_cnt = 61;
	movlw	(03Dh)
	movwf	(_but_cnt)
	line	85
	
l6008:	
;main.c: 85: if(!RA3 & lbut)
	clrc
	btfss	(99/8),(99)&7
	setc
	movlw	0
	addwfc	wreg,w
	andwf	(_lbut),w
	btfsc	status,2
	goto	u1181
	goto	u1180
u1181:
	goto	l6018
u1180:
	line	87
	
l6010:	
;main.c: 86: {
;main.c: 87: if(battery_state == IDLE) Init_State_Machine(); else
	movf	(_battery_state),f
	skipz
	goto	u1191
	goto	u1190
u1191:
	goto	l6014
u1190:
	
l6012:	
	fcall	_Init_State_Machine
	goto	l6018
	line	89
	
l6014:	
;main.c: 88: {
;main.c: 89: battery_state = IDLE;
	clrf	(_battery_state)
	line	90
	
l6016:	
;main.c: 90: { led_state = LED_OFF; };
	movlw	(064h)
	movwf	(_led_state)
	line	93
	
l6018:	
;main.c: 91: }
;main.c: 92: }
;main.c: 93: lbut = RA3;
	movlw	0
	movlb 0	; select bank0
	btfsc	(99/8),(99)&7
	movlw	1
	movwf	(_lbut)
	line	95
	
l6020:	
;main.c: 95: led_blink();
	fcall	_led_blink
	line	97
	
l6022:	
;main.c: 96: }
;main.c: 97: if(!second)
	movf	((_second+1)),w
	iorwf	((_second)),w
	skipz
	goto	u1201
	goto	u1200
u1201:
	goto	l5980
u1200:
	line	99
	
l6024:	
;main.c: 98: {
;main.c: 99: second = 976;
	movlw	low(03D0h)
	movwf	(_second)
	movlw	high(03D0h)
	movwf	((_second))+1
	line	101
	
l6026:	
;main.c: 101: Battery_State_Machine();
	fcall	_Battery_State_Machine
	goto	l5980
	global	start
	ljmp	start
	opt stack 0
psect	maintext
	line	105
GLOBAL	__end_of_main
	__end_of_main:
;; =============== function _main ends ============

	signat	_main,88
	global	_read_ADC
psect	text258,local,class=CODE,delta=2
global __ptext258
__ptext258:

;; *************** function _read_ADC *****************
;; Defined at:
;;		line 209 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: 0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          2       0       0
;;      Totals:         2       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text258
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	209
	global	__size_of_read_ADC
	__size_of_read_ADC	equ	__end_of_read_ADC-_read_ADC
	
_read_ADC:	
	opt	stack 15
; Regs used in _read_ADC: [wreg+status,2+status,0+btemp+1]
	line	210
	
l5938:	
;Hardware.c: 210: { ADCON0 = 0b10000001 | (0x01 << 2); };
	movlw	(085h)
	movlb 1	; select bank1
	movwf	(157)^080h	;volatile
	line	211
;Hardware.c: 211: _delay(10);
	opt asmopt_off
movlw	3
movwf	(??_read_ADC+0)+0,f
u1217:
decfsz	(??_read_ADC+0)+0,f
	goto	u1217
opt asmopt_on

	line	212
	
l5940:	
;Hardware.c: 212: ad_res = 0;
	movlb 0	; select bank0
	clrf	(_ad_res)
	clrf	(_ad_res+1)
	line	214
	
l5942:	
;Hardware.c: 214: for(temp = 0; temp < 4; temp++)
	clrf	(_temp)
	line	215
	
l3226:	
	line	216
;Hardware.c: 215: {
;Hardware.c: 216: { GO_nDONE = 1; while(GO_nDONE); };
	movlb 1	; select bank1
	bsf	(1257/8)^080h,(1257)&7
	
l3228:	
	btfsc	(1257/8)^080h,(1257)&7
	goto	u1081
	goto	u1080
u1081:
	goto	l3228
u1080:
	line	217
	
l5946:	
;Hardware.c: 217: ad_res += ADRESL;
	movf	(155)^080h,w	;volatile
	movlb 0	; select bank0
	addwf	(_ad_res),f
	skipnc
	incf	(_ad_res+1),f
	line	218
	
l5948:	
;Hardware.c: 218: ad_res += (ADRESH << 8);
	movlb 1	; select bank1
	movf	(156)^080h,w	;volatile
	movwf	(??_read_ADC+0)+0
	clrf	(??_read_ADC+0)+0+1
	movf	(??_read_ADC+0)+0,w
	movwf	(??_read_ADC+0)+1
	clrf	(??_read_ADC+0)+0
	movf	0+(??_read_ADC+0)+0,w
	movlb 0	; select bank0
	addwf	(_ad_res),f
	movf	1+(??_read_ADC+0)+0,w
	addwfc	(_ad_res+1),f
	line	214
	
l5950:	
	incf	(_temp),f
	
l5952:	
	movlw	(04h)
	subwf	(_temp),w
	skipc
	goto	u1091
	goto	u1090
u1091:
	goto	l3226
u1090:
	line	220
	
l5954:	
;Hardware.c: 219: }
;Hardware.c: 220: iout = ad_res;
	movf	(_ad_res+1),w
	movwf	(_iout+1)
	movf	(_ad_res),w
	movwf	(_iout)
	line	222
;Hardware.c: 222: { ADCON0 = 0b10000001 | (0x07 << 2); };
	movlw	(09Dh)
	movlb 1	; select bank1
	movwf	(157)^080h	;volatile
	line	223
;Hardware.c: 223: _delay(10);
	opt asmopt_off
movlw	3
movwf	(??_read_ADC+0)+0,f
u1227:
decfsz	(??_read_ADC+0)+0,f
	goto	u1227
opt asmopt_on

	line	224
	
l5956:	
;Hardware.c: 224: ad_res = 0;
	movlb 0	; select bank0
	clrf	(_ad_res)
	clrf	(_ad_res+1)
	line	226
	
l5958:	
;Hardware.c: 226: for(temp = 0; temp < 4; temp++)
	clrf	(_temp)
	line	227
	
l3231:	
	line	228
;Hardware.c: 227: {
;Hardware.c: 228: { GO_nDONE = 1; while(GO_nDONE); };
	movlb 1	; select bank1
	bsf	(1257/8)^080h,(1257)&7
	
l3233:	
	btfsc	(1257/8)^080h,(1257)&7
	goto	u1101
	goto	u1100
u1101:
	goto	l3233
u1100:
	line	229
	
l5962:	
;Hardware.c: 229: ad_res += ADRESL;
	movf	(155)^080h,w	;volatile
	movlb 0	; select bank0
	addwf	(_ad_res),f
	skipnc
	incf	(_ad_res+1),f
	line	230
	
l5964:	
;Hardware.c: 230: ad_res += (ADRESH << 8);
	movlb 1	; select bank1
	movf	(156)^080h,w	;volatile
	movwf	(??_read_ADC+0)+0
	clrf	(??_read_ADC+0)+0+1
	movf	(??_read_ADC+0)+0,w
	movwf	(??_read_ADC+0)+1
	clrf	(??_read_ADC+0)+0
	movf	0+(??_read_ADC+0)+0,w
	movlb 0	; select bank0
	addwf	(_ad_res),f
	movf	1+(??_read_ADC+0)+0,w
	addwfc	(_ad_res+1),f
	line	226
	
l5966:	
	incf	(_temp),f
	
l5968:	
	movlw	(04h)
	subwf	(_temp),w
	skipc
	goto	u1111
	goto	u1110
u1111:
	goto	l3231
u1110:
	line	232
	
l5970:	
;Hardware.c: 231: }
;Hardware.c: 232: vout = ad_res;
	movf	(_ad_res+1),w
	movwf	(_vout+1)
	movf	(_ad_res),w
	movwf	(_vout)
	line	234
;Hardware.c: 234: { ADCON0 = 0b10000001 | (0x02 << 2); };
	movlw	(089h)
	movlb 1	; select bank1
	movwf	(157)^080h	;volatile
	line	235
;Hardware.c: 235: _delay(10);
	opt asmopt_off
movlw	3
movwf	(??_read_ADC+0)+0,f
u1237:
decfsz	(??_read_ADC+0)+0,f
	goto	u1237
opt asmopt_on

	line	236
	
l5972:	
;Hardware.c: 236: ad_res = 0;
	movlb 0	; select bank0
	clrf	(_ad_res)
	clrf	(_ad_res+1)
	line	238
	
l3236:	
	return
	opt stack 0
GLOBAL	__end_of_read_ADC
	__end_of_read_ADC:
;; =============== function _read_ADC ends ============

	signat	_read_ADC,88
	global	_pid
psect	text259,local,class=CODE,delta=2
global __ptext259
__ptext259:

;; *************** function _pid *****************
;; Defined at:
;;		line 164 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;  feedback        2    8[COMMON] unsigned int 
;;  setpoint        2   10[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;  er              2    2[BANK0 ] int 
;;  ipid            2    0[BANK0 ] int 
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/9
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         4       0       0
;;      Locals:         0       4       0
;;      Temps:          0       0       0
;;      Totals:         4       4       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		___awdiv
;;		_set_NCO
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text259
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	164
	global	__size_of_pid
	__size_of_pid	equ	__end_of_pid-_pid
	
_pid:	
	opt	stack 14
; Regs used in _pid: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	168
	
l5890:	
;Hardware.c: 165: int er;
;Hardware.c: 166: int ipid;
;Hardware.c: 168: er = setpoint - feedback;
	movf	(pid@setpoint+1),w
	movwf	(pid@er+1)
	movf	(pid@setpoint),w
	movwf	(pid@er)
	movf	(pid@feedback),w
	subwf	(pid@er),f
	movf	(pid@feedback+1),w
	subwfb	(pid@er+1),f
	line	170
	
l5892:	
;Hardware.c: 170: if(er > 4095) er = 4095;
	movf	(pid@er+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(01000h))^80h
	subwf	btemp+1,w
	skipz
	goto	u1005
	movlw	low(01000h)
	subwf	(pid@er),w
u1005:

	skipc
	goto	u1001
	goto	u1000
u1001:
	goto	l5896
u1000:
	
l5894:	
	movlw	low(0FFFh)
	movlb 0	; select bank0
	movwf	(pid@er)
	movlw	high(0FFFh)
	movwf	((pid@er))+1
	line	171
	
l5896:	
;Hardware.c: 171: if(er < -4095) er = -4095;
	movlb 0	; select bank0
	movf	(pid@er+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(-4095))^80h
	subwf	btemp+1,w
	skipz
	goto	u1015
	movlw	low(-4095)
	subwf	(pid@er),w
u1015:

	skipnc
	goto	u1011
	goto	u1010
u1011:
	goto	l5900
u1010:
	
l5898:	
	movlw	low(-4095)
	movlb 0	; select bank0
	movwf	(pid@er)
	movlw	high(-4095)
	movwf	((pid@er))+1
	line	173
	
l5900:	
;Hardware.c: 173: if(!warmup)
	movlb 0	; select bank0
	movf	((_warmup+1)),w
	iorwf	((_warmup)),w
	skipz
	goto	u1021
	goto	u1020
u1021:
	goto	l5926
u1020:
	line	175
	
l5904:	
	movf	(pid@er+1),w
	movwf	(_pp+1)
	movf	(pid@er),w
	movwf	(_pp)
	line	176
	
l3210:	
	line	178
;Hardware.c: 178: pi += er;
	movf	(pid@er),w
	addwf	(_pi),f
	movf	(pid@er+1),w
	addwfc	(_pi+1),f
	line	179
	
l5908:	
;Hardware.c: 179: if(pi > 4095) pi = 4095;
	movf	(_pi+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(01000h))^80h
	subwf	btemp+1,w
	skipz
	goto	u1035
	movlw	low(01000h)
	subwf	(_pi),w
u1035:

	skipc
	goto	u1031
	goto	u1030
u1031:
	goto	l5912
u1030:
	
l5910:	
	movlw	low(0FFFh)
	movlb 0	; select bank0
	movwf	(_pi)
	movlw	high(0FFFh)
	movwf	((_pi))+1
	line	180
	
l5912:	
;Hardware.c: 180: if(pi < -4095) pi = -4095;
	movlb 0	; select bank0
	movf	(_pi+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(-4095))^80h
	subwf	btemp+1,w
	skipz
	goto	u1045
	movlw	low(-4095)
	subwf	(_pi),w
u1045:

	skipnc
	goto	u1041
	goto	u1040
u1041:
	goto	l3212
u1040:
	
l5914:	
	movlw	low(-4095)
	movlb 0	; select bank0
	movwf	(_pi)
	movlw	high(-4095)
	movwf	((_pi))+1
	
l3212:	
	line	182
;Hardware.c: 182: ipid = pp;
	movlb 0	; select bank0
	movf	(_pp+1),w
	movwf	(pid@ipid+1)
	movf	(_pp),w
	movwf	(pid@ipid)
	line	183
	
l5916:	
;Hardware.c: 183: ipid += (pi / 256);
	movlw	low(0100h)
	movwf	(?___awdiv)
	movlw	high(0100h)
	movwf	((?___awdiv))+1
	movf	(_pi+1),w
	movwf	1+(?___awdiv)+02h
	movf	(_pi),w
	movwf	0+(?___awdiv)+02h
	fcall	___awdiv
	movf	(0+(?___awdiv)),w
	addwf	(pid@ipid),f
	movf	(1+(?___awdiv)),w
	addwfc	(pid@ipid+1),f
	line	185
	
l5918:	
;Hardware.c: 185: if(ipid > 4095) ipid = 4095;
	movf	(pid@ipid+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(01000h))^80h
	subwf	btemp+1,w
	skipz
	goto	u1055
	movlw	low(01000h)
	subwf	(pid@ipid),w
u1055:

	skipc
	goto	u1051
	goto	u1050
u1051:
	goto	l5922
u1050:
	
l5920:	
	movlw	low(0FFFh)
	movlb 0	; select bank0
	movwf	(pid@ipid)
	movlw	high(0FFFh)
	movwf	((pid@ipid))+1
	line	186
	
l5922:	
;Hardware.c: 186: if(ipid < -4095) ipid = -4095;
	movlb 0	; select bank0
	movf	(pid@ipid+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(-4095))^80h
	subwf	btemp+1,w
	skipz
	goto	u1065
	movlw	low(-4095)
	subwf	(pid@ipid),w
u1065:

	skipnc
	goto	u1061
	goto	u1060
u1061:
	goto	l3214
u1060:
	
l5924:	
	movlw	low(-4095)
	movlb 0	; select bank0
	movwf	(pid@ipid)
	movlw	high(-4095)
	movwf	((pid@ipid))+1
	
l3214:	
	line	188
;Hardware.c: 188: increment += ipid;
	movlb 0	; select bank0
	movf	(pid@ipid),w
	addwf	(_increment),f
	movf	(pid@ipid+1),w
	addwfc	(_increment+1),f
	line	189
;Hardware.c: 189: } else
	goto	l5936
	line	191
	
l5926:	
;Hardware.c: 190: {
;Hardware.c: 191: warmup--;
	movlw	low(01h)
	subwf	(_warmup),f
	movlw	high(01h)
	subwfb	(_warmup+1),f
	line	192
	
l5928:	
;Hardware.c: 192: if(er > 0) increment++; else increment--;
	movf	(pid@er+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(01h))^80h
	subwf	btemp+1,w
	skipz
	goto	u1075
	movlw	low(01h)
	subwf	(pid@er),w
u1075:

	skipc
	goto	u1071
	goto	u1070
u1071:
	goto	l5932
u1070:
	
l5930:	
	movlb 0	; select bank0
	incf	(_increment),f
	skipnz
	incf	(_increment+1),f
	goto	l5934
	
l5932:	
	movlw	-1
	movlb 0	; select bank0
	addwf	(_increment),f
	skipc
	decf	(_increment+1),f
	line	193
	
l5934:	
;Hardware.c: 193: pi = 0;
	clrf	(_pi)
	clrf	(_pi+1)
	line	196
	
l5936:	
;Hardware.c: 194: }
;Hardware.c: 196: set_NCO();
	fcall	_set_NCO
	line	197
	
l3218:	
	return
	opt stack 0
GLOBAL	__end_of_pid
	__end_of_pid:
;; =============== function _pid ends ============

	signat	_pid,8312
	global	_Initialize_Hardware
psect	text260,local,class=CODE,delta=2
global __ptext260
__ptext260:

;; *************** function _Initialize_Hardware *****************
;; Defined at:
;;		line 72 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 17F/9
;;		On exit  : 1F/1
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_set_NCO
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text260
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	72
	global	__size_of_Initialize_Hardware
	__size_of_Initialize_Hardware	equ	__end_of_Initialize_Hardware-_Initialize_Hardware
	
_Initialize_Hardware:	
	opt	stack 14
; Regs used in _Initialize_Hardware: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	73
	
l5856:	
;Hardware.c: 73: increment = 1;
	movlb 0	; select bank0
	clrf	(_increment)
	incf	(_increment),f
	clrf	(_increment+1)
	line	75
	
l5858:	
# 75 "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
clrwdt ;#
psect	text260
	line	77
	
l5860:	
;Hardware.c: 77: T0IF = 0;
	bcf	(90/8),(90)&7
	line	79
	
l5862:	
;Hardware.c: 79: { ADCON0 = 0b10000001 | (31 << 2); };
	movlw	(0FDh)
	movlb 1	; select bank1
	movwf	(157)^080h	;volatile
	line	81
	
l5864:	
;Hardware.c: 81: ad_res = 0xFFFF;
	movlw	low(0FFFFh)
	movlb 0	; select bank0
	movwf	(_ad_res)
	movlw	high(0FFFFh)
	movwf	((_ad_res))+1
	line	83
	
l5866:	
;Hardware.c: 83: cmode = 0;
	clrf	(_cmode)
	line	84
	
l5868:	
;Hardware.c: 84: warmup = 64;
	movlw	040h
	movwf	(_warmup)
	clrf	(_warmup+1)
	line	86
	
l5870:	
;Hardware.c: 86: cc_cv = 4;
	movlw	(04h)
	movwf	(_cc_cv)
	line	87
	
l5872:	
;Hardware.c: 87: but_cnt = 61;
	movlw	(03Dh)
	movwf	(_but_cnt)
	line	88
	
l5874:	
;Hardware.c: 88: second = 999;
	movlw	low(03E7h)
	movwf	(_second)
	movlw	high(03E7h)
	movwf	((_second))+1
	line	90
	
l5876:	
;Hardware.c: 90: battery_state = IDLE;
	clrf	(_battery_state)
	line	92
	
l5878:	
;Hardware.c: 92: iref = 0;
	clrf	(_iref)
	clrf	(_iref+1)
	line	93
	
l5880:	
;Hardware.c: 93: vref = 0;
	clrf	(_vref)
	clrf	(_vref+1)
	line	95
	
l5882:	
;Hardware.c: 95: lbut = 1;
	clrf	(_lbut)
	incf	(_lbut),f
	line	97
	
l5884:	
;Hardware.c: 97: { increment = 0; set_NCO(); TRISA4 = 1; };
	clrf	(_increment)
	clrf	(_increment+1)
	
l5886:	
	fcall	_set_NCO
	
l5888:	
	movlb 1	; select bank1
	bsf	(1124/8)^080h,(1124)&7
	line	98
	
l3188:	
	return
	opt stack 0
GLOBAL	__end_of_Initialize_Hardware
	__end_of_Initialize_Hardware:
;; =============== function _Initialize_Hardware ends ============

	signat	_Initialize_Hardware,88
	global	_Battery_State_Machine
psect	text261,local,class=CODE,delta=2
global __ptext261
__ptext261:

;; *************** function _Battery_State_Machine *****************
;; Defined at:
;;		line 72 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\BatteryCharger.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1, pclath, cstack
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1E/0
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; Hardware stack levels required when called:    1
;; This function calls:
;;		_set_NCO
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text261
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\BatteryCharger.c"
	line	72
	global	__size_of_Battery_State_Machine
	__size_of_Battery_State_Machine	equ	__end_of_Battery_State_Machine-_Battery_State_Machine
	
_Battery_State_Machine:	
	opt	stack 14
; Regs used in _Battery_State_Machine: [wreg+status,2+status,0+btemp+1+pclath+cstack]
	line	73
	
l5760:	
;BatteryCharger.c: 73: if(battery_state == PRECHARGE)
	movf	(_battery_state),w
	xorlw	03h&0ffh
	skipz
	goto	u801
	goto	u800
u801:
	goto	l5772
u800:
	line	75
	
l5762:	
;BatteryCharger.c: 74: {
;BatteryCharger.c: 75: { led_state = BLINK_05HZ; };
	movlw	(010h)
	movwf	(_led_state)
	line	76
;BatteryCharger.c: 76: if(vout < 1075)
	movlw	high(0433h)
	subwf	(_vout+1),w
	movlw	low(0433h)
	skipnz
	subwf	(_vout),w
	skipnc
	goto	u811
	goto	u810
u811:
	goto	l5770
u810:
	line	78
	
l5764:	
;BatteryCharger.c: 77: {
;BatteryCharger.c: 78: if(state_counter) state_counter--; else
	movf	(_state_counter+1),w
	iorwf	(_state_counter),w
	skipnz
	goto	u821
	goto	u820
u821:
	goto	l1576
u820:
	
l5766:	
	movlw	low(01h)
	subwf	(_state_counter),f
	movlw	high(01h)
	subwfb	(_state_counter+1),f
	goto	l1605
	
l1576:	
	line	80
;BatteryCharger.c: 79: {
;BatteryCharger.c: 80: battery_state = FAULT;
	clrf	(_battery_state)
	incf	(_battery_state),f
	line	81
	
l5768:	
;BatteryCharger.c: 81: { led_state = BLINK_05HZ; };
	movlw	(010h)
	movwf	(_led_state)
	goto	l1605
	line	85
	
l5770:	
;BatteryCharger.c: 84: {
;BatteryCharger.c: 85: battery_state = CHARGE;
	movlw	(04h)
	movwf	(_battery_state)
	line	86
;BatteryCharger.c: 86: { iref = 347; };
	movlw	low(015Bh)
	movwf	(_iref)
	movlw	high(015Bh)
	movwf	((_iref))+1
	goto	l1605
	line	89
	
l5772:	
;BatteryCharger.c: 89: if(battery_state == CHARGE)
	movf	(_battery_state),w
	xorlw	04h&0ffh
	skipz
	goto	u831
	goto	u830
u831:
	goto	l5796
u830:
	line	91
	
l5774:	
;BatteryCharger.c: 90: {
;BatteryCharger.c: 91: { led_state = BLINK_05HZ; };
	movlw	(010h)
	movwf	(_led_state)
	line	92
	
l5776:	
;BatteryCharger.c: 92: if((!cmode))
	movf	(_cmode),f
	skipz
	goto	u841
	goto	u840
u841:
	goto	l5790
u840:
	line	94
	
l5778:	
;BatteryCharger.c: 93: {
;BatteryCharger.c: 94: if(iout < imin)
	movf	(_imin+1),w
	subwf	(_iout+1),w
	skipz
	goto	u855
	movf	(_imin),w
	subwf	(_iout),w
u855:
	skipnc
	goto	u851
	goto	u850
u851:
	goto	l5786
u850:
	line	96
	
l5780:	
;BatteryCharger.c: 95: {
;BatteryCharger.c: 96: if(imin_db) imin_db--; else
	movf	(_imin_db),w
	skipz
	goto	u860
	goto	l5784
u860:
	
l5782:	
	decf	(_imin_db),f
	goto	l1587
	line	98
	
l5784:	
;BatteryCharger.c: 97: {
;BatteryCharger.c: 98: imin = iout;
	movf	(_iout+1),w
	movwf	(_imin+1)
	movf	(_iout),w
	movwf	(_imin)
	line	99
;BatteryCharger.c: 99: imin_db = 5;
	movlw	(05h)
	movwf	(_imin_db)
	line	100
;BatteryCharger.c: 100: iflat_db = 600;
	movlw	low(0258h)
	movwf	(_iflat_db)
	movlw	high(0258h)
	movwf	((_iflat_db))+1
	goto	l1587
	line	104
	
l5786:	
;BatteryCharger.c: 103: {
;BatteryCharger.c: 104: imin_db = 5;
	movlw	(05h)
	movwf	(_imin_db)
	line	105
;BatteryCharger.c: 105: if(iflat_db) iflat_db--;
	movf	(_iflat_db+1),w
	iorwf	(_iflat_db),w
	skipnz
	goto	u871
	goto	u870
u871:
	goto	l1587
u870:
	
l5788:	
	movlw	low(01h)
	subwf	(_iflat_db),f
	movlw	high(01h)
	subwfb	(_iflat_db+1),f
	goto	l1587
	line	109
	
l5790:	
;BatteryCharger.c: 108: {
;BatteryCharger.c: 109: imin_db = 5;
	movlw	(05h)
	movwf	(_imin_db)
	line	110
;BatteryCharger.c: 110: iflat_db = 600;
	movlw	low(0258h)
	movwf	(_iflat_db)
	movlw	high(0258h)
	movwf	((_iflat_db))+1
	line	111
;BatteryCharger.c: 111: imin = 347;
	movlw	low(015Bh)
	movwf	(_imin)
	movlw	high(015Bh)
	movwf	((_imin))+1
	line	112
	
l1587:	
	line	113
;BatteryCharger.c: 112: }
;BatteryCharger.c: 113: if(imin < 35 || !iflat_db)
	movlw	high(023h)
	subwf	(_imin+1),w
	movlw	low(023h)
	skipnz
	subwf	(_imin),w
	skipc
	goto	u881
	goto	u880
u881:
	goto	l5794
u880:
	
l5792:	
	movf	((_iflat_db+1)),w
	iorwf	((_iflat_db)),w
	skipz
	goto	u891
	goto	u890
u891:
	goto	l1605
u890:
	line	116
	
l5794:	
;BatteryCharger.c: 114: {
;BatteryCharger.c: 116: battery_state = FLOAT;
	movlw	(05h)
	movwf	(_battery_state)
	line	117
;BatteryCharger.c: 117: state_counter = 43200;
	movlw	low(0A8C0h)
	movwf	(_state_counter)
	movlw	high(0A8C0h)
	movwf	((_state_counter))+1
	line	119
;BatteryCharger.c: 119: { vref = 1382; };
	movlw	low(0566h)
	movwf	(_vref)
	movlw	high(0566h)
	movwf	((_vref))+1
	goto	l1605
	line	126
	
l5796:	
;BatteryCharger.c: 126: if(battery_state == FLOAT)
	movf	(_battery_state),w
	xorlw	05h&0ffh
	skipz
	goto	u901
	goto	u900
u901:
	goto	l5808
u900:
	line	128
	
l5798:	
;BatteryCharger.c: 127: {
;BatteryCharger.c: 128: { led_state = LED_ON; };
	movlw	(065h)
	movwf	(_led_state)
	line	129
;BatteryCharger.c: 129: if(state_counter) state_counter--; else
	movf	(_state_counter+1),w
	iorwf	(_state_counter),w
	skipnz
	goto	u911
	goto	u910
u911:
	goto	l5802
u910:
	
l5800:	
	movlw	low(01h)
	subwf	(_state_counter),f
	movlw	high(01h)
	subwfb	(_state_counter+1),f
	goto	l1594
	line	131
	
l5802:	
;BatteryCharger.c: 130: {
;BatteryCharger.c: 131: battery_state = DONE;
	movlw	(02h)
	movwf	(_battery_state)
	line	132
	
l1594:	
	line	134
;BatteryCharger.c: 132: }
;BatteryCharger.c: 134: if(state_counter < (43200 - 10) && iout < 16)
	movlw	high(0A8B6h)
	subwf	(_state_counter+1),w
	movlw	low(0A8B6h)
	skipnz
	subwf	(_state_counter),w
	skipnc
	goto	u921
	goto	u920
u921:
	goto	l1605
u920:
	
l5804:	
	movlw	high(010h)
	subwf	(_iout+1),w
	movlw	low(010h)
	skipnz
	subwf	(_iout),w
	skipnc
	goto	u931
	goto	u930
u931:
	goto	l1605
u930:
	line	135
	
l5806:	
;BatteryCharger.c: 135: battery_state = IDLE;
	clrf	(_battery_state)
	goto	l1605
	
l1595:	
	line	137
;BatteryCharger.c: 137: } else
	goto	l1605
	line	138
	
l5808:	
;BatteryCharger.c: 138: if(battery_state == IDLE)
	movf	(_battery_state),f
	skipz
	goto	u941
	goto	u940
u941:
	goto	l5822
u940:
	line	140
	
l5810:	
;BatteryCharger.c: 139: {
;BatteryCharger.c: 140: { led_state = LED_OFF; };
	movlw	(064h)
	movwf	(_led_state)
	line	141
	
l5812:	
;BatteryCharger.c: 141: { vref = 0; };
	clrf	(_vref)
	clrf	(_vref+1)
	line	142
	
l5814:	
;BatteryCharger.c: 142: { iref = 0; };
	clrf	(_iref)
	clrf	(_iref+1)
	line	143
	
l5816:	
;BatteryCharger.c: 143: { increment = 0; set_NCO(); TRISA4 = 1; };
	clrf	(_increment)
	clrf	(_increment+1)
	
l5818:	
	fcall	_set_NCO
	
l5820:	
	movlb 1	; select bank1
	bsf	(1124/8)^080h,(1124)&7
	line	144
;BatteryCharger.c: 144: } else
	goto	l1605
	line	145
	
l5822:	
;BatteryCharger.c: 145: if(battery_state == FAULT)
	decf	(_battery_state),w
	skipz
	goto	u951
	goto	u950
u951:
	goto	l5836
u950:
	line	147
	
l5824:	
;BatteryCharger.c: 146: {
;BatteryCharger.c: 147: { led_state = BLINK_2HZ; };
	movlw	(04h)
	movwf	(_led_state)
	goto	l5812
	line	152
	
l5836:	
;BatteryCharger.c: 152: if(battery_state == DONE)
	movf	(_battery_state),w
	xorlw	02h&0ffh
	skipz
	goto	u961
	goto	u960
u961:
	goto	l1605
u960:
	line	154
	
l5838:	
;BatteryCharger.c: 153: {
;BatteryCharger.c: 154: { led_state = LED_ON; };
	movlw	(065h)
	movwf	(_led_state)
	line	156
;BatteryCharger.c: 156: if(vout < 1290 && vout > 1640)
	movlw	high(050Ah)
	subwf	(_vout+1),w
	movlw	low(050Ah)
	skipnz
	subwf	(_vout),w
	skipnc
	goto	u971
	goto	u970
u971:
	goto	l5846
u970:
	
l5840:	
	movlw	high(0669h)
	subwf	(_vout+1),w
	movlw	low(0669h)
	skipnz
	subwf	(_vout),w
	skipc
	goto	u981
	goto	u980
u981:
	goto	l5846
u980:
	line	158
	
l5842:	
;BatteryCharger.c: 157: {
;BatteryCharger.c: 158: battery_state = CHARGE;
	movlw	(04h)
	movwf	(_battery_state)
	line	160
;BatteryCharger.c: 160: { iref = 347; };
	movlw	low(015Bh)
	movwf	(_iref)
	movlw	high(015Bh)
	movwf	((_iref))+1
	line	161
;BatteryCharger.c: 161: { vref = 1474; };
	movlw	low(05C2h)
	movwf	(_vref)
	movlw	high(05C2h)
	movwf	((_vref))+1
	line	163
;BatteryCharger.c: 163: imin = 347;
	movlw	low(015Bh)
	movwf	(_imin)
	movlw	high(015Bh)
	movwf	((_imin))+1
	line	164
;BatteryCharger.c: 164: imin_db = 5;
	movlw	(05h)
	movwf	(_imin_db)
	line	165
;BatteryCharger.c: 165: iflat_db = 600;
	movlw	low(0258h)
	movwf	(_iflat_db)
	movlw	high(0258h)
	movwf	((_iflat_db))+1
	line	167
;BatteryCharger.c: 167: { warmup = 64; TRISA4 = 0; };
	movlw	040h
	movwf	(_warmup)
	clrf	(_warmup+1)
	
l5844:	
	movlb 1	; select bank1
	bcf	(1124/8)^080h,(1124)&7
	line	168
;BatteryCharger.c: 168: } else
	goto	l1605
	line	170
	
l5846:	
;BatteryCharger.c: 169: {
;BatteryCharger.c: 170: { vref = 0; };
	clrf	(_vref)
	clrf	(_vref+1)
	line	171
;BatteryCharger.c: 171: { iref = 0; };
	clrf	(_iref)
	clrf	(_iref+1)
	line	172
;BatteryCharger.c: 172: { increment = 0; set_NCO(); TRISA4 = 1; };
	clrf	(_increment)
	clrf	(_increment+1)
	
l5848:	
	fcall	_set_NCO
	
l5850:	
	movlb 1	; select bank1
	bsf	(1124/8)^080h,(1124)&7
	line	173
	
l5852:	
;BatteryCharger.c: 173: if(vout < 1640) battery_state = IDLE;
	movlw	high(0668h)
	movlb 0	; select bank0
	subwf	(_vout+1),w
	movlw	low(0668h)
	skipnz
	subwf	(_vout),w
	skipnc
	goto	u991
	goto	u990
u991:
	goto	l1595
u990:
	goto	l5806
	line	179
	
l1605:	
	return
	opt stack 0
GLOBAL	__end_of_Battery_State_Machine
	__end_of_Battery_State_Machine:
;; =============== function _Battery_State_Machine ends ============

	signat	_Battery_State_Machine,88
	global	___awdiv
psect	text262,local,class=CODE,delta=2
global __ptext262
__ptext262:

;; *************** function ___awdiv *****************
;; Defined at:
;;		line 5 in file "C:\Program Files\HI-TECH Software\PICC\9.83\sources\awdiv.c"
;; Parameters:    Size  Location     Type
;;  divisor         2    0[COMMON] int 
;;  dividend        2    2[COMMON] int 
;; Auto vars:     Size  Location     Type
;;  quotient        2    6[COMMON] int 
;;  sign            1    5[COMMON] unsigned char 
;;  counter         1    4[COMMON] unsigned char 
;; Return value:  Size  Location     Type
;;                  2    0[COMMON] int 
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         4       0       0
;;      Locals:         4       0       0
;;      Temps:          0       0       0
;;      Totals:         8       0       0
;;Total ram usage:        8 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_pid
;; This function uses a non-reentrant model
;;
psect	text262
	file	"C:\Program Files\HI-TECH Software\PICC\9.83\sources\awdiv.c"
	line	5
	global	__size_of___awdiv
	__size_of___awdiv	equ	__end_of___awdiv-___awdiv
	
___awdiv:	
	opt	stack 14
; Regs used in ___awdiv: [wreg+status,2+status,0]
	line	9
	
l5716:	
	clrf	(___awdiv@sign)
	line	10
	
l5718:	
	btfss	(___awdiv@divisor+1),7
	goto	u731
	goto	u730
u731:
	goto	l5724
u730:
	line	11
	
l5720:	
	comf	(___awdiv@divisor),f
	comf	(___awdiv@divisor+1),f
	incf	(___awdiv@divisor),f
	skipnz
	incf	(___awdiv@divisor+1),f
	line	12
	
l5722:	
	clrf	(___awdiv@sign)
	incf	(___awdiv@sign),f
	line	14
	
l5724:	
	btfss	(___awdiv@dividend+1),7
	goto	u741
	goto	u740
u741:
	goto	l5730
u740:
	line	15
	
l5726:	
	comf	(___awdiv@dividend),f
	comf	(___awdiv@dividend+1),f
	incf	(___awdiv@dividend),f
	skipnz
	incf	(___awdiv@dividend+1),f
	line	16
	
l5728:	
	movlw	(01h)
	xorwf	(___awdiv@sign),f
	line	18
	
l5730:	
	clrf	(___awdiv@quotient)
	clrf	(___awdiv@quotient+1)
	line	19
	
l5732:	
	movf	(___awdiv@divisor+1),w
	iorwf	(___awdiv@divisor),w
	skipnz
	goto	u751
	goto	u750
u751:
	goto	l5752
u750:
	line	20
	
l5734:	
	clrf	(___awdiv@counter)
	incf	(___awdiv@counter),f
	line	21
	goto	l5738
	line	22
	
l5736:	
	lslf	(___awdiv@divisor),f
	rlf	(___awdiv@divisor+1),f
	line	23
	incf	(___awdiv@counter),f
	line	21
	
l5738:	
	btfss	(___awdiv@divisor+1),(15)&7
	goto	u761
	goto	u760
u761:
	goto	l5736
u760:
	line	26
	
l5740:	
	lslf	(___awdiv@quotient),f
	rlf	(___awdiv@quotient+1),f
	line	27
	
l5742:	
	movf	(___awdiv@divisor+1),w
	subwf	(___awdiv@dividend+1),w
	skipz
	goto	u775
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),w
u775:
	skipc
	goto	u771
	goto	u770
u771:
	goto	l5748
u770:
	line	28
	
l5744:	
	movf	(___awdiv@divisor),w
	subwf	(___awdiv@dividend),f
	movf	(___awdiv@divisor+1),w
	subwfb	(___awdiv@dividend+1),f
	line	29
	
l5746:	
	bsf	(___awdiv@quotient)+(0/8),(0)&7
	line	31
	
l5748:	
	lsrf	(___awdiv@divisor+1),f
	rrf	(___awdiv@divisor),f
	line	32
	
l5750:	
	decfsz	(___awdiv@counter),f
	goto	u781
	goto	u780
u781:
	goto	l5740
u780:
	line	34
	
l5752:	
	movf	(___awdiv@sign),w
	skipz
	goto	u790
	goto	l5756
u790:
	line	35
	
l5754:	
	comf	(___awdiv@quotient),f
	comf	(___awdiv@quotient+1),f
	incf	(___awdiv@quotient),f
	skipnz
	incf	(___awdiv@quotient+1),f
	line	36
	
l5756:	
	movf	(___awdiv@quotient+1),w
	movwf	(?___awdiv+1)
	movf	(___awdiv@quotient),w
	movwf	(?___awdiv)
	line	37
	
l5008:	
	return
	opt stack 0
GLOBAL	__end_of___awdiv
	__end_of___awdiv:
;; =============== function ___awdiv ends ============

	signat	___awdiv,8314
	global	_led_blink
psect	text263,local,class=CODE,delta=2
global __ptext263
__ptext263:

;; *************** function _led_blink *****************
;; Defined at:
;;		line 259 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text263
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	259
	global	__size_of_led_blink
	__size_of_led_blink	equ	__end_of_led_blink-_led_blink
	
_led_blink:	
	opt	stack 15
; Regs used in _led_blink: [wreg+status,2+status,0]
	line	260
	
l5702:	
;Hardware.c: 260: if(led_state == LED_ON) RC5 = 1; else
	movf	(_led_state),w
	xorlw	065h&0ffh
	skipz
	goto	u701
	goto	u700
u701:
	goto	l5706
u700:
	
l5704:	
	bsf	(117/8),(117)&7
	goto	l3254
	line	261
	
l5706:	
;Hardware.c: 261: if(led_state == LED_OFF) RC5 = 0; else
	movf	(_led_state),w
	xorlw	064h&0ffh
	skipz
	goto	u711
	goto	u710
u711:
	goto	l5710
u710:
	
l5708:	
	bcf	(117/8),(117)&7
	goto	l3254
	line	262
	
l5710:	
;Hardware.c: 262: if(led_cnt) led_cnt--; else
	movf	(_led_cnt),w
	skipz
	goto	u720
	goto	l5714
u720:
	
l5712:	
	decf	(_led_cnt),f
	goto	l3254
	line	264
	
l5714:	
;Hardware.c: 263: {
;Hardware.c: 264: led_cnt = led_state;
	movf	(_led_state),w
	movwf	(_led_cnt)
	line	265
;Hardware.c: 265: RC5 ^= 1;
	movlw	1<<((117)&7)
	xorwf	((117)/8),f
	line	267
	
l3254:	
	return
	opt stack 0
GLOBAL	__end_of_led_blink
	__end_of_led_blink:
;; =============== function _led_blink ends ============

	signat	_led_blink,88
	global	_cc_cv_mode
psect	text264,local,class=CODE,delta=2
global __ptext264
__ptext264:

;; *************** function _cc_cv_mode *****************
;; Defined at:
;;		line 241 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/0
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text264
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	241
	global	__size_of_cc_cv_mode
	__size_of_cc_cv_mode	equ	__end_of_cc_cv_mode-_cc_cv_mode
	
_cc_cv_mode:	
	opt	stack 15
; Regs used in _cc_cv_mode: [wreg+status,2+status,0]
	line	242
	
l5682:	
;Hardware.c: 242: if(vout > vref)
	movf	(_vout+1),w
	subwf	(_vref+1),w
	skipz
	goto	u655
	movf	(_vout),w
	subwf	(_vref),w
u655:
	skipnc
	goto	u651
	goto	u650
u651:
	goto	l5692
u650:
	line	244
	
l5684:	
;Hardware.c: 243: {
;Hardware.c: 244: if(cc_cv) cc_cv--; else
	movf	(_cc_cv),w
	skipz
	goto	u660
	goto	l5688
u660:
	
l5686:	
	decf	(_cc_cv),f
	goto	l5692
	line	246
	
l5688:	
;Hardware.c: 245: {
;Hardware.c: 246: if(cmode) pi = 0;
	movf	(_cmode),w
	skipz
	goto	u670
	goto	l3242
u670:
	
l5690:	
	clrf	(_pi)
	clrf	(_pi+1)
	
l3242:	
	line	247
;Hardware.c: 247: cmode = 0;
	clrf	(_cmode)
	line	250
	
l5692:	
;Hardware.c: 248: }
;Hardware.c: 249: }
;Hardware.c: 250: if(iout > iref)
	movf	(_iout+1),w
	subwf	(_iref+1),w
	skipz
	goto	u685
	movf	(_iout),w
	subwf	(_iref),w
u685:
	skipnc
	goto	u681
	goto	u680
u681:
	goto	l3245
u680:
	line	252
	
l5694:	
;Hardware.c: 251: {
;Hardware.c: 252: if(!cmode) pi = 0;
	movf	(_cmode),f
	skipz
	goto	u691
	goto	u690
u691:
	goto	l5698
u690:
	
l5696:	
	clrf	(_pi)
	clrf	(_pi+1)
	line	253
	
l5698:	
;Hardware.c: 253: cmode = 1;
	clrf	(_cmode)
	incf	(_cmode),f
	line	254
	
l5700:	
;Hardware.c: 254: cc_cv = 4;
	movlw	(04h)
	movwf	(_cc_cv)
	line	256
	
l3245:	
	return
	opt stack 0
GLOBAL	__end_of_cc_cv_mode
	__end_of_cc_cv_mode:
;; =============== function _cc_cv_mode ends ============

	signat	_cc_cv_mode,88
	global	_Delay_ms
psect	text265,local,class=CODE,delta=2
global __ptext265
__ptext265:

;; *************** function _Delay_ms *****************
;; Defined at:
;;		line 154 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;  msec            2    0[COMMON] unsigned int 
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0
;; Tracked objects:
;;		On entry : 1F/1
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         2       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         2       0       0
;;Total ram usage:        2 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text265
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	154
	global	__size_of_Delay_ms
	__size_of_Delay_ms	equ	__end_of_Delay_ms-_Delay_ms
	
_Delay_ms:	
	opt	stack 15
; Regs used in _Delay_ms: [wreg+status,2+status,0]
	line	155
	
l5676:	
;Hardware.c: 155: while(msec)
	goto	l5680
	line	157
	
l3199:	
	btfss	(90/8),(90)&7
	goto	u631
	goto	u630
u631:
	goto	l3199
u630:
	
l3201:	
	line	158
;Hardware.c: 158: T0IF = 0;
	bcf	(90/8),(90)&7
	line	159
	
l5678:	
;Hardware.c: 159: msec--;
	movlw	low(01h)
	subwf	(Delay_ms@msec),f
	movlw	high(01h)
	subwfb	(Delay_ms@msec+1),f
	line	155
	
l5680:	
	movf	((Delay_ms@msec+1)),w
	iorwf	((Delay_ms@msec)),w
	skipz
	goto	u641
	goto	u640
u641:
	goto	l3199
u640:
	line	161
	
l3203:	
	return
	opt stack 0
GLOBAL	__end_of_Delay_ms
	__end_of_Delay_ms:
;; =============== function _Delay_ms ends ============

	signat	_Delay_ms,4216
	global	_Init_Registers
psect	text266,local,class=CODE,delta=2
global __ptext266
__ptext266:

;; *************** function _Init_Registers *****************
;; Defined at:
;;		line 101 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2
;; Tracked objects:
;;		On entry : 17F/0
;;		On exit  : 17F/9
;;		Unchanged: FFE80/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text266
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	101
	global	__size_of_Init_Registers
	__size_of_Init_Registers	equ	__end_of_Init_Registers-_Init_Registers
	
_Init_Registers:	
	opt	stack 15
; Regs used in _Init_Registers: [wreg+status,2]
	line	103
	
l5646:	
;Hardware.c: 103: OSCCON = 0b01111000;
	movlw	(078h)
	movlb 1	; select bank1
	movwf	(153)^080h	;volatile
	line	105
;Hardware.c: 105: OPTION_REG = 0b01000011;
	movlw	(043h)
	movwf	(149)^080h	;volatile
	line	108
	
l5648:	
;Hardware.c: 108: PORTA = 0b00000000;
	movlb 0	; select bank0
	clrf	(12)	;volatile
	line	109
	
l5650:	
;Hardware.c: 109: TRISA = 0b11001110;
	movlw	(0CEh)
	movlb 1	; select bank1
	movwf	(140)^080h	;volatile
	line	110
	
l5652:	
;Hardware.c: 110: ANSELA = 0b11000110;
	movlw	(0C6h)
	movlb 3	; select bank3
	movwf	(396)^0180h	;volatile
	line	112
	
l5654:	
;Hardware.c: 112: PORTC = 0b00010000;
	movlw	(010h)
	movlb 0	; select bank0
	movwf	(14)	;volatile
	line	113
	
l5656:	
;Hardware.c: 113: TRISC = 0b11001100;
	movlw	(0CCh)
	movlb 1	; select bank1
	movwf	(142)^080h	;volatile
	line	114
	
l5658:	
;Hardware.c: 114: ANSELC = 0b11001100;
	movlw	(0CCh)
	movlb 3	; select bank3
	movwf	(398)^0180h	;volatile
	line	116
	
l5660:	
;Hardware.c: 116: WPUA = 0b00001000;
	movlw	(08h)
	movlb 4	; select bank4
	movwf	(524)^0200h	;volatile
	line	117
	
l5662:	
;Hardware.c: 117: APFCON = 0b00000001;
	movlw	(01h)
	movlb 2	; select bank2
	movwf	(285)^0100h	;volatile
	line	120
	
l5664:	
;Hardware.c: 120: FVRCON = 0b10001111;
	movlw	(08Fh)
	movwf	(279)^0100h	;volatile
	line	123
	
l5666:	
;Hardware.c: 123: ADCON0 = 0b10000001;
	movlw	(081h)
	movlb 1	; select bank1
	movwf	(157)^080h	;volatile
	line	124
	
l5668:	
;Hardware.c: 124: ADCON1 = 0b11010000;
	movlw	(0D0h)
	movwf	(158)^080h	;volatile
	line	126
	
l5670:	
;Hardware.c: 126: T2CON = 0x04;
	movlw	(04h)
	movlb 0	; select bank0
	movwf	(28)	;volatile
	line	129
	
l5672:	
;Hardware.c: 129: NCO1CON = 0b11000001;
	movlw	(0C1h)
	movlb 9	; select bank9
	movwf	(1182)^0480h	;volatile
	line	130
	
l5674:	
;Hardware.c: 130: NCO1CLK = 0xA0;
	movlw	(0A0h)
	movwf	(1183)^0480h	;volatile
	line	132
;Hardware.c: 132: NCO1INCH = 0x00;
	clrf	(1180)^0480h	;volatile
	line	133
;Hardware.c: 133: NCO1INCL = 0x00;
	clrf	(1179)^0480h	;volatile
	line	134
	
l3191:	
	return
	opt stack 0
GLOBAL	__end_of_Init_Registers
	__end_of_Init_Registers:
;; =============== function _Init_Registers ends ============

	signat	_Init_Registers,88
	global	_set_NCO
psect	text267,local,class=CODE,delta=2
global __ptext267
__ptext267:

;; *************** function _set_NCO *****************
;; Defined at:
;;		line 200 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg, status,2, status,0, btemp+1
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/9
;;		Unchanged: FFE00/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_Battery_State_Machine
;;		_Initialize_Hardware
;;		_pid
;; This function uses a non-reentrant model
;;
psect	text267
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\Hardware.c"
	line	200
	global	__size_of_set_NCO
	__size_of_set_NCO	equ	__end_of_set_NCO-_set_NCO
	
_set_NCO:	
	opt	stack 14
; Regs used in _set_NCO: [wreg+status,2+status,0+btemp+1]
	line	201
	
l5634:	
;Hardware.c: 201: if(increment < 1) increment = 1;
	movf	(_increment+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(01h))^80h
	subwf	btemp+1,w
	skipz
	goto	u615
	movlw	low(01h)
	subwf	(_increment),w
u615:

	skipnc
	goto	u611
	goto	u610
u611:
	goto	l5638
u610:
	
l5636:	
	movlb 0	; select bank0
	clrf	(_increment)
	incf	(_increment),f
	clrf	(_increment+1)
	line	202
	
l5638:	
;Hardware.c: 202: if(increment > 29500) increment = 29500;
	movlb 0	; select bank0
	movf	(_increment+1),w
	xorlw	80h
	movwf	btemp+1
	movlw	(high(0733Dh))^80h
	subwf	btemp+1,w
	skipz
	goto	u625
	movlw	low(0733Dh)
	subwf	(_increment),w
u625:

	skipc
	goto	u621
	goto	u620
u621:
	goto	l5642
u620:
	
l5640:	
	movlw	low(0733Ch)
	movlb 0	; select bank0
	movwf	(_increment)
	movlw	high(0733Ch)
	movwf	((_increment))+1
	line	204
	
l5642:	
;Hardware.c: 204: NCO1INCH = increment >> 8;
	movlb 0	; select bank0
	movf	(_increment+1),w
	movlb 9	; select bank9
	movwf	(1180)^0480h	;volatile
	line	205
	
l5644:	
;Hardware.c: 205: NCO1INCL = increment & 0xFF;
	movlb 0	; select bank0
	movf	(_increment),w
	movlb 9	; select bank9
	movwf	(1179)^0480h	;volatile
	line	206
	
l3223:	
	return
	opt stack 0
GLOBAL	__end_of_set_NCO
	__end_of_set_NCO:
;; =============== function _set_NCO ends ============

	signat	_set_NCO,88
	global	_Init_State_Machine
psect	text268,local,class=CODE,delta=2
global __ptext268
__ptext268:

;; *************** function _Init_State_Machine *****************
;; Defined at:
;;		line 56 in file "C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\BatteryCharger.c"
;; Parameters:    Size  Location     Type
;;		None
;; Auto vars:     Size  Location     Type
;;		None
;; Return value:  Size  Location     Type
;;		None               void
;; Registers used:
;;		wreg
;; Tracked objects:
;;		On entry : 1F/0
;;		On exit  : 1F/1
;;		Unchanged: FFFE0/0
;; Data sizes:     COMMON   BANK0   BANK1
;;      Params:         0       0       0
;;      Locals:         0       0       0
;;      Temps:          0       0       0
;;      Totals:         0       0       0
;;Total ram usage:        0 bytes
;; Hardware stack levels used:    1
;; This function calls:
;;		Nothing
;; This function is called by:
;;		_main
;; This function uses a non-reentrant model
;;
psect	text268
	file	"C:\_mihnea\_work\_SVN\HP CC_CV Charger Zeta\Firmware\zeta-battery-charger\BatteryCharger.c"
	line	56
	global	__size_of_Init_State_Machine
	__size_of_Init_State_Machine	equ	__end_of_Init_State_Machine-_Init_State_Machine
	
_Init_State_Machine:	
	opt	stack 15
; Regs used in _Init_State_Machine: [wreg]
	line	57
	
l5630:	
;BatteryCharger.c: 57: battery_state = PRECHARGE;
	movlw	(03h)
	movwf	(_battery_state)
	line	58
;BatteryCharger.c: 58: state_counter = 600;
	movlw	low(0258h)
	movwf	(_state_counter)
	movlw	high(0258h)
	movwf	((_state_counter))+1
	line	59
;BatteryCharger.c: 59: { led_state = BLINK_05HZ; };
	movlw	(010h)
	movwf	(_led_state)
	line	61
;BatteryCharger.c: 61: { iref = 173; };
	movlw	0ADh
	movwf	(_iref)
	clrf	(_iref+1)
	line	62
;BatteryCharger.c: 62: { vref = 1474; };
	movlw	low(05C2h)
	movwf	(_vref)
	movlw	high(05C2h)
	movwf	((_vref))+1
	line	64
;BatteryCharger.c: 64: imin = 347;
	movlw	low(015Bh)
	movwf	(_imin)
	movlw	high(015Bh)
	movwf	((_imin))+1
	line	65
;BatteryCharger.c: 65: imin_db = 5;
	movlw	(05h)
	movwf	(_imin_db)
	line	66
;BatteryCharger.c: 66: iflat_db = 600;
	movlw	low(0258h)
	movwf	(_iflat_db)
	movlw	high(0258h)
	movwf	((_iflat_db))+1
	line	68
;BatteryCharger.c: 68: { warmup = 64; TRISA4 = 0; };
	movlw	040h
	movwf	(_warmup)
	clrf	(_warmup+1)
	
l5632:	
	movlb 1	; select bank1
	bcf	(1124/8)^080h,(1124)&7
	line	69
	
l1571:	
	return
	opt stack 0
GLOBAL	__end_of_Init_State_Machine
	__end_of_Init_State_Machine:
;; =============== function _Init_State_Machine ends ============

	signat	_Init_State_Machine,88
psect	text269,local,class=CODE,delta=2
global __ptext269
__ptext269:
	global	btemp
	btemp set 07Eh

	DABS	1,126,2	;btemp
	global	wtemp0
	wtemp0 set btemp
	end
