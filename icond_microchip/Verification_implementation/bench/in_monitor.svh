class in_monitor extends uvm_monitor;
    `uvm_component_utils(in_monitor)
     
    // in_monitor variables
      
    uvm_analysis_port #(in_transaction) in_aport;
    uvm_analysis_imp #(in_transaction, in_monitor) driver_get;

    // Initialization functions
    
    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase (uvm_phase phase);
        super.build_phase(phase);
        in_aport = new("in_aport", this);
        driver_get = new("driver_get", this);
    endfunction :  build_phase

    function void write(in_transaction t);
        in_aport.write(t);
    endfunction : write
    
endclass : in_monitor
