class in_agent extends uvm_agent;
	`uvm_component_utils(in_agent)

	typedef uvm_sequencer #(in_transaction) sequencer;

    // in_agent components

	uvm_analysis_export #(in_transaction) in_aport;
	sequencer sequencer_h;
	in_driver in_driver_h;
	in_monitor in_monitor_h;

    // Initialization and connection functions

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		in_aport = new("in_aport", this);
		sequencer_h = sequencer::type_id::create("sequencer_h", this);
		in_driver_h = in_driver::type_id::create("in_driver_h", this);
		in_monitor_h = in_monitor::type_id::create("in_monitor_h", this);
	endfunction : build_phase

	function void connect_phase(uvm_phase phase);
		in_driver_h.seq_item_port.connect(sequencer_h.seq_item_export);
		in_monitor_h.in_aport.connect(in_aport);
		in_driver_h.driver_put.connect(in_monitor_h.driver_get);
	endfunction : connect_phase

endclass : in_agent
