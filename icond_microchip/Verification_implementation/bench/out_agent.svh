class out_agent extends uvm_agent;
    `uvm_component_utils(out_agent)

    // out_agent variables

    uvm_analysis_export #(out_transaction) out_aport;
    out_monitor out_monitor_h;

    // Initialization and connection functions
    
    function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		out_aport = new("out_aport", this);
		out_monitor_h = out_monitor::type_id::create("out_monitor_h", this);
	endfunction : build_phase

    function void connect_phase(uvm_phase phase);
        out_monitor_h.out_aport.connect(out_aport);

    endfunction : connect_phase 


endclass
