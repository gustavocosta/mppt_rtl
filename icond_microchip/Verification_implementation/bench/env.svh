class env extends uvm_env;
    `uvm_component_utils(env)

    // Ports

    uvm_tlm_analysis_fifo#(in_transaction) fifo;

    // Variables

    in_agent in_agent_h;
    out_agent out_agent_h;
    scoreboard scoreboard_h;
    subscriber subscriber_h;
 

    // Initialization functions
    
    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        in_agent_h = in_agent::type_id::create("in_agent_h", this);
        out_agent_h = out_agent::type_id::create("out_agent_h", this);
        scoreboard_h = scoreboard::type_id::create("scoreboard_h",this);
        subscriber_h = subscriber::type_id::create("subscriber_h",this);
        fifo = new("fifo", this);
    endfunction

    function void connect_phase(uvm_phase phase);
      	in_agent_h.in_aport.connect(fifo.analysis_export);
      	in_agent_h.in_aport.connect(subscriber_h.analysis_export);
        out_agent_h.out_aport.connect(scoreboard_h.comparator_h.dut_export);
        scoreboard_h.reffmod_h.tx_collected.connect(fifo.get_export);
    endfunction
endclass
