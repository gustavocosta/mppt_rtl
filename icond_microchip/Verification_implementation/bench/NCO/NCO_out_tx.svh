class out_transaction extends uvm_sequence_item;
	`uvm_object_utils(out_transaction)
	
	// Variables

	bit NCO;

	// Basic functions

	function new(string name = "out_transaction");
		super.new(name);
	endfunction : new

    function bit compare(out_transaction to_be_compared);
        if (NCO == to_be_compared.NCO)
            return 1;
        else
            return 0;
    endfunction

	function string convert2string;
		return $sformatf("NCO = %b", NCO);
	endfunction : convert2string


endclass : out_transaction
