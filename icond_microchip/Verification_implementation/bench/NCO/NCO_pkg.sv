package pkg;

    import uvm_pkg::*;
    
    `include "uvm_macros.svh"
    `include "NCO_in_tx.svh"
    `include "NCO_out_tx.svh"
    `include "NCO_reffmod.svh"
    `include "../comparator.svh"
    `include "../scoreboard.svh"
    `include "../in_monitor.svh"
    `include "NCO_out_monitor.svh"
    `include "NCO_in_driver.svh"
    `include "NCO_seqlib.svh"
    `include "../in_agent.svh"
    `include "NCO_subscriber.svh"
    `include "../out_agent.svh"
    `include "../env.svh"
    `include "../test.svh"
    
    
endpackage
