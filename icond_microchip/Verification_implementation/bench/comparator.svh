class comparator extends uvm_component;
  `uvm_component_utils(comparator)
    
    //Ports

    uvm_analysis_export #(out_transaction) reffmod_export;
    uvm_analysis_export #(out_transaction) dut_export;
    uvm_tlm_analysis_fifo #(out_transaction) expected_fifo;
    uvm_tlm_analysis_fifo #(out_transaction) current_fifo;

    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        reffmod_export = new("reffmod_export", this);
        dut_export = new("dut_export", this);
        expected_fifo = new("expected_fifo", this);
        current_fifo = new("current_fifo", this);
    endfunction

    virtual function void connect_phase(uvm_phase phase);
      reffmod_export.connect(expected_fifo.analysis_export);
        dut_export.connect(current_fifo.analysis_export);

    endfunction

  task run_phase(uvm_phase phase);
        out_transaction reffmod_tx;
        out_transaction dut_tx;

        forever begin
            expected_fifo.get_peek_export.get(reffmod_tx);
            current_fifo.get_peek_export.get(dut_tx);
          if(!reffmod_tx.compare(dut_tx))
            `uvm_info("NO_MATCH", $sformatf("Reffmod %s and dut %s",reffmod_tx.convert2string, dut_tx.convert2string), UVM_HIGH)
        end
    endtask


endclass
