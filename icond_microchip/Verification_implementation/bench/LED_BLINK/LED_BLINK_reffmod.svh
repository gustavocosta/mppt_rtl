class reffmod extends uvm_component;
    `uvm_component_utils(reffmod)

    // Parameters 

    localparam LED_ON = 2'b01;
    localparam LED_OFF = 2'b00;
    localparam BLINK_05HZ = 2'b10;
    localparam BLINK_2HZ = 2'b11;
    
    // Ports 

    uvm_analysis_port #(out_transaction) tx_exported;
    uvm_blocking_get_port #(in_transaction) tx_collected;

    // Variables
    
    
    out_transaction out_tx;
    in_transaction in_tx;
    int led_count;

    function new(string name, uvm_component parent);
        super.new(name, parent);
        led_count = 32'd0;
    endfunction : new

    virtual function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        tx_exported = new("tx_exported", this);
        tx_collected = new("tx_collected", this);
        out_tx = out_transaction::type_id::create("out_tx");
    endfunction : build_phase

    virtual task run_phase(uvm_phase phase);
        forever begin
            tx_collected.get(in_tx);

           if(in_tx.reset == 0)
             led_count = 0;
            
            case (in_tx.led_s)
                LED_ON : out_tx.led = 1;
                LED_OFF : out_tx.led = 0;
                BLINK_05HZ : begin
                  if(led_count)
                        led_count--;
                    else begin
                        led_count = 32'd200000000;
                        out_tx.led = ~out_tx.led;
                    end
                end
                BLINK_2HZ : begin
                  if(led_count)
                        led_count--;
                  else begin
                        led_count = 32'd50000000;
                        out_tx.led = ~out_tx.led;
                  end
                end
             endcase
            tx_exported.write(out_tx);
        end
    endtask : run_phase


endclass : reffmod



