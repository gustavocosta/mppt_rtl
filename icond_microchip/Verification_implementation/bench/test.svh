class test extends uvm_test;
	`uvm_component_utils(test)

	// Variables

	env env_h;

	// Basic functions

	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction : new

	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
		env_h = env::type_id::create("env_h", this);
	endfunction : build_phase

    function void end_of_elaboration_phase (uvm_phase phase);
        uvm_top.print_topology();
    endfunction

	task run_phase(uvm_phase phase);
		seqlib seqlib_h;
        seqlib_h = seqlib::type_id::create("seqlib_h");
        fork
            seqlib_h.start(env_h.in_agent_h.sequencer_h);
        join_none
	endtask : run_phase

endclass : test
