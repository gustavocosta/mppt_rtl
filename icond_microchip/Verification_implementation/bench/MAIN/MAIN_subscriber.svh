class subscriber extends uvm_subscriber #(in_transaction);
  `uvm_component_utils(subscriber)
  
  protected uvm_phase running_phase;
  int count;

  function new(string name, uvm_component parent);
    super.new(name, parent);
    count = 0;
  endfunction: new
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
  endfunction: build_phase
  
  function void write(in_transaction t);
    count++;
    if (count == 1000)begin
      running_phase.drop_objection(this);
    end
  endfunction: write
  
  task run_phase(uvm_phase phase);
    running_phase = phase;
    running_phase.raise_objection(this);
  endtask: run_phase
  
  
endclass: subscriber
