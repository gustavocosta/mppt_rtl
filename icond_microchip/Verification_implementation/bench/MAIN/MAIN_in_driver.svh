class in_driver extends uvm_driver #(in_transaction);
    `uvm_component_utils(in_driver)
    
    // in_driver variables
    
    in_transaction tx;
    virtual dut_if vif;
    uvm_analysis_port #(in_transaction) driver_put;
   
    // Initialization functions

    function new (string name, uvm_component parent);
        super.new(name, parent);
    endfunction : new

    function void build_phase(uvm_phase phase);
        super.build_phase(phase);
        if (!uvm_config_db#(virtual dut_if)::get(this,"","vif", vif))
            `uvm_fatal("NOVIF", $sformatf("The dut_if virtual interface was not pre-configured"));
        driver_put = new("driver_put", this);    
    endfunction : build_phase

    task run_phase(uvm_phase phase);
        forever begin
            seq_item_port.get_next_item(tx);
            drive_item(tx);
            seq_item_port.item_done();
        end

    endtask : run_phase

    task drive_item(in_transaction tx);
        @(negedge vif.clk);
        vif.vin = tx.vin;
        vif.iin = tx.iin;
        vif.vout = tx.vout;
        vif.iout = tx.iout;
        vif.done = tx.done;
        vif.reset = tx.reset;
        driver_put.write(tx);
    endtask : drive_item

endclass : in_driver
