class out_transaction extends uvm_sequence_item;
	`uvm_object_utils(out_transaction)
    
    bit [7:0] vin, iin, vout, iout;
	bit[2:0] A;
	bit convst, rd, done;

	function new(string name = "out_transaction");
		super.new(name);
	endfunction : new

    function bit compare(out_transaction to_be_compared);
        if (vin == to_be_compared.vin &&
            iin == to_be_compared.iin &&
            vout == to_be_compared.vout &&
            iout == to_be_compared.iout &&
            A == to_be_compared.A &&
            convst == to_be_compared.convst &&
            rd == to_be_compared.rd &&
            done == to_be_compared.done)
            return 1;
        else
            return 0;
    endfunction

	function string convert2string;
		return $sformatf("vin = %b, iin = %0d, vout  = %0d, iout = %0d, A = %b, convst = %b, rd = %b, done = %b",
        vin, iin, vout, iout, A, convst, rd, done);
	endfunction : convert2string


endclass : out_transaction
