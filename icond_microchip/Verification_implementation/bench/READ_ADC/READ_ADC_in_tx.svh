class in_transaction extends uvm_sequence_item;
	`uvm_object_utils(in_transaction)
    
    rand bit[7:0] db; // constraint
	bit EOC, read, reset;

	function new(string name = "in_transaction");
		super.new(name);
	endfunction : new

	function string convert2string;
		return $sformatf("EOC = %b, db = %0d, reset = %b", EOC, db, reset);
	endfunction : convert2string


endclass : in_transaction
