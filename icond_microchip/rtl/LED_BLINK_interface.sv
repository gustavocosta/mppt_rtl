interface dut_if (input clk, reset);

    // In signals
    
    logic [1:0] led_s;

    // Out signals

    logic led;
endinterface
