/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/edge/Documentos/MPPT/mppt-29-rtl-final-699beade2d899d953c7dfd0e9cab19f634505684/rtl/nco.v";
static int ng1[] = {0, 0};
static unsigned int ng2[] = {200U, 0U};
static unsigned int ng3[] = {1U, 0U};
static unsigned int ng4[] = {0U, 0U};
static int ng5[] = {1, 0};



static int sp_init(char *t1, char *t2)
{
    int t0;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;

LAB0:    t0 = 1;
    t3 = (t2 + 48U);
    t4 = *((char **)t3);
    if (t4 == 0)
        goto LAB2;

LAB3:    goto *t4;

LAB2:    t4 = (t1 + 984);
    xsi_vlog_subprogram_setdisablestate(t4, &&LAB4);
    xsi_set_current_line(42, ng0);

LAB5:    xsi_set_current_line(43, ng0);
    t5 = ((char*)((ng1)));
    t6 = (t1 + 2496);
    xsi_vlogvar_assign_value(t6, t5, 0, 0, 23);
    xsi_set_current_line(44, ng0);
    t4 = ((char*)((ng2)));
    t5 = (t1 + 2816);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 8);
    xsi_set_current_line(45, ng0);
    t4 = (t1 + 2816);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t1 + 2656);
    xsi_vlogvar_assign_value(t7, t6, 0, 0, 8);
    xsi_set_current_line(46, ng0);
    t4 = ((char*)((ng3)));
    t5 = (t1 + 2976);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 1);
    xsi_set_current_line(47, ng0);
    t4 = ((char*)((ng3)));
    t5 = (t1 + 2336);
    xsi_vlogvar_assign_value(t5, t4, 0, 0, 2);

LAB4:    xsi_vlog_dispose_subprogram_invocation(t2);
    t4 = (t2 + 48U);
    *((char **)t4) = &&LAB2;
    t0 = 0;

LAB1:    return t0;
}

static void Initial_37_0(char *t0)
{
    char *t1;
    char *t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    int t13;
    char *t14;
    char *t15;
    char *t16;
    char *t17;

LAB0:    t1 = (t0 + 3888U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(37, ng0);

LAB4:    xsi_set_current_line(38, ng0);
    t2 = (t0 + 3696);
    t3 = (t0 + 984);
    t4 = xsi_create_subprogram_invocation(t2, 0, t0, t3, 0, 0);
    xsi_vlog_subprogram_pushinvocation(t3, t4);

LAB7:    t5 = (t0 + 3792);
    t6 = *((char **)t5);
    t7 = (t6 + 80U);
    t8 = *((char **)t7);
    t9 = (t8 + 272U);
    t10 = *((char **)t9);
    t11 = (t10 + 0U);
    t12 = *((char **)t11);
    t13 = ((int  (*)(char *, char *))t12)(t0, t6);

LAB9:    if (t13 != 0)
        goto LAB10;

LAB5:    t6 = (t0 + 984);
    xsi_vlog_subprogram_popinvocation(t6);

LAB6:    t14 = (t0 + 3792);
    t15 = *((char **)t14);
    t14 = (t0 + 984);
    t16 = (t0 + 3696);
    t17 = 0;
    xsi_delete_subprogram_invocation(t14, t15, t0, t16, t17);

LAB1:    return;
LAB8:;
LAB10:    t5 = (t0 + 3888U);
    *((char **)t5) = &&LAB7;
    goto LAB1;

}

static void Always_51_1(char *t0)
{
    char t4[8];
    char t45[8];
    char *t1;
    char *t2;
    char *t3;
    char *t5;
    char *t6;
    unsigned int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    unsigned int t19;
    unsigned int t20;
    unsigned int t21;
    unsigned int t22;
    char *t23;
    unsigned int t24;
    unsigned int t25;
    unsigned int t26;
    unsigned int t27;
    unsigned int t28;
    char *t29;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    char *t38;
    char *t39;
    int t40;
    char *t41;
    char *t42;
    char *t43;
    char *t44;

LAB0:    t1 = (t0 + 4136U);
    t2 = *((char **)t1);
    if (t2 == 0)
        goto LAB2;

LAB3:    goto *t2;

LAB2:    xsi_set_current_line(51, ng0);
    t2 = (t0 + 4456);
    *((int *)t2) = 1;
    t3 = (t0 + 4168);
    *((char **)t3) = t2;
    *((char **)t1) = &&LAB4;

LAB1:    return;
LAB4:    xsi_set_current_line(51, ng0);

LAB5:    xsi_set_current_line(52, ng0);
    t5 = (t0 + 1776U);
    t6 = *((char **)t5);
    memset(t4, 0, 8);
    t5 = (t6 + 4);
    t7 = *((unsigned int *)t5);
    t8 = (~(t7));
    t9 = *((unsigned int *)t6);
    t10 = (t9 & t8);
    t11 = (t10 & 1U);
    if (t11 != 0)
        goto LAB9;

LAB7:    if (*((unsigned int *)t5) == 0)
        goto LAB6;

LAB8:    t12 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t12) = 1;

LAB9:    t13 = (t4 + 4);
    t14 = (t6 + 4);
    t15 = *((unsigned int *)t6);
    t16 = (~(t15));
    *((unsigned int *)t4) = t16;
    *((unsigned int *)t13) = 0;
    if (*((unsigned int *)t14) != 0)
        goto LAB11;

LAB10:    t21 = *((unsigned int *)t4);
    *((unsigned int *)t4) = (t21 & 1U);
    t22 = *((unsigned int *)t13);
    *((unsigned int *)t13) = (t22 & 1U);
    t23 = (t4 + 4);
    t24 = *((unsigned int *)t23);
    t25 = (~(t24));
    t26 = *((unsigned int *)t4);
    t27 = (t26 & t25);
    t28 = (t27 != 0);
    if (t28 > 0)
        goto LAB12;

LAB13:    xsi_set_current_line(55, ng0);

LAB22:    xsi_set_current_line(56, ng0);
    t2 = (t0 + 2976);
    t3 = (t2 + 56U);
    t5 = *((char **)t3);

LAB23:    t6 = ((char*)((ng4)));
    t40 = xsi_vlog_unsigned_case_compare(t5, 1, t6, 1);
    if (t40 == 1)
        goto LAB24;

LAB25:    t2 = ((char*)((ng3)));
    t40 = xsi_vlog_unsigned_case_compare(t5, 1, t2, 1);
    if (t40 == 1)
        goto LAB26;

LAB27:
LAB28:
LAB14:    goto LAB2;

LAB6:    *((unsigned int *)t4) = 1;
    goto LAB9;

LAB11:    t17 = *((unsigned int *)t4);
    t18 = *((unsigned int *)t14);
    *((unsigned int *)t4) = (t17 | t18);
    t19 = *((unsigned int *)t13);
    t20 = *((unsigned int *)t14);
    *((unsigned int *)t13) = (t19 | t20);
    goto LAB10;

LAB12:    xsi_set_current_line(52, ng0);

LAB15:    xsi_set_current_line(53, ng0);
    t29 = (t0 + 3944);
    t30 = (t0 + 984);
    t31 = xsi_create_subprogram_invocation(t29, 0, t0, t30, 0, 0);
    xsi_vlog_subprogram_pushinvocation(t30, t31);

LAB18:    t32 = (t0 + 4040);
    t33 = *((char **)t32);
    t34 = (t33 + 80U);
    t35 = *((char **)t34);
    t36 = (t35 + 272U);
    t37 = *((char **)t36);
    t38 = (t37 + 0U);
    t39 = *((char **)t38);
    t40 = ((int  (*)(char *, char *))t39)(t0, t33);

LAB20:    if (t40 != 0)
        goto LAB21;

LAB16:    t33 = (t0 + 984);
    xsi_vlog_subprogram_popinvocation(t33);

LAB17:    t41 = (t0 + 4040);
    t42 = *((char **)t41);
    t41 = (t0 + 984);
    t43 = (t0 + 3944);
    t44 = 0;
    xsi_delete_subprogram_invocation(t41, t42, t0, t43, t44);
    goto LAB14;

LAB19:;
LAB21:    t32 = (t0 + 4136U);
    *((char **)t32) = &&LAB18;
    goto LAB1;

LAB24:    xsi_set_current_line(57, ng0);

LAB29:    xsi_set_current_line(58, ng0);
    t12 = (t0 + 2496);
    t13 = (t12 + 56U);
    t14 = *((char **)t13);
    t23 = (t0 + 1936U);
    t29 = *((char **)t23);
    memset(t4, 0, 8);
    xsi_vlog_unsigned_add(t4, 23, t14, 23, t29, 15);
    t23 = (t0 + 2496);
    t30 = (t23 + 56U);
    t31 = *((char **)t30);
    memset(t45, 0, 8);
    t32 = (t4 + 4);
    if (*((unsigned int *)t32) != 0)
        goto LAB31;

LAB30:    t33 = (t31 + 4);
    if (*((unsigned int *)t33) != 0)
        goto LAB31;

LAB34:    if (*((unsigned int *)t4) < *((unsigned int *)t31))
        goto LAB32;

LAB33:    t35 = (t45 + 4);
    t7 = *((unsigned int *)t35);
    t8 = (~(t7));
    t9 = *((unsigned int *)t45);
    t10 = (t9 & t8);
    t11 = (t10 != 0);
    if (t11 > 0)
        goto LAB35;

LAB36:    xsi_set_current_line(67, ng0);
    t2 = (t0 + 2496);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t12 = (t0 + 1936U);
    t13 = *((char **)t12);
    memset(t4, 0, 8);
    xsi_vlog_unsigned_add(t4, 23, t6, 23, t13, 15);
    t12 = (t0 + 2496);
    xsi_vlogvar_assign_value(t12, t4, 0, 0, 23);

LAB37:    goto LAB28;

LAB26:    xsi_set_current_line(70, ng0);

LAB43:    xsi_set_current_line(71, ng0);
    t3 = (t0 + 2496);
    t6 = (t3 + 56U);
    t12 = *((char **)t6);
    t13 = (t0 + 1936U);
    t14 = *((char **)t13);
    memset(t4, 0, 8);
    xsi_vlog_unsigned_add(t4, 23, t12, 23, t14, 15);
    t13 = (t0 + 2496);
    xsi_vlogvar_assign_value(t13, t4, 0, 0, 23);
    xsi_set_current_line(72, ng0);
    t2 = (t0 + 2656);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t12 = ((char*)((ng1)));
    memset(t4, 0, 8);
    t13 = (t6 + 4);
    if (*((unsigned int *)t13) != 0)
        goto LAB45;

LAB44:    t14 = (t12 + 4);
    if (*((unsigned int *)t14) != 0)
        goto LAB45;

LAB48:    if (*((unsigned int *)t6) > *((unsigned int *)t12))
        goto LAB46;

LAB47:    t29 = (t4 + 4);
    t7 = *((unsigned int *)t29);
    t8 = (~(t7));
    t9 = *((unsigned int *)t4);
    t10 = (t9 & t8);
    t11 = (t10 != 0);
    if (t11 > 0)
        goto LAB49;

LAB50:    xsi_set_current_line(74, ng0);

LAB52:    xsi_set_current_line(75, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2336);
    t6 = (t0 + 2336);
    t12 = (t6 + 72U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng5)));
    xsi_vlog_generic_convert_bit_index(t4, t13, 2, t14, 32, 1);
    t23 = (t4 + 4);
    t7 = *((unsigned int *)t23);
    t40 = (!(t7));
    if (t40 == 1)
        goto LAB53;

LAB54:    xsi_set_current_line(76, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 2336);
    t6 = (t0 + 2336);
    t12 = (t6 + 72U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng1)));
    xsi_vlog_generic_convert_bit_index(t4, t13, 2, t14, 32, 1);
    t23 = (t4 + 4);
    t7 = *((unsigned int *)t23);
    t40 = (!(t7));
    if (t40 == 1)
        goto LAB55;

LAB56:    xsi_set_current_line(77, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2976);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);

LAB51:    goto LAB28;

LAB31:    t34 = (t45 + 4);
    *((unsigned int *)t45) = 1;
    *((unsigned int *)t34) = 1;
    goto LAB33;

LAB32:    *((unsigned int *)t45) = 1;
    goto LAB33;

LAB35:    xsi_set_current_line(59, ng0);

LAB38:    xsi_set_current_line(60, ng0);
    t36 = ((char*)((ng1)));
    t37 = (t0 + 2496);
    xsi_vlogvar_assign_value(t37, t36, 0, 0, 23);
    xsi_set_current_line(61, ng0);
    t2 = ((char*)((ng3)));
    t3 = (t0 + 2336);
    t6 = (t0 + 2336);
    t12 = (t6 + 72U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng5)));
    xsi_vlog_generic_convert_bit_index(t4, t13, 2, t14, 32, 1);
    t23 = (t4 + 4);
    t7 = *((unsigned int *)t23);
    t40 = (!(t7));
    if (t40 == 1)
        goto LAB39;

LAB40:    xsi_set_current_line(62, ng0);
    t2 = ((char*)((ng4)));
    t3 = (t0 + 2336);
    t6 = (t0 + 2336);
    t12 = (t6 + 72U);
    t13 = *((char **)t12);
    t14 = ((char*)((ng1)));
    xsi_vlog_generic_convert_bit_index(t4, t13, 2, t14, 32, 1);
    t23 = (t4 + 4);
    t7 = *((unsigned int *)t23);
    t40 = (!(t7));
    if (t40 == 1)
        goto LAB41;

LAB42:    xsi_set_current_line(63, ng0);
    t2 = (t0 + 2816);
    t3 = (t2 + 56U);
    t6 = *((char **)t3);
    t12 = (t0 + 2656);
    xsi_vlogvar_assign_value(t12, t6, 0, 0, 8);
    xsi_set_current_line(64, ng0);
    t2 = ((char*)((ng5)));
    t3 = (t0 + 2976);
    xsi_vlogvar_assign_value(t3, t2, 0, 0, 1);
    goto LAB37;

LAB39:    xsi_vlogvar_assign_value(t3, t2, 0, *((unsigned int *)t4), 1);
    goto LAB40;

LAB41:    xsi_vlogvar_assign_value(t3, t2, 0, *((unsigned int *)t4), 1);
    goto LAB42;

LAB45:    t23 = (t4 + 4);
    *((unsigned int *)t4) = 1;
    *((unsigned int *)t23) = 1;
    goto LAB47;

LAB46:    *((unsigned int *)t4) = 1;
    goto LAB47;

LAB49:    xsi_set_current_line(73, ng0);
    t30 = (t0 + 2656);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = ((char*)((ng5)));
    memset(t45, 0, 8);
    xsi_vlog_unsigned_minus(t45, 32, t32, 8, t33, 32);
    t34 = (t0 + 2656);
    xsi_vlogvar_assign_value(t34, t45, 0, 0, 8);
    goto LAB51;

LAB53:    xsi_vlogvar_assign_value(t3, t2, 0, *((unsigned int *)t4), 1);
    goto LAB54;

LAB55:    xsi_vlogvar_assign_value(t3, t2, 0, *((unsigned int *)t4), 1);
    goto LAB56;

}


extern void work_m_06645046728704417431_3553762526_init()
{
	static char *pe[] = {(void *)Initial_37_0,(void *)Always_51_1};
	static char *se[] = {(void *)sp_init};
	xsi_register_didat("work_m_06645046728704417431_3553762526", "isim/nco_tb_isim_beh.exe.sim/work/m_06645046728704417431_3553762526.didat");
	xsi_register_executes(pe);
	xsi_register_subprogram_executes(se);
}
