`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:    	20:03:59 07/28/2017
// Design Name:		Gustavo Costa
// Module Name:		top 
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Revision 0.10 - Added an button debounce function and making two NCO signals to control both transistors
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module top(
    input clk,
    input button,
    input [7:0] db,
    input eoc,
    output [2:0] a,
    output convst,
    output cs,
    output rd,
    output led,
    output [1:0] nco_signal,
	 output on
    );
	

	wire [1:0] led_s;
	wire [7:0] vin, iin, vout, iout;
	wire [14:0] increment;
	wire done, read, button_debounce;

	assign on = 1;
	
	debouncer instdb(clk, button, button_debounce);
	
	read_adc read_adc_module(clk, button_debounce, db, eoc, read, a,	convst,	cs,	rd,	done, vin, iin,	vout, iout);
	led_blink led_module (clk, led_s, led); 
	nco nco_module(clk, button_debounce, increment, nco_signal);
	main main_module(clk, button_debounce, done, vin, iin, vout, iout, read, led_s, increment);

endmodule

module debouncer(
    input clk, //this is the clock provided
    input PB,  //this is the input to be debounced
    output reg PB_state  //this is the debounced switch
);
/*This module debounces the pushbutton PB.
 *It can be added to your project files and called as is:
 *DO NOT EDIT THIS MODULE
 */

// Synchronize the switch input to the clock
reg PB_sync_0;
always @(posedge clk) PB_sync_0 <= PB; 
reg PB_sync_1;
always @(posedge clk) PB_sync_1 <= PB_sync_0;

// Debounce the switch
reg [19:0] PB_cnt;
always @(posedge clk)
if(PB_state==PB_sync_1)
    PB_cnt <= 0;
else
begin
    PB_cnt <= PB_cnt + 1'b1;  
    if(PB_cnt == 20'hfffff) PB_state <= ~PB_state;  
end
endmodule