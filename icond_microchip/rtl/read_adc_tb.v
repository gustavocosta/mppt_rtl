`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:46:05 08/04/2017
// Design Name:   read_adc
// Module Name:   /home/brunolima/Projetos/MPPT/lastBranch/rtl/read_adc_tb.v
// Project Name:  mppt
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: read_adc
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module read_adc_tb;

	// Inputs
	reg clk;
	reg reset;
	reg [7:0] db;
	reg eoc;
	reg read;

	// Outputs
	wire [2:0] a;
	wire convst;
	wire cs;
	wire rd;
	wire done;
	wire [7:0] vin;
	wire [7:0] iin;
	wire [7:0] vout;
	wire [7:0] iout;
	reg [10:0] clk_index;
	
	// Instantiate the Unit Under Test (UUT)
	read_adc uut (
		.clk(clk), 
		.reset(reset), 
		.db(db), 
		.eoc(eoc), 
		.read(read), 
		.a(a), 
		.convst(convst), 
		.cs(cs), 
		.rd(rd), 
		.done(done), 
		.vin(vin), 
		.iin(iin), 
		.vout(vout), 
		.iout(iout)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		db = 0;
		eoc = 1;
		read = 0;
		clk_index = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		reset = 1;
	end
	
	always begin
		#10; clk = ~clk;
		if(clk)clk_index = clk_index + 1;
		
		if(clk_index == 49) begin
			eoc = 0;
		end
		if(clk_index == 51) begin
			eoc = 1;
			db = 1;
		end
		if(clk_index == 92) begin
			eoc = 0;
		end
		if(clk_index == 94) begin
			eoc = 1;
			db =  35;
		end
		if(clk_index == 135) begin
			eoc = 0;
		end
		if(clk_index == 137) begin
			eoc = 1;
			db =  112;
		end		
		if(clk_index == 177) eoc = 0;
		if(clk_index == 179)
		 begin
			eoc = 1;
			db =  57;
		end		
		if(clk_index == 220) read = 1;
	end
      
endmodule

