`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   19:00:33 08/04/2017
// Design Name:   nco
// Module Name:   /home/brunolima/Projetos/MPPT/lastBranch/rtl/nco_tb.v
// Project Name:  mppt
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: nco
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module nco_tb;

	// Inputs
	reg clk;
	reg reset;
	reg [14:0] increment;
	reg [10:0] clk_index;

	// Outputs
	wire [1:0] nco_signal;

	// Instantiate the Unit Under Test (UUT)
	nco uut (
		.clk(clk), 
		.reset(reset), 
		.increment(increment), 
		.nco_signal(nco_signal)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		reset = 0;
		increment = 0;
		clk_index = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		increment = 15'b111001100111100; 
		
		#10; reset = 1;
	end
	
	always begin
		#5; clk = ~clk;
		if(clk)clk_index = clk_index + 1;
		//if(clk_index == 350) reset = 0;
		//if(clk_index == 352) reset = 1;
	end
      
endmodule

