`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date:		15:02:51 03/13/2017 
// Design Name:		Gustavo Costa
// Module Name:		main 
// Project Name:		Implementação de um controlador MPPT utilizandoFPGA
// Target Devices:	FPGA Spartan 6 XC6SLX9
// Tool versions:		ISE 14.7
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Revision 0.10 - Icond algorithm adjusted
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
`include "parameters.vh"
module main(
	input	clk,
	input	reset,
	input	done,
   input	[7:0] vin,
   input [7:0] iin,
   input [7:0] vout,
   input [7:0] iout,
   output reg	read,
   output reg	[1:0] led_s,
   output reg	[14:0] increment
   );
	


	// -- INITIALIZING VARIABLES
	// Battery state
	reg [2:0]	battery_state;
	reg [7:0]	imin_db;
	reg [15:0]	iflat_db;
	reg [15:0]	state_counter;
	reg [7:0]	imin;
	// NCO variables
	reg [7:0] 	cmode;				// Char, 			1 byte
	reg [15:0]	warmup;				// Unsigned Int,	2 bytes
	reg [9:0]	vref, iref;			// Unsigned Int,	2 bytes
	// PID variables
	reg signed [16:0] pp, pi;		// Unsigned Int,	2 bytes
	reg  [7:0] cc_cv;					// Unsigned Char, 	1 byte
	// Main
	reg [15:0] mppt_calc;
	reg [15:0] second;
	reg [15:0] track;
	reg [15:0] dmax;
	// MPPT
	reg [9:0] 	delta_i, delta_v;
	reg [19:0]	delta_p;
	reg [9:0]	f_iin, f_vin, fl_iin, fl_vin;
	reg [19:0]	power, l_power;
	reg [9:0]	ineq;
	reg signed [9:0] vinref;
	
	reg [1:0] main_state;	/*	0 = initial_state
								1 = waiting done to be active
								2 = doing calculations
							*/
	
	// -- MACROS
	task SET_VOLTAGE();
		input [9:0] n;
		begin
			vref = n;
		end
	endtask
	
	task SET_CURRENT();
		input [9:0] n;
		begin
			iref = n;
		end
	endtask

	task SET_LED_BLINK();
		input[1:0] n;
		begin
			led_s = n;
		end
	endtask
	
	task START_CONVERTER();
	begin
		warmup = WARMUP_TIME;
	end
	endtask
	
	task STOP_CONVERTER();
	begin
		increment = 0;
		set_nco();
	end
	endtask
	
	// -- BATTERY STATE FUNCTIONS
	task init_state_machine();
	begin
		battery_state = PRECHARGE;	
		state_counter = PRECHARGE_TIME;
		SET_LED_BLINK(BLINK_05HZ);

		SET_CURRENT(ILIM_PRECHARGE);
		SET_VOLTAGE(CHARGING_VOLTAGE);
		
		imin = ILIM;
		imin_db = IMIN_UPDATE;
		iflat_db = IFLAT_COUNT;

		START_CONVERTER();
	end
	endtask
	
	
	task battery_state_machine();
		case ( battery_state )
			PRECHARGE:	
				begin
					SET_LED_BLINK(BLINK_05HZ);
						if(vout < CUTOFF_VOLTAGE)
						begin
							if(state_counter > 0) state_counter = state_counter - 1; 
							else
								begin
									battery_state = FAULT;
									SET_LED_BLINK(BLINK_05HZ);
								end
						end 
						else
							begin
								battery_state = CHARGE;
								SET_CURRENT(ILIM);
							end
				end 
		
			CHARGE:
				begin
					SET_LED_BLINK(BLINK_05HZ);
					if(cmode == 0)
					begin
						if(iout < imin)
						begin
							if(imin_db) imin_db = imin_db - 1; else
							begin
								imin = iout;
								imin_db = IMIN_UPDATE;
								iflat_db = IFLAT_COUNT;
							end
						end else
						begin
							imin_db = IMIN_UPDATE;
							if(iflat_db) iflat_db = iflat_db - 1;
						end
					end else
					begin
						imin_db = IMIN_UPDATE;
						iflat_db = IFLAT_COUNT;
						imin = ILIM;
					end
					if(imin < IFLOAT || !iflat_db)
					begin				
						battery_state = FLOAT;
						state_counter = FLOAT_TIME;

						SET_VOLTAGE(FLOATING_VOLTAGE);
					end
				end
				
			FLOAT:
				begin
					SET_LED_BLINK(LED_ON);
					if(state_counter > 0) state_counter = state_counter - 1; else
					begin
						battery_state = DONE;
					end
					if(state_counter < FLOAT_RELAX_TIME && iout < I_BAT_DETECT)
					battery_state = IDLE;
				end
			
			IDLE:
				begin
					SET_LED_BLINK(LED_OFF);
					SET_VOLTAGE(0);
					SET_CURRENT(0);
					STOP_CONVERTER();
				end			
			FAULT:
				begin
				SET_LED_BLINK(BLINK_2HZ);
				SET_VOLTAGE(0);
				SET_CURRENT(0);
				STOP_CONVERTER();	
				end
			DONE:
				begin
					if( vout < TOPPING_VOLTAGE && vout > VBAT_DETECTION)
					begin
						battery_state = CHARGE;

						SET_CURRENT(ILIM);
						SET_VOLTAGE(CHARGING_VOLTAGE);

						imin = ILIM;
						imin_db = IMIN_UPDATE;
						iflat_db = IFLAT_COUNT;

						START_CONVERTER();
					end else
					begin
						SET_VOLTAGE(0);
						SET_CURRENT(0);
						STOP_CONVERTER();	
						if( vout < VBAT_DETECTION ) battery_state = IDLE;
					end
				end
		endcase
	endtask
	
	
	// TASKS USED IN MAIN
	// Initialize variables used in all tasks
	task init;
	begin
		//A VERIFICAR
		track = TRACK_DELAY;
		mppt_calc = MPPT_INTERVAL;
		second = SECOND_COUNT;
		vinref = 8'b100;
		f_iin = 8'b0;
		f_vin = 8'b0;
		fl_iin = 8'b0;
		fl_vin = 8'b0;
		//----------------------
	
		increment = NCO_MIN;
		
		cmode = 0;
		warmup = WARMUP_TIME;
		
		cc_cv = CURRENT_MODE;
		
		iref = 0;
		vref = 0;
		
		main_state = 2'b0;
		read = 0;
		
		STOP_CONVERTER();
	end
	endtask
	
	// Check current and voltage, comparing them to ref value
	task cc_cv_mode;
	begin
		// Comparing vout com vref
		if ( vout > vref ) begin
			if ( cc_cv == 1'b1 ) begin
				cc_cv = cc_cv - 1'b1;
			end
			else begin
				if ( cmode == 1 ) pi = 1'b0;
				cmode = 0;
			end
		end

		// Comparing iout com iref
		if ( iout > iref ) begin
			if ( cmode == 1'b0 ) pi = 0;
			cmode = 1;
			cc_cv = CURRENT_MODE;
		end		
	end
	endtask
	
	task inc_vinref();
		input [9:0] value;
		begin
			vinref = vinref + value;
			if (vinref > 255)
			begin
				vinref = 10'd255;
			end
			else if (vinref < 1)
			begin
				vinref = 10'b1;
			end
		end
	endtask
	
	
	// PID calculations
	reg signed [15:0] pid_er, pid_ipid;	// Signed. Used only in "pid task", as it is not possible to declare a internal variable inside a task.
	task pid;
	input [15:0] feedback, setpoint;
	begin
		pid_er = setpoint - feedback;
		
		if ( pid_er > ERR_MAX ) pid_er = ERR_MAX;
		if ( pid_er < ERR_MIN ) pid_er = ERR_MIN;
		
		if ( warmup == 0 )
		begin
			if ( cmode > 0 ) pp = pid_er;
			else pp = pid_er;
			pi = pi + pid_er;
			if ( pi > ERR_MAX ) pi = ERR_MAX;
			if ( pi < ERR_MIN ) pi = ERR_MIN;
			
			pid_ipid = pp;
			pid_ipid = pid_ipid + (pi / 256);
			
			if ( pid_ipid > ERR_MAX ) pid_ipid = ERR_MAX;
			if ( pid_ipid < ERR_MIN ) pid_ipid = ERR_MIN;
			
			increment = increment + pid_ipid;
		end
		else
		begin
			warmup = warmup - 1;
			if ( pid_er > 0 ) increment = increment + 1;
			else increment = increment - 1;
			pi = 0;
		end
		
		set_nco();		
	end
	endtask;

	// Set NCO
	task set_nco;
	begin
		if ( increment < NCO_MIN ) increment = NCO_MIN;
		if ( increment > NCO_MAX ) increment = NCO_MAX;
	end
	endtask;
	
	// MPPT			
	task mppt_INCCOND;
	begin
		power = iin * vin;
		delta_p = power - l_power;

		delta_i = f_iin - fl_iin;
		delta_v = f_vin - fl_vin;

		if(delta_v)
		begin
			ineq = delta_p / delta_v;
			if(ineq != 0)
			begin
				if(ineq > 0) inc_vinref(MPPT_STEP);
				else inc_vinref(MPPT_STEP*(-1));
			end
		end else
		begin
			if(delta_i != 0)
			begin
				if(delta_i > 0) inc_vinref(MPPT_STEP);
				else inc_vinref(MPPT_STEP*(-1));
			end
		end

		fl_iin = f_iin;
		fl_vin = f_vin;
		l_power = power;

		f_iin = 0;
		f_vin = 0;
	end
	endtask


	reg[31:0] count;

	initial begin
		count = 0;
		init();
		battery_state = IDLE;
		SET_LED_BLINK(LED_OFF);
	end
	
	// Main loop
	always @(posedge clk or negedge reset)
	begin
		if ( ~reset ) begin
			if ( !reset ) 
			begin
				init();
				if( battery_state == IDLE ) begin
					init_state_machine();
				end
				else begin
					battery_state = IDLE;
					SET_LED_BLINK(LED_OFF);
				end
			end 	
		end		
		else begin
			count = count + 1;
			case( main_state )
			0 :	begin
					if ( track > 0 && mppt_calc > 0 )
						mppt_calc = mppt_calc - 1;
					if ( second > 0 ) 
						second = second - 1;
					
					//read_adc();
					read = 1;
					main_state = 1;
				end // End of main_state == 0
			1 : if ( done == 1'b1 ) begin
				main_state = 2; // Update state when done goes active high. Data is ready at input.
				read = 0;
			end
			2 : begin
					if ( track == 0 ) 				// Indicates if the main loop is calculating MPPT or regulating the output
					begin
						if ( battery_state != FAULT )
						begin
							cc_cv_mode();			// Solves the transactions between working modules
							if ( cmode == 0 ) 		// code - shows who must be regulated
								pid( vout, vref );
							else
								pid( iout, iref );	// Proportional, Integral, Derivative
								
							if( increment >= dmax )
								track = TRACK_DELAY;
						end
					end
					else
					begin
						if ( mppt_calc < MPPT_AVERAGE )	// Defines the number of samples
						begin
							f_vin = f_vin + vin;	// Average of input voltages
							f_iin = f_iin + iin;	// Average of input currents
						end
						
						if ( mppt_calc == 0 ) 
						begin
							mppt_calc = MPPT_INTERVAL;
							mppt_INCCOND();			// Executes MPPT
						end
						
						pid(vinref, vin);
						
						if ( vout > vref || iout > iref )
						begin
							track = track - 1;
							dmax = increment;
							if ( track > 0 ) 
								battery_state_machine();
						end
						else
						begin
							track = TRACK_DELAY;	// Debouncing value to alternate between tracking and output regulating
						end
					end
					
					if ( second == 0 )
					begin
						second = SECOND_COUNT;
						if ( track > 0 )
							battery_state_machine();
					end
					main_state = 0;
				end	// End of main_state == 1

			endcase	// END_CASE
		end		// END_OF_NOT_RESET
	end

endmodule
