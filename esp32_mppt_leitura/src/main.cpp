#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <driver/adc.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

const int ledPin = 13;
const int butonPin = 32;

portMUX_TYPE mux = portMUX_INITIALIZER_UNLOCKED;

int page;
int v_ch0;
int v_ch3;
int v_ch6;
int v_ch7;
int power;

const int debounceDelay = 200;
unsigned long lastDebounceTime = 0;

void IRAM_ATTR handleInterrupt() {
  portENTER_CRITICAL_ISR(&mux);
  if((millis() - lastDebounceTime) > debounceDelay){
    page++;
    if(page >= 3){
      page = 0;
    }
    lastDebounceTime = millis();
  }
  portEXIT_CRITICAL_ISR(&mux);
}

void setup()
{
  Serial.begin(9600);
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  // init done
  display.clearDisplay();
  // text display tests
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.println("Hello,");
  display.println("world!");
  display.display();
  display.clearDisplay();

  pinMode(ledPin, OUTPUT);
  pinMode(butonPin, INPUT);

  attachInterrupt(digitalPinToInterrupt(butonPin), handleInterrupt, FALLING);

  adc1_config_width(ADC_WIDTH_12Bit);
  adc1_config_channel_atten(ADC1_CHANNEL_0,ADC_ATTEN_6db);
  adc1_config_channel_atten(ADC1_CHANNEL_3,ADC_ATTEN_6db);
  adc1_config_channel_atten(ADC1_CHANNEL_6,ADC_ATTEN_6db);
  adc1_config_channel_atten(ADC1_CHANNEL_7,ADC_ATTEN_6db);

  page = 0;
}

double calc_voltage(int adcread){
  return (adcread*2.2)/4095.0;
}

double calc_sensor_voltage(int adcread){
  return (calc_voltage(adcread) * 11.0);
}

double calc_sensor_current(int adcread){
  return (calc_voltage(adcread) * 3);
}


void loop()
{
  v_ch0 = adc1_get_voltage(ADC1_CHANNEL_0);
  v_ch3 = adc1_get_voltage(ADC1_CHANNEL_3);
  v_ch6 = adc1_get_voltage(ADC1_CHANNEL_6);
  v_ch7 = adc1_get_voltage(ADC1_CHANNEL_7);

  switch (page) {
    case 0:
      display.setTextSize(1);
      display.setTextColor(WHITE);
      display.setCursor(0,0);
      display.println("Leituras");
      display.print("In: ");
      display.print(calc_sensor_voltage(v_ch0));
      display.print(" V | ");
      display.print(calc_sensor_current(v_ch3));
      display.println(" A");
      display.print("Out: ");
      display.print(calc_sensor_voltage(v_ch6));
      display.print(" V | ");
      display.print(calc_sensor_current(v_ch7));
      display.println(" A");
      display.display();
      display.clearDisplay();
    break;

    case 1:
      display.setTextSize(1.8);
      display.setTextColor(WHITE);
      display.setCursor(0,0);
      display.println("Eficiencia:");
      power = calc_sensor_voltage(v_ch6)*calc_sensor_current(v_ch7);
      display.print(power*5.0);
      display.println(" %");
      display.display();
      display.clearDisplay();
    break;

    case 2:
      display.setTextSize(1);
      display.setTextColor(WHITE);
      display.setCursor(0,0);
      display.println("Tensoes no ADC");
      display.print("ch0: ");
      display.print(calc_voltage(v_ch0));
      display.print("V ");
      display.print("ch3: ");
      display.print(calc_voltage(v_ch3));
      display.println("V");
      display.print("ch6: ");
      display.print(calc_voltage(v_ch6));
      display.print("V ");
      display.print("ch7: ");
      display.print(calc_voltage(v_ch7));
      display.println("V");
      display.display();
      display.clearDisplay();
    break;

    default:
      display.setTextSize(2);
      display.setTextColor(WHITE);
      display.setCursor(0,0);
      display.println("Erro!");
      display.display();
      display.clearDisplay();
    break;
  }
  delay(5);
}
